package es.com.acc.appEcommerce.utils.JSON;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import es.com.acc.appEcommerce.activitys.BaseActivity;
import es.com.acc.appEcommerce.entities.UserLogin;


public class JSONTask_Login extends AsyncTask<Void, Void, String>
{
    private Context context;
    private BaseActivity PrSh;
    private static final String TAG = "PostFetcher";
    private Boolean StockLoaded = false;
    public static final String SERVER_URL = "-- url del servicio --";
    private UserLogin login;

    private String email;
    private String password;

    public JSONTask_Login(BaseActivity A, Context mContext, String ema, String psw)
    {
        this.PrSh = A;
        this.context = mContext;
        StockLoaded = false;

        email = ema;
        password = psw;
    }

    private void handlePostsList(UserLogin posts)
    {
        this.login = posts;
    }

    //private void failedLoadingPosts()
    //{
    //    Log.d("Error", "Post Failed");
    //}

    @Override
    protected void onPostExecute(String result)
    {
        if((StockLoaded==false)&&(login!=null)) {
            StockLoaded = true;
            PrSh.xmlLoginCharged(login);
        }
    }

     @Override
     protected String doInBackground(Void... params)
     {
         try {
             //Create an HTTP client
             HttpClient client = new DefaultHttpClient();

             String pathURL = SERVER_URL;
             pathURL += email;
             pathURL += "&password=";
             pathURL += password;

             System.out.println(" ::::::::::::::::::::::::::::: Product Stock: >> "+pathURL);

             HttpPost post = new HttpPost(pathURL);

             //Perform the request and check the status code
             HttpResponse response = client.execute(post);
             StatusLine statusLine = response.getStatusLine();
             if(statusLine.getStatusCode() == 200) {
                 HttpEntity entity = response.getEntity();
                 InputStream content = entity.getContent();

                 try {
                     //Read the server response and attempt to parse it as JSON
                     Reader reader = new InputStreamReader(content);

                     GsonBuilder gsonBuilder = new GsonBuilder();
                     gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                     Gson gson = gsonBuilder.create();
                     UserLogin log = gson.fromJson(reader, UserLogin.class);
                     content.close();

                     handlePostsList(log);
                 } catch (Exception ex) {
                     //Log.e(TAG, "Failed to parse JSON due to: " + ex);
                     //failedLoadingPosts();
                 }
             } else {
                 //Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                 //failedLoadingPosts();
             }
         } catch(Exception ex) {
             //Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
             //failedLoadingPosts();
         }
         return null;
     }
}
