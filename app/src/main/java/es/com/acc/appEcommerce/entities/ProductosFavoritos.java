package es.com.acc.appEcommerce.entities;

/**
 * Created by Imanol on 22/05/2015.
 */
public class ProductosFavoritos
{
    int _product_id;

    public ProductosFavoritos()
    {

    }

    public ProductosFavoritos(int id)
    {
        this._product_id = id;
    }

    // getting Id
    public int getId(){
        return this._product_id;
    }

    // setting id
    public void setId(int id){
        this._product_id = id;
    }
}
