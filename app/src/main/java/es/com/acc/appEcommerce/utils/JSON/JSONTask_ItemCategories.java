package es.com.acc.appEcommerce.utils.JSON;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;

import es.com.acc.appEcommerce.activitys.BaseActivity;
import es.com.acc.appEcommerce.entities.ItemCategories;


public class JSONTask_ItemCategories extends AsyncTask<Void, Void, String>{

    private Context context;
    private BaseActivity lodAct;
    private static final String TAG = "PostFetcher";
    private Boolean CestaLoaded = false;
    public static final String SERVER_URL = "-- url del servicio --";
    private List<ItemCategories> itemCat;

    private String productID;

    public JSONTask_ItemCategories(BaseActivity A, Context mContext, int ProductID){
        this.lodAct = A;
        this.context = mContext;

        this.productID = String.valueOf(ProductID);

        CestaLoaded = false;
        itemCat = null;
    }

    private void handlePostsList(List<ItemCategories>  posts)
    {
        this.itemCat = posts;
    }

    private void failedLoadingPosts() {
        //Log.d("Error", "Post Failed");
        lodAct.xmlCestaCharged(null);
    }

    @Override
    protected void onPostExecute(String result)
    {
        if((CestaLoaded==false)&&(itemCat!=null)) {
            CestaLoaded = true;
            lodAct.xmlItemCatCharged(itemCat);
        }
    }

     @Override
     protected String doInBackground(Void... params) {
         try {
             //Create an HTTP client
             HttpClient client = new DefaultHttpClient();

             String pathURL = SERVER_URL;
             pathURL += this.productID;

             HttpPost post = new HttpPost(pathURL);
             System.out.println(" ::::::::::::::::::::::::::::: POST Item Cats 0 : >> "+pathURL);

             //Perform the request and check the status code
             HttpResponse response = client.execute(post);
             StatusLine statusLine = response.getStatusLine();

             if(statusLine.getStatusCode() == 200)
             {
                 HttpEntity entity = response.getEntity();
                 InputStream content = entity.getContent();

                 try {
                     //Read the server response and attempt to parse it as JSON
                     Reader reader = new InputStreamReader(content);

                     GsonBuilder gsonBuilder = new GsonBuilder();
                     gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                     Gson gson = gsonBuilder.create();

                     //List<Producto> Prod = Arrays.asList(gson.fromJson(reader, Producto[].class));
                     List<ItemCategories> cesta = Arrays.asList(gson.fromJson(reader, ItemCategories[].class));
                     content.close();

                     //System.out.println(" ::::::::::::::::::::::::::::: reader GSON  2: >> "+reader);

                     handlePostsList(cesta);
                 } catch (Exception ex) {
                     Log.e(TAG, "Failed to parse JSON due to: " + ex);
                     failedLoadingPosts();
                 }
             } else {
                 Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                 failedLoadingPosts();
             }
         } catch(Exception ex) {
             Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
             failedLoadingPosts();
         }
         return null;
     }

}
