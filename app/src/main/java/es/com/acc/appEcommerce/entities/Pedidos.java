package es.com.acc.appEcommerce.entities;

/**
 * Created by Imanol on 01/06/2015.
 */
public class Pedidos {
    public int Availability;
    public String Date;
    public int Id;
    public int IdERP;
    public Pedido[] Items;
    public String MRW;
    public String PaymentMethod;
    public int ShippingAddress;
    public String ShippingMethod;
    public int User;
}
