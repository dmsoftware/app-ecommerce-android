package es.com.acc.appEcommerce.utils.JSON;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import es.com.acc.appEcommerce.activitys.ProductSheetActivity;
import es.com.acc.appEcommerce.entities.ProductStock;


public class JSONTask_Stock extends AsyncTask<Void, Void, String>
{
    private Context context;
    private ProductSheetActivity PrSh;
    private static final String TAG = "PostFetcher";
    private Boolean StockLoaded = false;
    public static final String SERVER_URL = "-- url del servicio --";
    private ProductStock stock;

    private String productID;
    private String productSize;
    private String productCant;

    public JSONTask_Stock(ProductSheetActivity A, Context mContext, int proID, String SizeID, int Canti)
    {
        this.PrSh = A;
        this.context = mContext;
        StockLoaded = false;

        productID = String.valueOf(proID);
        productSize = SizeID;
        productCant = String.valueOf(Canti);
    }

    private void handlePostsList(ProductStock posts)
    {
        this.stock = posts;
    }

    //private void failedLoadingPosts()
    //{
    //    Log.d("Error", "Post Failed");
    //}

    @Override
    protected void onPostExecute(String result)
    {
        if((StockLoaded==false)&&(stock!=null)) {
            StockLoaded = true;
            PrSh.xmlStockCharged(stock);
        }
    }

     @Override
     protected String doInBackground(Void... params)
     {
         try {
             //Create an HTTP client
             HttpClient client = new DefaultHttpClient();

             String pathURL = SERVER_URL;
             pathURL += productID;
             pathURL += "&SizeId=";
             pathURL += productSize;
             pathURL += "&ShoppingCartId=-1&Units=";
             pathURL += productCant;
             pathURL += "&ShippingType=-1&province=-1&isforemail=false";

             System.out.println(" ::::::::::::::::::::::::::::: Product Stock: >> "+pathURL);

             HttpPost post = new HttpPost(pathURL);

             //Perform the request and check the status code
             HttpResponse response = client.execute(post);
             StatusLine statusLine = response.getStatusLine();
             if(statusLine.getStatusCode() == 200) {
                 HttpEntity entity = response.getEntity();
                 InputStream content = entity.getContent();

                 try {
                     //Read the server response and attempt to parse it as JSON
                     Reader reader = new InputStreamReader(content);

                     GsonBuilder gsonBuilder = new GsonBuilder();
                     gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                     Gson gson = gsonBuilder.create();
                     ProductStock stck = gson.fromJson(reader, ProductStock.class);
                     content.close();

                     handlePostsList(stck);
                 } catch (Exception ex) {
                     //Log.e(TAG, "Failed to parse JSON due to: " + ex);
                     //failedLoadingPosts();
                 }
             } else {
                 //Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                 //failedLoadingPosts();
             }
         } catch(Exception ex) {
             //Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
             //failedLoadingPosts();
         }
         return null;
     }
}
