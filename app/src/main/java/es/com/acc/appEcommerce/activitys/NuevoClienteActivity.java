package es.com.acc.appEcommerce.activitys;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.AddUserResponse;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Add_User;

/**
 * Created by Imanol on 20/05/2015.
 */
public class NuevoClienteActivity extends FragmentActivity
{
    private JSONTask_Add_User json;

    private EditText nombre;
    private EditText apellido1;
    private EditText apellido2;
    private Spinner  genre;
    private Button   birthbut;
    private EditText email;
    private EditText mobile;
    private EditText pasw1;
    private EditText pasw2;

    private CheckBox chk1;
    private CheckBox chk2;

    private boolean birthsetted = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cuenta_nuevo);

        nombre = (EditText)findViewById(R.id.nombre);
        apellido1 = (EditText)findViewById(R.id.apellido1);
        apellido2 = (EditText)findViewById(R.id.apellido2);
        email = (EditText)findViewById(R.id.email);

        pasw1 = (EditText)findViewById(R.id.password1);
        pasw2 = (EditText)findViewById(R.id.password2);

        mobile = (EditText)findViewById(R.id.mobile);

        genre = (Spinner)findViewById(R.id.genero);
        birthbut = (Button)findViewById(R.id.datepick);

        chk1 = (CheckBox)findViewById(R.id.terminos);
        chk2 = (CheckBox)findViewById(R.id.promociones);

        List<String> list = new ArrayList<String>();
        if(GlobalVars.idioma == 1)
        {
            nombre.setHint(getResources().getString(R.string.cuenta_nombre_eu));
            apellido1.setHint(getResources().getString(R.string.cuenta_apellidos_1_eu));
            apellido2.setHint(getResources().getString(R.string.cuenta_apellidos_2_eu));
            email.setHint(getResources().getString(R.string.cuenta_email_eu));
            mobile.setHint(getResources().getString(R.string.cuenta_telefono_eu));
            pasw1.setHint(getResources().getString(R.string.cuenta_password_eu));
            pasw2.setHint(getResources().getString(R.string.cuenta_re_password_eu));
            chk1.setText(getResources().getString(R.string.cuenta_check_1_eu));
            chk2.setText(getResources().getString(R.string.cuenta_check_2_eu));
            birthbut.setText(getResources().getString(R.string.cuenta_birth_date_eu));

            list.add(getResources().getString(R.string.cuenta_genero_eu));
            list.add(getResources().getString(R.string.cuenta_male_eu));
            list.add(getResources().getString(R.string.cuenta_female_eu));
        }
        else
        {
            nombre.setHint(getResources().getString(R.string.cuenta_nombre));
            apellido1.setHint(getResources().getString(R.string.cuenta_apellidos_1));
            apellido2.setHint(getResources().getString(R.string.cuenta_apellidos_2));
            email.setHint(getResources().getString(R.string.cuenta_email));
            mobile.setHint(getResources().getString(R.string.cuenta_telefono));
            pasw1.setHint(getResources().getString(R.string.cuenta_password));
            pasw2.setHint(getResources().getString(R.string.cuenta_re_password));
            chk1.setText(getResources().getString(R.string.cuenta_check_1));
            chk2.setText(getResources().getString(R.string.cuenta_check_2));
            birthbut.setText(getResources().getString(R.string.cuenta_birth_date));

            list.add(getResources().getString(R.string.cuenta_genero));
            list.add(getResources().getString(R.string.cuenta_male));
            list.add(getResources().getString(R.string.cuenta_female));
        }

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this, R.layout.my_spinner_item, list);
        spinnerArrayAdapter1.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        genre.setAdapter(spinnerArrayAdapter1);

        genre.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                if (position > 0) {

                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        birthsetted = false;
    }

    public void  onBackPressed () {
        ((CuentaActivity)getParent()).onBackPressed();
    }

    public void setdateBut(String str)
    {
        birthsetted = true;
        birthbut.setText(str);
    }

    public void xmlCharged(AddUserResponse addUserRes)
    {
        if(addUserRes.Error == 0)
        {
            GlobalVars.UserData.ID = addUserRes.Id;
            GlobalVars.UserData.Birthdate = birthbut.getText().toString();
            GlobalVars.UserData.Email = email.getText().toString();
            GlobalVars.UserData.Genre = genre.getSelectedItemPosition();
            GlobalVars.UserData.Name = nombre.getText().toString();
            GlobalVars.UserData.Surname1 = apellido1.getText().toString();
            GlobalVars.UserData.Surname2 = apellido2.getText().toString();
            GlobalVars.UserData.Telephone = mobile.getText().toString();
            GlobalVars.UserData.addToNewsletter = chk2.isChecked();

            SharedPreferences mPrefs = getSharedPreferences("mypref", 0);

            SharedPreferences.Editor mEditor = mPrefs.edit();
            mEditor.putString("birthdate", GlobalVars.UserData.Birthdate).commit();
            mEditor.putString("email", GlobalVars.UserData.Email).commit();
            mEditor.putInt("genre", GlobalVars.UserData.Genre).commit();
            mEditor.putInt("id", GlobalVars.UserData.ID).commit();
            mEditor.putString("name", GlobalVars.UserData.Name).commit();
            mEditor.putString("surname1", GlobalVars.UserData.Surname1).commit();
            mEditor.putString("surname2", GlobalVars.UserData.Surname2).commit();
            mEditor.putString("telephone", GlobalVars.UserData.Telephone).commit();
            mEditor.putBoolean("addnewsletter", GlobalVars.UserData.addToNewsletter).commit();
            mEditor.putString("password", GlobalVars.password).commit();

            Intent nextScreen = new Intent(getApplicationContext(), CuentaActivity.class);
            startActivity(nextScreen);
        }
        else if(addUserRes.Error == 1)
        {
            Toast.makeText(getApplicationContext(), "Error: La cuenta "+email.getText().toString()+" ya existe", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Se ha producido un error", Toast.LENGTH_SHORT).show();
        }
    }

    public void RegisterClicked(View v)
    {
        if((nombre.getText().length()<1)||
            (apellido1.getText().length()<1)||
            (apellido2.getText().length()<1)||
            (email.getText().length()<1)||
            (mobile.getText().length()<1)||
            (pasw1.getText().length()<1)||
            (pasw2.getText().length()<1)||
            (genre.getSelectedItemPosition()==0)||
            (birthsetted==false))
        {
            Toast.makeText(getApplicationContext(), "Por favor, rellena todos los campos", Toast.LENGTH_SHORT).show();
        }
        else if(pasw1.getText().toString().equals(pasw2.getText().toString()))
        {
            if(chk1.isChecked()) {
                json = new JSONTask_Add_User(this, NuevoClienteActivity.this, 0, nombre.getText().toString(), apellido1.getText().toString(), apellido2.getText().toString(), email.getText().toString(), pasw1.getText().toString(), mobile.getText().toString(), birthbut.getText().toString(), genre.getSelectedItemPosition(), chk2.isChecked(), GlobalVars.shopID);
                json.execute();
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Debe aceptar las condiciones", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
        }
    }

    public void showTimePickerDialog(View v)
    {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, day, month);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user

            ((NuevoClienteActivity)getActivity()).setdateBut(String.valueOf(year)+"-"+String.valueOf(month+1)+"-"+String.valueOf(day));
        }
    }
}