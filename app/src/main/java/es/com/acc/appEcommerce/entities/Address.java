package es.com.acc.appEcommerce.entities;

/**
 * Created by Imanol on 30/04/2015.
 */
public class Address
{
    public String AddressName;
    public String City;
    public String Contact;
    public int Country;
    public int Id;
    public Boolean IsDefault;
    public String Observation;
    public int Province;
    public String Zip;

    public Address()
    {

    }
}
