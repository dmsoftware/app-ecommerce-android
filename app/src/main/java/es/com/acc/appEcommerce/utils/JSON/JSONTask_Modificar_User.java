package es.com.acc.appEcommerce.utils.JSON;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import es.com.acc.appEcommerce.activitys.MisDatosActivity;
import es.com.acc.appEcommerce.entities.AddUserResponse;


public class JSONTask_Modificar_User extends AsyncTask<Void, Void, String>
{
    private Context context;
    private MisDatosActivity PrSh;
    private static final String TAG = "PostFetcher";
    private Boolean UserLoaded = false;
    public static final String SERVER_URL = "-- url del servicio --";
    private AddUserResponse addUserRes;

    private int    Id;
    private String name;
    private String surname1;
    private String surname2;
    private String email;
    private String password;
    private String mobile;
    private String birthdate;
    private int    genre;
    private boolean addToNewsletter;
    private int    shopId;

    public JSONTask_Modificar_User(MisDatosActivity A, Context mContext, int _id, String _name, String _surname1, String _surname2, String _email, String _password, String _mobile, String _birthdate, int _genre, boolean _addToNewsletter, int _shopId)
    {
        this.PrSh = A;
        this.context = mContext;
        UserLoaded = false;

        Id = _id;
        name = _name;
        surname1 = _surname1;
        surname2 = _surname2;
        email = _email;
        password = _password;
        mobile = _mobile;
        birthdate = _birthdate;
        genre = _genre;
        addToNewsletter = _addToNewsletter;
        shopId = _shopId;
    }

    private void handlePostsList(AddUserResponse posts)
    {
        this.addUserRes = posts;
    }

    //private void failedLoadingPosts()
    //{
    //    Log.d("Error", "Post Failed");
    //}

    @Override
    protected void onPostExecute(String result)
    {
        if(UserLoaded==false)
        {
            UserLoaded = true;
            PrSh.xmlCharged(addUserRes);
        }
    }

     @Override
     protected String doInBackground(Void... params)
     {
         try {
             //Create an HTTP client
             HttpClient client = new DefaultHttpClient();

             String pathURL = SERVER_URL;
             pathURL += "&Id=";
             pathURL += Id;
             pathURL += "&name=";
             pathURL += name;
             pathURL += "&surname1=";
             pathURL += surname1;
             pathURL += "&surname2=";
             pathURL += surname2;
             pathURL += "&email=";
             pathURL += email;
             pathURL += "&password=";
             pathURL += password;
             pathURL += "&mobile=";
             pathURL += mobile;
             pathURL += "&birthdate=";
             pathURL += birthdate;
             pathURL += "&genre=";
             pathURL += genre;
             pathURL += "&addToNewsletter=";
             pathURL += addToNewsletter;
             pathURL += "&shopId=";
             pathURL += shopId;

             System.out.println(" ::::::::::::::::::::::::::::: Product Stock: >> "+pathURL);

             HttpPost post = new HttpPost(pathURL);

             //Perform the request and check the status code
             HttpResponse response = client.execute(post);
             StatusLine statusLine = response.getStatusLine();
             if(statusLine.getStatusCode() == 200) {
                 HttpEntity entity = response.getEntity();
                 InputStream content = entity.getContent();

                 try {
                     //Read the server response and attempt to parse it as JSON
                     Reader reader = new InputStreamReader(content);

                     GsonBuilder gsonBuilder = new GsonBuilder();
                     gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                     Gson gson = gsonBuilder.create();
                     AddUserResponse log = gson.fromJson(reader, AddUserResponse.class);
                     content.close();

                     handlePostsList(log);
                 } catch (Exception ex) {
                     //Log.e(TAG, "Failed to parse JSON due to: " + ex);
                     //failedLoadingPosts();
                 }
             } else {
                 //Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                 //failedLoadingPosts();
             }
         } catch(Exception ex) {
             //Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
             //failedLoadingPosts();
         }
         return null;
     }
}
