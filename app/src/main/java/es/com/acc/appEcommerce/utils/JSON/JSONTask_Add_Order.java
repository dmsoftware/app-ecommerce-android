package es.com.acc.appEcommerce.utils.JSON;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import es.com.acc.appEcommerce.activitys.CompraPagoActivity;
import es.com.acc.appEcommerce.entities.RespuestaPedido;


public class JSONTask_Add_Order extends AsyncTask<Void, Void, String>
{
    private CompraPagoActivity PrSh;
    private static final String TAG = "PostFetcher";
    private Boolean UserLoaded = false;
    public static final String SERVER_URL = "-- url del servicio --";
    private RespuestaPedido respuesta;

    private String cestaID;
    private String methPago;
    private String methEnvio;
    private String adressEnvio;
    private String user;
    private String shop;

    public JSONTask_Add_Order(CompraPagoActivity A, Context mContext, String cesta, String pago, String envio, String adress, String use, String sho)
    {
        this.PrSh = A;
        UserLoaded = false;
        cestaID = cesta;
        methPago=pago;
        methEnvio=envio;
        adressEnvio=adress;
        user=use;
        shop=sho;
    }

    private void handlePostsList(RespuestaPedido post)
    {
        this.respuesta = post;
    }

    //private void failedLoadingPosts()
    //{
    //    Log.d("Error", "Post Failed");
    //}

    @Override
    protected void onPostExecute(String result)
    {
        if(UserLoaded==false)
        {
            UserLoaded = true;
            PrSh.xmlRespuestaPedidoCharged(this.respuesta);
        }
    }

     @Override
     protected String doInBackground(Void... params)
     {
         try {
             //Create an HTTP client
             HttpClient client = new DefaultHttpClient();

             String pathURL = SERVER_URL;
             pathURL += "&ShoppingCartID=";
             pathURL +=  cestaID;
             pathURL += "&PaymentID=";
             pathURL += methPago;
             pathURL += "&ShippingID=";
             pathURL +=  methEnvio;
             pathURL += "&ShippingAddress=";
             pathURL +=  adressEnvio;
             pathURL += "&UserID=";
             pathURL +=  user;
             pathURL += "&shopID=";
             pathURL +=  shop;

             System.out.println(" ::::::::::::::::::::::::::::: Get respuesta order: >> "+pathURL);

             HttpPost post = new HttpPost(pathURL);

             //Perform the request and check the status code
             HttpResponse response = client.execute(post);
             StatusLine statusLine = response.getStatusLine();
             if(statusLine.getStatusCode() == 200) {
                 HttpEntity entity = response.getEntity();
                 InputStream content = entity.getContent();

                 try {
                     //Read the server response and attempt to parse it as JSON
                     Reader reader = new InputStreamReader(content);

                     GsonBuilder gsonBuilder = new GsonBuilder();
                     gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                     Gson gson = gsonBuilder.create();

                     RespuestaPedido co = gson.fromJson(reader, RespuestaPedido.class);
                     content.close();

                     handlePostsList(co);
                 } catch (Exception ex) {
                     //Log.e(TAG, "Failed to parse JSON due to: " + ex);
                     //failedLoadingPosts();
                 }
             } else {
                 //Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                 //failedLoadingPosts();
             }
         } catch(Exception ex) {
             //Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
             //failedLoadingPosts();
         }
         return null;
     }
}
