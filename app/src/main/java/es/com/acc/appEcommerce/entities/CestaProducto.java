package es.com.acc.appEcommerce.entities;

/**
 * Created by Imanol on 22/05/2015.
 */
public class CestaProducto
{
    int _product_id;
    String _talla_id;
    int _cantidad;

    public CestaProducto()
    {

    }

    public CestaProducto(int id, String talla, int cant)
    {
        this._product_id = id;
        this._talla_id = talla;
        this._cantidad = cant;
    }

    // getting Id
    public int getId(){
        return this._product_id;
    }

    // setting id
    public void setId(int id){
        this._product_id = id;
    }

    // getting Talla
    public String getTalla(){
        return this._talla_id;
    }

    // setting Talla
    public void setTalla(String talla){
        this._talla_id = talla;
    }

    // getting Cantidad
    public int getCantidad(){
        return this._cantidad;
    }

    // setting Cantidad
    public void setCantidad(int cant){
        this._cantidad = cant;
    }
}
