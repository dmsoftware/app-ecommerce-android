package es.com.acc.appEcommerce.entities;

/**
 * Created by Imanol on 01/06/2015.
 */
public class CestaCompra {
    public int Id;
    public int Availability;
    public int ItemCount;
    public CestaItem[] Items;
}
