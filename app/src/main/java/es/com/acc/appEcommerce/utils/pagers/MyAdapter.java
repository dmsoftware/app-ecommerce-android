package es.com.acc.appEcommerce.utils.pagers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import es.com.acc.appEcommerce.R;

/**
 * Created by Imanol on 23/04/2015.
 */

public class MyAdapter extends BaseAdapter {

    Context context;
    String[] data;
    int [] images;

    private static LayoutInflater inflater = null;

    public MyAdapter(Context context, String[] data, int[] images) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        this.images = images;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount(){
        // TODO Auto-generated method stub
        return data.length;
    }

    @Override
    public Object getItem(int position){
        // TODO Auto-generated method stub
        return data[position];
    }

    @Override
    public long getItemId(int position){
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;

        if(images[position] != -1)
        {
            //if (vi == null)
                vi = inflater.inflate(R.layout.cell_special, null);

            TextView text = (TextView) vi.findViewById(R.id.text1);
            text.setText(data[position]);

            ImageView img = (ImageView) vi.findViewById(R.id.imageView);
            if(img!=null) {
                img.setImageResource(images[position]);
            }
        }
        else
        {
            //if (vi == null)
                vi = inflater.inflate(R.layout.cell_normal, null);

            TextView text = (TextView) vi.findViewById(R.id.text1);
            text.setText(data[position]);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)text.getLayoutParams();
            params.setMargins(60, 0, 0, 0); //substitute parameters for left, top, right, bottom
            text.setLayoutParams(params);

            /*View linea = (View) vi.findViewById(R.id.line);
            linea.setVisibility(View.GONE);*/
        }
        return vi;
    }
}