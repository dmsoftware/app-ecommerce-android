package es.com.acc.appEcommerce.entities;

import java.util.List;

public class Categories {

	//@SerializedName("id")
	public int ID;
    public String MerchantID;
	public String Name;
	public int Parent;
	public int Position;

	public List<Tag> tags;

	public Categories() {

	}
}
