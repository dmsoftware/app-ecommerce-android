package es.com.acc.appEcommerce.entities;

/**
 * Created by Imanol on 27/05/2015.
 */
public class ProductStock
{
    public int AvailableUnits;
    public String Description;
    public int Id;
    public float OriginalPrice;
    public float Price;

    public ProductStock() {

    }
}
