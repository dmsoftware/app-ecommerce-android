package es.com.acc.appEcommerce.entities;

/**
 * Created by Imanol on 30/04/2015.
 */
public class RespuestaPedido
{
    public String Status ;
    public String Description ;
    public String CRMOrderID  ;
    public String ERPOrderID  ;
    public String TPVCode  ;

    public RespuestaPedido()
    {

    }
}
