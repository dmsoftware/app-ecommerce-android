package es.com.acc.appEcommerce.utils.JSON;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.com.acc.appEcommerce.activitys.BaseActivity;
import es.com.acc.appEcommerce.entities.FiltrosRow;


public class JSONTask_Filtros extends AsyncTask<Void, Void, String>
{
    private Context context;
    private BaseActivity subCa;
    private static final String TAG = "PostFetcher";
    private Boolean FiltrLoaded = false;
    public static final String SERVER_URL = "-- url del servicio --";
    private List<FiltrosRow> Filtrs = new ArrayList<FiltrosRow>();

    private String Categorias;
    private String Filters;

    public JSONTask_Filtros(BaseActivity A, Context mContext, String Categories, String Filters){
        this.subCa = A;
        this.context = mContext;

        this.Categorias = Categories;
        this.Filters = Filters;

        FiltrLoaded = false;
    }

    private void handlePostsList(List<FiltrosRow> posts)
    {
        this.Filtrs = posts;
    }

    private void failedLoadingPosts() {
        //Log.d("Error", "Post Failed");
    }

    @Override
    protected void onPostExecute(String result)
    {
        if(FiltrLoaded==false)
        {
            FiltrLoaded = true;
            subCa.xmlFiltrsCharged(Filtrs);
        }
    }

     @Override
     protected String doInBackground(Void... params) {
         try {
             //Create an HTTP client
             HttpClient client = new DefaultHttpClient();

             String pathURL = SERVER_URL;
             pathURL += "&categories=";
             pathURL += this.Categorias;
             if(this.Filters.length()>0) {
                 pathURL += "&filters=";
                 pathURL += this.Filters;
             }

             HttpPost post = new HttpPost(pathURL);

             System.out.println(" ::::::::::::::::::::::::::::: Get filtros : >> "+pathURL);

             //Perform the request and check the status code
             HttpResponse response = client.execute(post);
             StatusLine statusLine = response.getStatusLine();

             if(statusLine.getStatusCode() == 200)
             {
                 HttpEntity entity = response.getEntity();
                 InputStream content = entity.getContent();

                 try {
                     //Read the server response and attempt to parse it as JSON
                     Reader reader = new InputStreamReader(content);

                     GsonBuilder gsonBuilder = new GsonBuilder();
                     gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                     Gson gson = gsonBuilder.create();

                     //System.out.println(" ::::::::::::::::::::::::::::: reader GSON 1 : >> "+gson.toString());

                     List<FiltrosRow> Prods = Arrays.asList(gson.fromJson(reader, FiltrosRow[].class));
                     content.close();

                     //System.out.println(" ::::::::::::::::::::::::::::: reader GSON  2: >> "+reader);

                     handlePostsList(Prods);
                 } catch (Exception ex) {
                     Log.e(TAG, "Failed to parse JSON due to: " + ex);
                     failedLoadingPosts();
                 }
             } else {
                 Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                 failedLoadingPosts();
             }
         } catch(Exception ex) {
             Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
             failedLoadingPosts();
         }
         return null;
     }

}
