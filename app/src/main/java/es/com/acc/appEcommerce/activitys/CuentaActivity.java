package es.com.acc.appEcommerce.activitys;

import android.app.LocalActivityManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;

import java.util.Locale;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.UserLogin;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Login;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Recordar;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

/**
 * Created by Imanol on 20/05/2015.
 */
public class CuentaActivity  extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{
    TabHost tabHost;
    LocalActivityManager mLocalActivityManager;

    private JSONTask_Login loginjson;
    private JSONTask_Recordar recordarjson;

    private TextView logedText;
    private Button logoutBot;
    private Button misdatosBot;
    private Button misdireciconesBot;
    private Button cambioBot;
    private Button devolucionBot;
    private Button mispedidosBot;
    private Button favoritosBot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Secondary Activity", String.valueOf(GlobalVars.momentSection));
        setContentView(R.layout.activity_cuenta);

        dataCharged = true;

        mTitle = MainSections[GlobalVars.momentSection];

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                CategoriasPrincipales,R.drawable.ic_drawer);

        logedText= (TextView) findViewById(R.id.loged_text);

        logoutBot = (Button)findViewById(R.id.logoutbot);
        misdatosBot = (Button)findViewById(R.id.misdatosbot);
        misdireciconesBot = (Button)findViewById(R.id.misdireccionesbot);
        mispedidosBot = (Button)findViewById(R.id.mispedidosbot);
        favoritosBot = (Button)findViewById(R.id.favoritosbot);

        tabHost = (TabHost) findViewById(android.R.id.tabhost);

        if(GlobalVars.UserData.ID > -1){
            tabHost.setVisibility(View.INVISIBLE);
            if(GlobalVars.idioma == 1) {
                logedText.setText(getResources().getString(R.string.cuenta_bienvenido_eu) + " " + GlobalVars.UserData.Name);
                misdatosBot.setText(getResources().getString(R.string.cuenta_misdatos_eu));
                misdireciconesBot.setText(getResources().getString(R.string.cuenta_misdirecciones_eu));
                mispedidosBot.setText(getResources().getString(R.string.cuenta_mispedidos_eu));
                favoritosBot.setText(getResources().getString(R.string.menu_favoritos_eu));
            }
            else {
                logedText.setText(getResources().getString(R.string.cuenta_bienvenido) + " " + GlobalVars.UserData.Name);
                misdatosBot.setText(getResources().getString(R.string.cuenta_misdatos));
                misdireciconesBot.setText(getResources().getString(R.string.cuenta_misdirecciones));
                mispedidosBot.setText(getResources().getString(R.string.cuenta_mispedidos));
                favoritosBot.setText(getResources().getString(R.string.menu_favoritos));
            }
        }
        else {
            initTabs(savedInstanceState);
            logoutBot.setVisibility(View.INVISIBLE);
            logedText.setVisibility(View.INVISIBLE);
            misdatosBot.setVisibility(View.INVISIBLE);
            misdireciconesBot.setVisibility(View.INVISIBLE);
            mispedidosBot.setVisibility(View.INVISIBLE);
            favoritosBot.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);

        String lag = "es";
        if(GlobalVars.idioma == 1)lag = "eu";
        String lagUser = "";
        if(GlobalVars.UserData!=null){
            lagUser = String.valueOf(GlobalVars.UserData.ID);
        }

        // Set screen name.
        tracker.setScreenName("VistaCuenta");

        // Send a screen view.
        tracker.send(new HitBuilders.ScreenViewBuilder()
                .setCustomDimension(1, lag)
                .setCustomDimension(2, lagUser)
                .setCustomDimension(3, "(not set)")
                .setCustomDimension(4, "(not set)")
                .setCustomDimension(5, "(not set)")
                .setCustomDimension(6, "(not set)")
                .setCustomDimension(7, "(not set)")
                .setCustomDimension(8, Locale.getDefault().getLanguage())
                .build());
    }

    public void xmlPasswordCharged(int respuesta)
    {
        if(respuesta == 1)
        {
            if(GlobalVars.idioma == 1)
                Toast.makeText(CuentaActivity.this, getResources().getString(R.string.cuenta_password_ok_eu), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(CuentaActivity.this, getResources().getString(R.string.cuenta_password_ok), Toast.LENGTH_SHORT).show();
        }
        else
        {
            if(GlobalVars.idioma == 1)
                Toast.makeText(CuentaActivity.this, getResources().getString(R.string.cuenta_password_ko_eu), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(CuentaActivity.this, getResources().getString(R.string.cuenta_password_ko), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void xmlLoginCharged(UserLogin log)
    {
        if(log.ID == -1)
        {
            if(GlobalVars.idioma == 1)
                Toast.makeText(CuentaActivity.this, getResources().getString(R.string.cuenta_login_ko_eu), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(CuentaActivity.this, getResources().getString(R.string.cuenta_login_ko), Toast.LENGTH_SHORT).show();
        }
        else {
            GlobalVars.UserData = log;

            SharedPreferences mPrefs = getSharedPreferences("mypref", 0);

            SharedPreferences.Editor mEditor = mPrefs.edit();
            mEditor.putString("birthdate", log.Birthdate).commit();
            mEditor.putString("email", log.Email).commit();
            mEditor.putInt("genre", log.Genre).commit();
            mEditor.putInt("id", log.ID).commit();
            mEditor.putString("name", log.Name).commit();
            mEditor.putString("surname1", log.Surname1).commit();
            mEditor.putString("surname2", log.Surname2).commit();
            mEditor.putString("telephone", log.Telephone).commit();
            mEditor.putBoolean("addnewsletter", log.addToNewsletter).commit();
            mEditor.putString("password", GlobalVars.password).commit();

            tabHost.setVisibility(View.INVISIBLE);
            logedText.setVisibility(View.VISIBLE);

            logoutBot.setVisibility(View.VISIBLE);
            misdatosBot.setVisibility(View.VISIBLE);
            misdireciconesBot.setVisibility(View.VISIBLE);
            mispedidosBot.setVisibility(View.VISIBLE);
            favoritosBot.setVisibility(View.VISIBLE);

            if (GlobalVars.idioma == 1) {
                logedText.setText(getResources().getString(R.string.cuenta_bienvenido_eu) +" "+ log.Name);
                misdatosBot.setText(getResources().getString(R.string.cuenta_misdatos_eu));
                misdireciconesBot.setText(getResources().getString(R.string.cuenta_misdirecciones_eu));
                mispedidosBot.setText(getResources().getString(R.string.cuenta_mispedidos_eu));
            }
            else
            {
                logedText.setText(getResources().getString(R.string.cuenta_bienvenido) +" "+ log.Name);
                misdatosBot.setText(getResources().getString(R.string.cuenta_misdatos));
                misdireciconesBot.setText(getResources().getString(R.string.cuenta_misdirecciones));
                mispedidosBot.setText(getResources().getString(R.string.cuenta_mispedidos));
            }
            Intent nextScreen;

            if(GlobalVars.momentSection == GlobalVars.Categorias.size()+2){
                nextScreen = new Intent(getApplicationContext(), MisDatosActivity.class);
                startActivity(nextScreen);
                finish();
            }else if(GlobalVars.momentSection == GlobalVars.Categorias.size()+3) {
                nextScreen = new Intent(getApplicationContext(), MisDireccionesActivity.class);
                startActivity(nextScreen);
                finish();
            }else if(GlobalVars.momentSection == GlobalVars.Categorias.size()+4){
                nextScreen = new Intent(getApplicationContext(), MisPedidosActivity.class);
                startActivity(nextScreen);
                finish();
            }else if(GlobalVars.momentSection == GlobalVars.Categorias.size()+5){
                nextScreen = new Intent(getApplicationContext(), FavoritoActivity.class);
                startActivity(nextScreen);
                finish();
            }
        }
        setRefreshing(false);
    }
    /*
    @Override
    protected void onResume() {
        mLocalActivityManager.dispatchResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mLocalActivityManager.dispatchPause(isFinishing());
        super.onPause();
    }*/

    private void initTabs(Bundle savedInstanceState) {
        Resources res = getResources();                     // Resource object to get Drawables
        mLocalActivityManager = new LocalActivityManager(this, false);
        mLocalActivityManager.dispatchCreate(savedInstanceState);
        tabHost.setup(mLocalActivityManager);

        TabHost.TabSpec spec;                               // Resusable TabSpec for each tab
        Intent intent;                                      // Reusable Intent for each tab

        intent = new Intent().setClass(this, InicioSesionActivity.class);

        String lag;
        if(GlobalVars.idioma == 1)
            lag = getResources().getString(R.string.cuenta_inicio_eu);
        else
            lag = getResources().getString(R.string.cuenta_inicio);

        spec = tabHost.newTabSpec("inicio").setIndicator(lag,
                res.getDrawable(R.drawable.cuenta))
                .setContent(intent);
        tabHost.addTab(spec);

        if(GlobalVars.idioma == 1)
            lag = getResources().getString(R.string.cuenta_nuevo_eu);
        else
            lag = getResources().getString(R.string.cuenta_nuevo);

        intent = new Intent().setClass(this, NuevoClienteActivity.class);
        spec = tabHost.newTabSpec("nuevo").setIndicator(lag,
                res.getDrawable(R.drawable.categorias))
                .setContent(intent);
        tabHost.addTab(spec);

        TabWidget widget = tabHost.getTabWidget();

        for(int i = 0; i < widget.getChildCount(); i++) {
            View v = widget.getChildAt(i);

            // Look for the title view to ensure this is an indicator and not a divider.
            TextView tv = (TextView)v.findViewById(android.R.id.title);
            if(tv == null) {
                continue;
            }
            v.setBackgroundResource(R.drawable.apptheme_tab_indicator_holo);
        }

        tabHost.setCurrentTab(0);

        setRefreshing(false);
    }

    @Override
    public void  InicioClick (String id, String psw)
    {
        setRefreshing(true);
        GlobalVars.password = psw;
        loginjson = new JSONTask_Login(this,CuentaActivity.this,id,psw);
        loginjson.execute();
    }

    @Override
    public void RecordarClicked (String id)
    {
        recordarjson = new JSONTask_Recordar(this,CuentaActivity.this,id);
        recordarjson.execute();
    }

    public void logoutClicked(View view)
    {
        GlobalVars.UserData.ID = -1;
        SharedPreferences mPrefs = getSharedPreferences("mypref", 0);

        SharedPreferences.Editor mEditor = mPrefs.edit();
        mEditor.putString("birthdate", "").commit();
        mEditor.putString("email", "").commit();
        mEditor.putInt("genre", 0).commit();
        mEditor.putInt("id", -1).commit();
        mEditor.putString("name", "").commit();
        mEditor.putString("surname1", "").commit();
        mEditor.putString("surname2", "").commit();
        mEditor.putString("telephone", "").commit();
        mEditor.putBoolean("addnewsletter", false).commit();
        mEditor.putString("password", "").commit();

        Intent nextScreen = new Intent(getApplicationContext(), CuentaActivity.class);
        startActivity(nextScreen);
    }

    public void misdatosClicked(View view)
    {
        Intent nextScreen = new Intent(getApplicationContext(), MisDatosActivity.class);
        startActivity(nextScreen);
    }

    public void misdireccionesClicked(View view)
    {
        Intent nextScreen = new Intent(getApplicationContext(), MisDireccionesActivity.class);
        startActivity(nextScreen);
    }

    public void mispedidosClicked(View view)
    {
        Intent nextScreen = new Intent(getApplicationContext(), MisPedidosActivity.class);
        startActivity(nextScreen);
    }

    public void favoritosClicked(View view)
    {
        Intent nextScreen = new Intent(getApplicationContext(), FavoritoActivity.class);
        startActivity(nextScreen);
    }
}
