package es.com.acc.appEcommerce.entities;

import java.util.List;

/**
 * Created by Imanol on 30/04/2015.
 */
public class FiltrosRow
{
    public String Name;
    public String Type;
    public String Selected;
    public List<Filtro> Value;
}
