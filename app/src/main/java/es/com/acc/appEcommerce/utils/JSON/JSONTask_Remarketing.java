package es.com.acc.appEcommerce.utils.JSON;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;

import es.com.acc.appEcommerce.activitys.DestacadosActivity;
import es.com.acc.appEcommerce.entities.Producto;


public class JSONTask_Remarketing extends AsyncTask<Void, Void, String>{

    private Context context;
    private DestacadosActivity subCa;
    private static final String TAG = "PostFetcher";
    private Boolean CatsLoaded = false;
    public static final String SERVER_URL = "-- url del servicio --";
    private List<Producto> Products;

    private String userID;
    private String Filter;
    private String Order;
    private String maxRes;
    private String currPage;

    public JSONTask_Remarketing(DestacadosActivity A, Context mContext, String userID){
        this.subCa = A;
        this.context = mContext;

        this.userID = userID;
        CatsLoaded = false;
        Products = null;
    }

    private void handlePostsList(List<Producto> posts)
    {
        this.Products = posts;
    }

    private void failedLoadingPosts() {
        //Log.d("Error", "Post Failed");
    }

    @Override
    protected void onPostExecute(String result)
    {
        if((CatsLoaded==false)&&(Products!=null)) {
            CatsLoaded = true;

            subCa.RemarketinCharged(Products);
        }
    }

     @Override
     protected String doInBackground(Void... params) {
         try {
             //Create an HTTP client
             HttpClient client = new DefaultHttpClient();

             String pathURL = SERVER_URL;
             pathURL += userID;

             HttpPost post = new HttpPost(pathURL);
             System.out.println(" ::::::::::::::::::::::::::::: POST GSON 0 : >> "+pathURL);

             //Perform the request and check the status code
             HttpResponse response = client.execute(post);
             StatusLine statusLine = response.getStatusLine();

             if(statusLine.getStatusCode() == 200)
             {
                 HttpEntity entity = response.getEntity();
                 InputStream content = entity.getContent();

                 try {
                     //Read the server response and attempt to parse it as JSON
                     Reader reader = new InputStreamReader(content);

                     GsonBuilder gsonBuilder = new GsonBuilder();
                     gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                     Gson gson = gsonBuilder.create();

                     //System.out.println(" ::::::::::::::::::::::::::::: reader GSON 1 : >> "+gson.toString());

                     List<Producto> Prods = Arrays.asList(gson.fromJson(reader, Producto[].class));
                     content.close();

                     //System.out.println(" ::::::::::::::::::::::::::::: reader GSON  2: >> "+reader);

                     handlePostsList(Prods);
                 } catch (Exception ex) {
                     Log.e(TAG, "Failed to parse JSON due to: " + ex);
                     failedLoadingPosts();
                 }
             } else {
                 Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                 failedLoadingPosts();
             }
         } catch(Exception ex) {
             Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
             failedLoadingPosts();
         }
         return null;
     }

}
