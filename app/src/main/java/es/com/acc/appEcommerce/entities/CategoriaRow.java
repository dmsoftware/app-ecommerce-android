package es.com.acc.appEcommerce.entities;

import java.util.List;

/**
 * Created by Imanol on 24/04/2015.
 */
public class CategoriaRow
{
    public Categories principal;
    public List<CategoriaRow> secundarios;
}
