package es.com.acc.appEcommerce.utils.pagers;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.activitys.ProductSheetActivity;
import es.com.acc.appEcommerce.utils.ImageLoader;

public class CustomPageAdapter extends PagerAdapter {

    public final List<String> urls;
    public final List<Bitmap> bitmaps;
    public final int anchuraScreen;
    public final int alturaFrame;
    public final Context context;
    public LayoutInflater mLayoutInflater;
    public ViewPager RootPager;
    public boolean shared = false;

    public ArrayList<View> views = new ArrayList<View>();

	public CustomPageAdapter(Context context, List<String> urls, int anchura, int altura, ViewPager pag) {
		super();
        this.RootPager = pag;
		this.urls = urls;
		this.context = context;
        this.anchuraScreen = anchura;
        this.alturaFrame = altura;
        this.bitmaps = new ArrayList<Bitmap>();

        initLoaders();
	}

    public void initLoaders()
    {
        for(final String url:urls)
        {
            mLayoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View page = mLayoutInflater.inflate(R.layout.product_image_cell, null);

            ProgressBar proBar = (ProgressBar) page.findViewById(R.id.imageLoadingPanel);
            proBar.setVisibility(View.VISIBLE);

            ImageLoader imgLod = new ImageLoader(CustomPageAdapter.this, page, RootPager);
            imgLod.execute(url);
        }
    }

	@Override
	public void destroyItem(ViewGroup  collection, int position, Object view) {
		collection.removeView(views.get (position));
	}

    //-----------------------------------------------------------------------------
    // Add "view" to right end of "views".
    // Returns the position of the new view.
    // The app should call this to add pages; not used by ViewPager.
    public int addView (View v, Bitmap img)
    {
        if(shared==false) {
            ((ProductSheetActivity) context).setShareImage(img);
            shared = true;
        }
        return addView (v, img, views.size());
    }

    //-----------------------------------------------------------------------------
    // Add "view" at "position" to "views".
    // Returns position of new view.
    // The app should call this to add pages; not used by ViewPager.
    public int addView (View v, Bitmap img, int position)
    {
        ImageView image = (ImageView)v.findViewById(R.id.prodImg);
        image.setImageBitmap(img);
        image.getLayoutParams().width  = this.anchuraScreen;
        image.getLayoutParams().height = (int) (this.anchuraScreen*0.667);

        ProgressBar proBar = (ProgressBar)v.findViewById(R.id.imageLoadingPanel);
        proBar.setVisibility(View.GONE);

        v.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                //this will log the page number that was click
                Log.i("TAG", "This page was clicked: ");
            }
        });

        views.add (position, v);
        notifyDataSetChanged();
        return position;
    }

    public int removeView (ViewPager pager, View v)
    {
        return removeView (pager, views.indexOf (v));
    }

    public int removeView (ViewPager pager, int position)
    {
        pager.setAdapter (null);
        views.remove (position);
        pager.setAdapter (this);

        return position;
    }

	@Override
	public void finishUpdate(ViewGroup  arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public int getCount() {
		return views.size();
	}

	@TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
	public Object instantiateItem(ViewGroup collection, final int position)
    {
        View v = views.get (position);
        v.setId(position);
        collection.addView (v);

        return v;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		 return view==((RelativeLayout)object);
	}

	@Override
	public void restoreState(Parcelable arg0, ClassLoader arg1) {
		// TODO Auto-generated method stub
        //System.out.println(" :::::::::::::::::::: restoreState ::::::::: >> "+arg0+" :: "+arg1);
	}

	@Override
	public Parcelable saveState() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void startUpdate(ViewGroup  arg0) {
		// TODO Auto-generated method stub
        //System.out.println(" :::::::::::::::::::: UPDATEEEEE ::::::::: >> "+arg0);
	}


}
