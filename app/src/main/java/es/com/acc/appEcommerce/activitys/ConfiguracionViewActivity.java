package es.com.acc.appEcommerce.activitys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;

import java.util.Locale;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

public class ConfiguracionViewActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{
    private RadioButton euskaraB;
    private RadioButton castellanoB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dataCharged = true;

        setContentView(R.layout.activity_configuracion);

        mTitle = MainSections[GlobalVars.momentSection];

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                CategoriasPrincipales,R.drawable.ic_drawer);

        euskaraB = (RadioButton) findViewById(R.id.euskarabot);
        castellanoB = (RadioButton) findViewById(R.id.castellanobot);
    }

    @Override
    public void onStart() {
        super.onStart();

        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);

        String lag = "es";
        if(GlobalVars.idioma == 1)
        {
            lag = "eu";
            euskaraB.setChecked(true);
            castellanoB.setChecked(false);
        }
        else
        {
            euskaraB.setChecked(false);
            castellanoB.setChecked(true);
        }
        String lagUser = "";
        if(GlobalVars.UserData!=null){
            lagUser = String.valueOf(GlobalVars.UserData.ID);
        }

        // Set screen name.
        tracker.setScreenName("VistaConfiguracion");

        // Send a screen view.
        tracker.send(new HitBuilders.ScreenViewBuilder()
                .setCustomDimension(1, lag)
                .setCustomDimension(2, lagUser)
                .setCustomDimension(3, "(not set)")
                .setCustomDimension(4, "(not set)")
                .setCustomDimension(5, "(not set)")
                .setCustomDimension(6, "(not set)")
                .setCustomDimension(7, "(not set)")
                .setCustomDimension(8, Locale.getDefault().getLanguage())
                .build());
    }


    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        Intent intent;
        SharedPreferences mPrefs;
        SharedPreferences.Editor mEditor;

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.euskarabot:
                if (checked)
                    Toast.makeText(this, "Euskara aukeratua", Toast.LENGTH_SHORT).show();
                    GlobalVars.idioma = 1;

                    mPrefs= getSharedPreferences("mypref", 0);
                    mEditor = mPrefs.edit();
                    mEditor.putInt("idioma", 1).commit();

                    intent = new Intent(ConfiguracionViewActivity.this, ConfiguracionViewActivity.class);
                    startActivity(intent);
                    break;
            case R.id.castellanobot:
                if (checked)
                    Toast.makeText(this, "Castellano seleccionado", Toast.LENGTH_SHORT).show();
                    GlobalVars.idioma = 0;

                    mPrefs= getSharedPreferences("mypref", 0);
                    mEditor = mPrefs.edit();
                    mEditor.putInt("idioma", 0).commit();

                    intent = new Intent(ConfiguracionViewActivity.this, ConfiguracionViewActivity.class);
                    startActivity(intent);
                    break;
        }
    }
}