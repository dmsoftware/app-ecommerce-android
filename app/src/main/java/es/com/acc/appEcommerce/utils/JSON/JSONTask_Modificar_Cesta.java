package es.com.acc.appEcommerce.utils.JSON;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import es.com.acc.appEcommerce.activitys.BaseActivity;
import es.com.acc.appEcommerce.entities.CestaCompra;


public class JSONTask_Modificar_Cesta extends AsyncTask<Void, Void, String>{

    private Context context;
    private BaseActivity prorSheetAct;
    private static final String TAG = "PostFetcher";
    private Boolean CestaLoaded = false;
    public static final String SERVER_URL = "-- url del servicio --";
    private CestaCompra  Cesta;

    private String id;
    private String cartID;
    private String userID;
    private String units;
    private String stock;
    private String sizeId;
    private String sizeDisplay;
    private String availability;

    public JSONTask_Modificar_Cesta(BaseActivity A, Context mContext, int ID, int CartID, int UserID, int Units, int Stock, String SizeId, String SizeDisplay, int Availability)
    {
        this.prorSheetAct = A;
        this.context = mContext;

        this.id = String.valueOf(ID);
        this.cartID = String.valueOf(CartID);
        this.userID = String.valueOf(UserID);
        this.units = String.valueOf(Units);
        this.stock = String.valueOf(Stock);
        this.sizeId = SizeId;
        this.sizeDisplay = SizeDisplay;
        this.availability = String.valueOf(Availability);

        CestaLoaded = false;
        Cesta = null;
    }

    private void handlePostsList(CestaCompra  posts)
    {
        this.Cesta = posts;
    }

    private void failedLoadingPosts() {
        //Log.d("Error", "Post Failed");
    }

    @Override
    protected void onPostExecute(String result)
    {
        if((CestaLoaded==false)&&(Cesta!=null)) {
            CestaLoaded = true;
            prorSheetAct.xmlCestaCharged(Cesta);
        }
    }

     @Override
     protected String doInBackground(Void... params) {
         try {
             //Create an HTTP client
             HttpClient client = new DefaultHttpClient();

             String pathURL = SERVER_URL;
             pathURL += "&id=";
             pathURL += this.id;
             pathURL += "&units=";
             pathURL += this.units;
             pathURL += "&stock=";
             pathURL += this.stock;
             pathURL += "&sizeId=";
             pathURL += this.sizeId;
             pathURL += "&sizeDisplay=";
             pathURL += this.sizeDisplay;
             pathURL += "&shoppingCartId=";
             pathURL += this.cartID;
             pathURL += "&availability=";
             pathURL += this.availability;
             pathURL += "&user=";
             pathURL += this.userID;

             HttpPost post = new HttpPost(pathURL);
             System.out.println(" ::::::::::::::::::::::::::::: POST GSON 0 : >> "+pathURL);

             //Perform the request and check the status code
             HttpResponse response = client.execute(post);
             StatusLine statusLine = response.getStatusLine();

             if(statusLine.getStatusCode() == 200)
             {
                 HttpEntity entity = response.getEntity();
                 InputStream content = entity.getContent();

                 try {
                     //Read the server response and attempt to parse it as JSON
                     Reader reader = new InputStreamReader(content);

                     GsonBuilder gsonBuilder = new GsonBuilder();
                     gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                     Gson gson = gsonBuilder.create();

                     //List<Producto> Prod = Arrays.asList(gson.fromJson(reader, Producto[].class));
                     CestaCompra cesta = gson.fromJson(reader, CestaCompra.class);
                     content.close();

                     //System.out.println(" ::::::::::::::::::::::::::::: reader GSON  2: >> "+reader);

                     handlePostsList(cesta);
                 } catch (Exception ex) {
                     Log.e(TAG, "Failed to parse JSON due to: " + ex);
                     failedLoadingPosts();
                 }
             } else {
                 Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                 failedLoadingPosts();
             }
         } catch(Exception ex) {
             Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
             failedLoadingPosts();
         }
         return null;
     }

}
