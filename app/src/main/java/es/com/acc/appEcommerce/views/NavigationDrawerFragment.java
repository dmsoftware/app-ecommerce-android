package es.com.acc.appEcommerce.views;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.pagers.MyAdapter;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends Fragment {

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerListView;
    private View mFragmentContainerView;

    private int mCurrentSelectedPosition = 0;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;

    private String[] MainSections;
    private int[] MainCatImages;

    public NavigationDrawerFragment() {
    }

    public void open()
    {
        mDrawerLayout.openDrawer(Gravity.LEFT);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }

        // Select either the default item (0) or the last selected item.
        selectItem(mCurrentSelectedPosition);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mDrawerListView = (ListView) inflater.inflate(
                R.layout.fragment_navigation_drawer, container, false);
        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });

        return mDrawerListView;
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout, List<String> Cats, int icon)
    {
        int lag = Cats.size() + 20;
        MainSections = new String[lag];
        MainCatImages = new int[lag];

        MainCatImages[0] = R.drawable.categorias;
        MainCatImages[1] = -1;

        //MainCatImages = new ArrayList<int>();

        for (int i = 0; i < Cats.size(); i++) {
            MainSections[i+2] = Cats.get(i);
            MainCatImages[i+2] = -1;
        }
        int kont = Cats.size()+2;

        if(GlobalVars.idioma == 1) {
            MainSections[0] = getResources().getString(R.string.menu_section1_eu);
            MainSections[1] = getResources().getString(R.string.menu_section0_eu);
            MainSections[2] = "Gizona";
            MainSections[3] = "Emakumea";
            MainSections[4] = "Mutikoa";
            MainSections[5] = "Neskatoa";
            MainSections[6] = "Markak";
            MainSections[7] = "Outlet";
            //MainSections[7] = getResources().getString(R.string.menu_section2_eu);
            MainSections[8]  = getResources().getString(R.string.menu_section3_eu);
            MainSections[9]  = getResources().getString(R.string.cuenta_misdatos_eu);
            MainSections[10]  = getResources().getString(R.string.cuenta_misdirecciones_eu);
            MainSections[11] = getResources().getString(R.string.cuenta_mispedidos_eu);
            MainSections[12] = getResources().getString(R.string.menu_favoritos_eu);
            MainSections[13] = getResources().getString(R.string.menu_section4_eu);
            MainSections[14] = getResources().getString(R.string.menu_section5_eu);
            MainSections[15] = getResources().getString(R.string.menu_section6_eu);
            MainSections[16] = getResources().getString(R.string.menu_section7_eu);
            MainSections[17] = getResources().getString(R.string.menu_section8_eu);
            MainSections[18] = getResources().getString(R.string.menu_section9_eu);
            MainSections[19] = getResources().getString(R.string.menu_section10_eu);
            MainSections[20] = getResources().getString(R.string.menu_section11_eu);
            MainSections[21] = getResources().getString(R.string.menu_section12_eu);
            MainSections[22] = getResources().getString(R.string.menu_section13_eu);
            MainSections[23] = getResources().getString(R.string.menu_section14_eu);
            MainSections[24] = getResources().getString(R.string.menu_section15_eu);
            kont = 8;
        } else {
            MainSections[0] = getResources().getString(R.string.menu_section1);
            MainSections[1] = getResources().getString(R.string.menu_section0);

            //MainSections[kont] = getResources().getString(R.string.menu_section2);
            MainSections[kont]   = getResources().getString(R.string.menu_section3);
            MainSections[kont+1] = getResources().getString(R.string.cuenta_misdatos);
            MainSections[kont+2] = getResources().getString(R.string.cuenta_misdirecciones);
            MainSections[kont+3] = getResources().getString(R.string.cuenta_mispedidos);
            MainSections[kont+4] = getResources().getString(R.string.menu_favoritos);
            MainSections[kont+5] = getResources().getString(R.string.menu_section4);
            MainSections[kont+6] = getResources().getString(R.string.menu_section5);
            MainSections[kont+7] = getResources().getString(R.string.menu_section6);
            MainSections[kont+8] = getResources().getString(R.string.menu_section7);
            MainSections[kont+9] = getResources().getString(R.string.menu_section8);
            MainSections[kont+10] =getResources().getString(R.string.menu_section9);
            MainSections[kont+11] =getResources().getString(R.string.menu_section10);
            MainSections[kont+12] =getResources().getString(R.string.menu_section11);
            MainSections[kont+13] =getResources().getString(R.string.menu_section12);
            MainSections[kont+14] =getResources().getString(R.string.menu_section13);
            MainSections[kont+15] =getResources().getString(R.string.menu_section14);
            MainSections[kont+16] =getResources().getString(R.string.menu_section15);
        }
        MainCatImages[kont] = R.drawable.configuracion;
        MainCatImages[kont+1] = -1;
        MainCatImages[kont+2] = -1;
        MainCatImages[kont+3] = -1;
        MainCatImages[kont+4] = -1;
        MainCatImages[kont+5] = -1;
        MainCatImages[kont+6] = R.drawable.avisolegal;
        MainCatImages[kont+7] = -1;
        MainCatImages[kont+8] = -1;
        MainCatImages[kont+9] = -1;
        MainCatImages[kont+10] = -1;
        MainCatImages[kont+11] = -1;
        MainCatImages[kont+12] = -1;
        MainCatImages[kont+13] = -1;
        MainCatImages[kont+14] = -1;
        MainCatImages[kont+15] = -1;
        MainCatImages[kont+16] = -1;
        MainCatImages[kont+17] = -1;

        mDrawerListView.setAdapter(new MyAdapter(
                getActionBar().getThemedContext(),
                MainSections,
                MainCatImages));
        //mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);
        mDrawerListView.setItemChecked(GlobalVars.momentSection, true);

        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        //actionBar.setIcon(R.drawable.icono);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                icon,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }

                if (!mUserLearnedDrawer) {
                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager
                            .getDefaultSharedPreferences(getActivity());
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (mDrawerListView != null) {
            mDrawerListView.setItemChecked(position, true);
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the drawer is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
            inflater.inflate(R.menu.global, menu);
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Per the navigation drawer design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle(R.string.app_name);
    }

    private ActionBar getActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);
    }
}
