package es.com.acc.appEcommerce.entities;

/**
 * Created by Imanol on 30/04/2015.
 */
public class Pedido
{
    public int Availability;
    public int Id;
    public String Image;
    public String ItemBrand;
    public int ItemId;
    public String ItemName;
    public float OriginalPrice;
    public float Price;
    public String SizeDisplay;
    public String SizeId;
    public int Stock;
    public int Units;
    public String returnDate;
    public int returnUnits;

    public Pedido()
    {

    }
}
