package es.com.acc.appEcommerce.activitys.WebView;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.webkit.WebView;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.activitys.BaseActivity;
import es.com.acc.appEcommerce.entities.WebNode;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Get_Web_Node;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

public class AvisoLegalWebViewActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{
    private JSONTask_Get_Web_Node json;
    public String URLPath;

    public void onStart() {
        super.onStart();
        // The rest of your onStart() code.
        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Secondary Activity", String.valueOf(GlobalVars.momentSection));
        setContentView(R.layout.activity_webview);

        mTitle = MainSections[GlobalVars.momentSection];

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                CategoriasPrincipales,R.drawable.ic_drawer);

        json = new JSONTask_Get_Web_Node(this, AvisoLegalWebViewActivity.this, "2403");
        json.execute();
        //WebView myWebView = (WebView) this.findViewById(R.id.webView);
        //myWebView.loadUrl("http://www.kukimba.com/kukimba/de/aviso-legal.asp?nombre=2403&cod=2403&sesion=1");
    }

    @Override
    public void xmlWebCharged(WebNode result)
    {
        dataCharged = true;
        setRefreshing(false);

        WebView myWebView = (WebView) this.findViewById(R.id.webView);
        myWebView.loadData("<style>\n" +
                "body { margin:20px }\n" +
                "</style>\n"+result.Description, "text/html", "UTF-8");
        //String htmlData = "<link rel=\"stylesheet\" type=\"text/css\" href=\"skin49.css\" />" + result.Description;
        //myWebView.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html", "UTF-8", null);
    }
/*
    @Override
    public void onStart() {
        super.onStart();
        // The rest of your onStart() code.
        easyTracker = EasyTracker.getInstance(this);

        easyTracker.set(Fields.SCREEN_NAME, "VistaAvisoLegal");

        easyTracker.send(MapBuilder
                        .createAppView()
                        .build()
        );
    }*/
}