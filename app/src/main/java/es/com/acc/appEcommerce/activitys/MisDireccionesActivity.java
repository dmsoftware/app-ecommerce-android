package es.com.acc.appEcommerce.activitys;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;

import java.util.List;
import java.util.Locale;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.AddUserResponse;
import es.com.acc.appEcommerce.entities.Address;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Add_Direcciones;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Erase_Direcciones;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Get_Direcciones;
import es.com.acc.appEcommerce.views.DireccionDialog;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

/**
 * Created by Imanol on 20/05/2015.
 */
public class MisDireccionesActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, DireccionDialog.DialogListener
{
    private JSONTask_Get_Direcciones json;
    private JSONTask_Add_Direcciones jsonAdd;
    private JSONTask_Erase_Direcciones jsonErase;

    public LinearLayout container;

    List<Address> direcciones;


    TextView atencion;
    TextView opciones;

    Button anadir;

    int parentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Bundle b = getIntent().getExtras();
        //ProductID = b.getInt("keyID");


            setContentView(R.layout.activity_mis_direcciones);

            mNavigationDrawerFragment = (NavigationDrawerFragment)
                    getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

            // Set up the drawer.
            mNavigationDrawerFragment.setUp(
                    R.id.navigation_drawer,
                    (DrawerLayout) findViewById(R.id.drawer_layout),
                    CategoriasPrincipales, R.drawable.ic_drawer);

            anadir = (Button) findViewById(R.id.botAdd);
            atencion = (TextView) findViewById(R.id.txtAtencion);
            /*direccion = (TextView) findViewById(R.id.txtDireccion);
            codigoPostal = (TextView) findViewById(R.id.txtCP);
            ciudad = (TextView) findViewById(R.id.txtCiudad);*/
            opciones = (TextView) findViewById(R.id.txtOpciones);

            if (GlobalVars.idioma == 1) {
                anadir.setText(getResources().getString(R.string.compra_direccion_anadir_eu));
                atencion.setText(getResources().getString(R.string.direcciones_direccion_eu));
                /*direccion.setText(getResources().getString(R.string.direcciones_direccion_eu));
                codigoPostal.setText(getResources().getString(R.string.direcciones_cp_eu));
                ciudad.setText(getResources().getString(R.string.direcciones_ciudad_eu));*/
                opciones.setText(getResources().getString(R.string.direcciones_helbideak_eu));

                mTitle = getResources().getString(R.string.cuenta_misdirecciones_eu);
            } else {
                anadir.setText(getResources().getString(R.string.compra_direccion_anadir));
                atencion.setText(getResources().getString(R.string.direcciones_direccion));
                /*direccion.setText(getResources().getString(R.string.direcciones_direccion));
                codigoPostal.setText(getResources().getString(R.string.direcciones_cp));
                ciudad.setText(getResources().getString(R.string.direcciones_ciudad));*/
                opciones.setText(getResources().getString(R.string.direcciones_helbideak));

                mTitle = getResources().getString(R.string.cuenta_misdirecciones);
            }

            json = new JSONTask_Get_Direcciones(this, MisDireccionesActivity.this, String.valueOf(GlobalVars.UserData.ID), GlobalVars.password);
            json.execute();

            restoreActionBar();

    }

    @Override
    public void onStart() {
        super.onStart();

        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);

        String lag = "es";
        if(GlobalVars.idioma == 1)lag = "eu";
        String lagUser = "";
        if(GlobalVars.UserData!=null){
            lagUser = String.valueOf(GlobalVars.UserData.ID);
        }

        // Set screen name.
        tracker.setScreenName("VistaMisDirecciones");

        // Send a screen view.
        tracker.send(new HitBuilders.ScreenViewBuilder()
                .setCustomDimension(1, lag)
                .setCustomDimension(2, lagUser)
                .setCustomDimension(3, "(not set)")
                .setCustomDimension(4, "(not set)")
                .setCustomDimension(5, "(not set)")
                .setCustomDimension(6, "(not set)")
                .setCustomDimension(7, "(not set)")
                .setCustomDimension(8, Locale.getDefault().getLanguage())
                .build());
    }

    @Override
    public void xmlDirectionsCharged(List<Address> adreses)
    {
        dataCharged = true;
        setRefreshing(false);

        direcciones = adreses;

        if(direcciones!=null) {
            if (container != null) {
                container.removeAllViews();
            } else {
                container = (LinearLayout) findViewById(R.id.cellContDir);
            }

            for (int number = 0; number < adreses.size(); number++) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout menuLayout = (LinearLayout) inflater.inflate(R.layout.cell_direccion, null, false);

                TextView predeterminado = (TextView) menuLayout.findViewById(R.id.txtPredeterminado);
                if(adreses.get(number).IsDefault)
                {
                    if (GlobalVars.idioma == 1)
                        predeterminado.setText(getResources().getString(R.string.direcciones_predeterminado_eu));
                    else
                        predeterminado.setText(getResources().getString(R.string.direcciones_predeterminado));
                }
                else
                {
                    if (GlobalVars.idioma == 1)
                        predeterminado.setText(getResources().getString(R.string.direcciones_secundaria_eu));
                    else
                        predeterminado.setText(getResources().getString(R.string.direcciones_secundaria));
                }

                TextView atencion = (TextView) menuLayout.findViewById(R.id.txtAtencion);
                atencion.setText(adreses.get(number).Contact);
                //descripcion.setText(String.valueOf(number));

                TextView direcion = (TextView) menuLayout.findViewById(R.id.txtDireccion);
                direcion.setText(adreses.get(number).AddressName);

                TextView cp = (TextView) menuLayout.findViewById(R.id.txtCP);
                cp.setText(adreses.get(number).Zip);

                TextView ciudad = (TextView) menuLayout.findViewById(R.id.txtCiudad);
                ciudad.setText(adreses.get(number).City);

                Button butModi = (Button)menuLayout.findViewById(R.id.botModificar);
                butModi.setId(number);

                Button butErase = (Button)menuLayout.findViewById(R.id.botOpciones);
                butErase.setId(number);

                TextView ate = (TextView) menuLayout.findViewById(R.id.txtAte);
                TextView dire = (TextView) menuLayout.findViewById(R.id.txtDir);
                TextView copos = (TextView) menuLayout.findViewById(R.id.txtC);
                TextView ciu = (TextView) menuLayout.findViewById(R.id.Ciu);

                if (GlobalVars.idioma == 1) {
                    ate.setText(getResources().getString(R.string.direcciones_atencion_eu));
                    dire.setText(getResources().getString(R.string.direcciones_direccion_eu));
                    copos.setText(getResources().getString(R.string.direcciones_cp_eu));
                    ciu.setText(getResources().getString(R.string.direcciones_ciudad_eu));
                } else {
                    ate.setText(getResources().getString(R.string.direcciones_atencion));
                    dire.setText(getResources().getString(R.string.direcciones_direccion));
                    copos.setText(getResources().getString(R.string.direcciones_cp));
                    ciu.setText(getResources().getString(R.string.direcciones_ciudad));
                }

                /*RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams((GlobalVars.screenWidth / 2 - 2), (int) (GlobalVars.screenWidth / 1.8 - 1));
                int x = 2 + number % 2 * (GlobalVars.screenWidth / 2 - 1);
                int y = 1 + number / 2 * (int) (GlobalVars.screenWidth / 1.8);
                params2.setMargins(x, y, 1, 0);*/
                container.addView(menuLayout);
            }
        }
    }

    public void onDialogPositiveClick(DialogFragment dialog, Address dir) {
        // User touched the dialog's positive button
        //Toast.makeText(getApplicationContext(), String.valueOf(value), Toast.LENGTH_SHORT).show();
        jsonAdd = new JSONTask_Add_Direcciones(this, MisDireccionesActivity.this, String.valueOf(dir.Id), String.valueOf(GlobalVars.UserData.ID), dir.Contact, dir.AddressName, dir.City, dir.Zip, String.valueOf(dir.Province), String.valueOf(dir.Country), dir.Observation, String.valueOf(dir.IsDefault));
        jsonAdd.execute();
    }

    @Override
    public void xmlDirAdded(AddUserResponse response) {
        Intent intent = new Intent(MisDireccionesActivity.this, MisDireccionesActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), String.valueOf(response.Error), Toast.LENGTH_SHORT).show();
    }

    public void AnadirClicked(View v)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();

        DireccionDialog direccionFragment = new DireccionDialog();
        if (GlobalVars.idioma == 1)
            direccionFragment.setValues(getResources().getString(R.string.direcciones_dialog_anade_eu), null);
        else
            direccionFragment.setValues(getResources().getString(R.string.direcciones_dialog_anade), null);

        direccionFragment.show(fragmentManager, "Dialog");
        AlertDialog dialog = (AlertDialog) direccionFragment.getDialog();
    }

    public void modificaClicked(View v)
    {
        parentId = v.getId();

        FragmentManager fragmentManager = getSupportFragmentManager();

        DireccionDialog direccionFragment = new DireccionDialog();
        if (GlobalVars.idioma == 1)
            direccionFragment.setValues(getResources().getString(R.string.direcciones_dialog_modifica_eu), direcciones.get(parentId));
        else
            direccionFragment.setValues(getResources().getString(R.string.direcciones_dialog_modifica), direcciones.get(parentId));

        direccionFragment.show(fragmentManager, "Dialog");
        AlertDialog dialog = (AlertDialog) direccionFragment.getDialog();
    }

    public void eraseClicked(View v)
    {
        if(direcciones.size()>1) {
            parentId = v.getId();
            String lagOK;
            String lagKO;
            AlertDialog alertDialog = new AlertDialog.Builder(MisDireccionesActivity.this).create();
            if (GlobalVars.idioma == 1) {
                alertDialog.setTitle(getResources().getString(R.string.direcciones_dialog_borrar_eu));
                alertDialog.setMessage(getResources().getString(R.string.direcciones_dialog__desea_borrar_eu));
                lagOK = getResources().getString(R.string.dialog_ok_eu);
                lagKO = getResources().getString(R.string.dialog_ko_eu);
            }
            else {
                alertDialog.setTitle(getResources().getString(R.string.direcciones_dialog_borrar));
                alertDialog.setMessage(getResources().getString(R.string.direcciones_dialog__desea_borrar));
                lagOK = getResources().getString(R.string.dialog_ok);
                lagKO = getResources().getString(R.string.dialog_ko);
            }
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, lagOK,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            eraseDir();
                            dialog.dismiss();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, lagKO,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
        else
        {
            if (GlobalVars.idioma == 1) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.direcciones_alerta_borrrado_eu), Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.direcciones_alerta_borrado), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void eraseDir()
    {
        jsonErase = new JSONTask_Erase_Direcciones(this, MisDireccionesActivity.this, String.valueOf(direcciones.get(parentId).Id), String.valueOf(GlobalVars.UserData.ID));
        jsonErase.execute();
    }
}
