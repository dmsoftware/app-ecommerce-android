package es.com.acc.appEcommerce.utils.JSON;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;

import es.com.acc.appEcommerce.activitys.MisPedidosActivity;
import es.com.acc.appEcommerce.entities.Pedidos;


public class JSONTask_Get_Orders extends AsyncTask<Void, Void, String>
{
    private Context context;
    private MisPedidosActivity PrSh;
    private static final String TAG = "PostFetcher";
    private Boolean UserLoaded = false;
    public static final String SERVER_URL = "-- url del servicio --";
    private List<Pedidos> userAdresses;

    private String pathURL;

    public JSONTask_Get_Orders(MisPedidosActivity A, Context mContext, String _userID, String _password, String _orderID)
    {

        pathURL = SERVER_URL;
        pathURL += "&id=";
        pathURL +=  _userID;
        pathURL += "&password=";
        pathURL += _password;
        pathURL += "&orderID=";
        pathURL += _orderID;

        this.PrSh = A;
        this.context = mContext;
        UserLoaded = false;
    }

    private void handlePostsList(List<Pedidos> posts)
    {
        this.userAdresses = posts;
    }

    //private void failedLoadingPosts()
    //{
    //    Log.d("Error", "Post Failed");
    //}

    @Override
    protected void onPostExecute(String result)
    {
        if(UserLoaded==false)
        {
            UserLoaded = true;
            PrSh.xmlCharged(userAdresses);
        }
    }

     @Override
     protected String doInBackground(Void... params)
     {
         try {
             //Create an HTTP client
             HttpClient client = new DefaultHttpClient();

             System.out.println(" ::::::::::::::::::::::::::::: Get orders: >> "+pathURL);

             HttpPost post = new HttpPost(pathURL);

             //Perform the request and check the status code
             HttpResponse response = client.execute(post);
             StatusLine statusLine = response.getStatusLine();
             if(statusLine.getStatusCode() == 200) {
                 HttpEntity entity = response.getEntity();
                 InputStream content = entity.getContent();

                 try {
                     //Read the server response and attempt to parse it as JSON
                     Reader reader = new InputStreamReader(content);

                     GsonBuilder gsonBuilder = new GsonBuilder();
                     gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                     Gson gson = gsonBuilder.create();

                     List<Pedidos> Adreses = Arrays.asList(gson.fromJson(reader, Pedidos[].class));
                     content.close();

                     handlePostsList(Adreses);
                 } catch (Exception ex) {
                     //Log.e(TAG, "Failed to parse JSON due to: " + ex);
                     //failedLoadingPosts();
                 }
             } else {
                 //Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                 //failedLoadingPosts();
             }
         } catch(Exception ex) {
             //Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
             //failedLoadingPosts();
         }
         return null;
     }
}
