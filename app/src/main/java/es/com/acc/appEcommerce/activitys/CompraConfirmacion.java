package es.com.acc.appEcommerce.activitys;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.CestaItem;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

/**
 * Created by Imanol on 02/07/2015.
 */
public class CompraConfirmacion extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{
    private TextView identificacion;
    private TextView pago;
    private TextView confirma;

    private TextView txtConfirma;

    private TextView txtPedido;
    private TextView lblPedido;

    private TextView txtFecha;
    private TextView lblFecha;

    private TextView txtCliente;
    private TextView lblCliente;

    private TextView txtMovil;
    private TextView lblMovil;

    private TextView txtPago;
    private TextView lblPago;

    private TextView txtModo;
    private TextView lblModo;

    private TextView txtEntrega;
    private TextView lblEntrega;

    private TextView txtplazo;
    private TextView txtinfo1;
    private TextView txtinfo2;

    private TextView gastoEnvio;
    private TextView gastoTotal;

    public LinearLayout container;
    public String SERVER_URL = "http://images.kukimba.com/item/233x155/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_compra_confirmacion);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                CategoriasPrincipales, R.drawable.ic_action_previous_item);

        identificacion= (TextView) findViewById(R.id.leyendaIdentificacion);
        pago= (TextView) findViewById(R.id.leyendaPagoEnvio);
        confirma = (TextView) findViewById(R.id.leyendaConfirmacion);

        txtConfirma = (TextView) findViewById(R.id.txtconfirmacion);
        txtPedido = (TextView) findViewById(R.id.txtPedido);
        lblPedido = (TextView) findViewById(R.id.lblPedido);
        txtFecha = (TextView) findViewById(R.id.txtFecha);
        lblFecha = (TextView) findViewById(R.id.lblFecha);
        txtCliente = (TextView) findViewById(R.id.txtCliente);
        lblCliente = (TextView) findViewById(R.id.lblCliente);
        txtMovil = (TextView) findViewById(R.id.txtMovil);
        lblMovil = (TextView) findViewById(R.id.lblMovil);
        txtPago = (TextView) findViewById(R.id.txtFormaPago);
        lblPago = (TextView) findViewById(R.id.lblFormaPago);
        txtModo = (TextView) findViewById(R.id.txtModoEnvio);
        lblModo = (TextView) findViewById(R.id.lblModoEnvio);
        txtEntrega = (TextView) findViewById(R.id.txtDirEntrega);
        lblEntrega = (TextView) findViewById(R.id.lblDirEntrega);

        gastoEnvio = (TextView) findViewById(R.id.GastoEnvio);
        gastoTotal = (TextView) findViewById(R.id.GastoTotal);

        txtplazo = (TextView) findViewById(R.id.txtPlazo);
        txtinfo1 = (TextView) findViewById(R.id.txtInfo1);
        txtinfo2 = (TextView) findViewById(R.id.txtInfo2);

        if (GlobalVars.idioma == 1) {
            mTitle = getResources().getString(R.string.compra_title_eu);
            identificacion.setText(getResources().getString(R.string.compra_identificacion_eu));
            pago.setText(getResources().getString(R.string.compra_pago_eu));
            confirma.setText(getResources().getString(R.string.compra_confirmacion_eu));

            txtConfirma.setText(getResources().getString(R.string.compra_confirmacion_ok_eu));
            txtPedido.setText(getResources().getString(R.string.compra_numero_pedido_eu));
            txtFecha.setText(getResources().getString(R.string.compra_fecha_eu));
            txtCliente.setText(getResources().getString(R.string.compra_cliente_eu));
            txtMovil.setText(getResources().getString(R.string.compra_movil_eu));
            txtPago.setText(getResources().getString(R.string.compra_forma_pago_eu));
            txtModo.setText(getResources().getString(R.string.compra_tipo_envio_eu));
            txtEntrega.setText(getResources().getString(R.string.compra_direccion_eu));

            gastoEnvio.setText(getResources().getString(R.string.cesta_gastos_eu));
            gastoTotal.setText(getResources().getString(R.string.cesta_total_iva_eu));

            txtplazo.setText(getResources().getString(R.string.compra_plazo_eu));
            txtinfo1.setText(getResources().getString(R.string.compra_info_1_eu));
            txtinfo2.setText(getResources().getString(R.string.compra_info_2_eu));
        }
        else
        {
            mTitle = getResources().getString(R.string.compra_title);
            identificacion.setText(getResources().getString(R.string.compra_identificacion));
            pago.setText(getResources().getString(R.string.compra_pago));
            confirma.setText(getResources().getString(R.string.compra_confirmacion));

            txtConfirma.setText(getResources().getString(R.string.compra_confirmacion_ok));
            txtPedido.setText(getResources().getString(R.string.compra_numero_pedido));
            txtFecha.setText(getResources().getString(R.string.compra_fecha));
            txtCliente.setText(getResources().getString(R.string.compra_cliente));
            txtMovil.setText(getResources().getString(R.string.compra_movil));
            txtPago.setText(getResources().getString(R.string.compra_forma_pago));
            txtModo.setText(getResources().getString(R.string.compra_tipo_envio));
            txtEntrega.setText(getResources().getString(R.string.compra_direccion));

            gastoEnvio.setText(getResources().getString(R.string.cesta_gastos));
            gastoTotal.setText(getResources().getString(R.string.cesta_total_iva));

            txtplazo.setText(getResources().getString(R.string.compra_plazo));
            txtinfo1.setText(getResources().getString(R.string.compra_info_1));
            txtinfo2.setText(getResources().getString(R.string.compra_info_2));
        }

        feelView();
    }

    @Override
    public void onStart() {
        super.onStart();

        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);
    }

    public void feelView()
    {
        if(container!=null)
        {
            container.removeAllViews();
        }
        else {
            container = (LinearLayout) findViewById(R.id.cellContainer);
        }

        if(GlobalVars.CestaData!=null)
        {
            int number = 0;
            float sumaTotal = 0;
            String lag;
            for (final CestaItem item : GlobalVars.CestaData.Items) {
                //JSONTask_Cesta json = new JSONTask_Cesta(this,CestaActivity.this, item.getId());
                //json.execute();
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout menuLayout = (LinearLayout) inflater.inflate(R.layout.cell_compra, null, false);

                TextView tallaT =  (TextView) menuLayout.findViewById(R.id.txtTalla);
                TextView unidadesT =  (TextView) menuLayout.findViewById(R.id.txtCantidad);
                TextView preciosT =  (TextView) menuLayout.findViewById(R.id.txtPrecio);
                TextView txtTotT =  (TextView) menuLayout.findViewById(R.id.txtTotal);

                if(GlobalVars.idioma == 1)
                {
                    tallaT.setText(getResources().getString(R.string.cesta_talla_eu));
                    unidadesT.setText(getResources().getString(R.string.cesta_unidades_eu));
                    preciosT.setText(getResources().getString(R.string.cesta_unitario_eu));
                    txtTotT.setText(getResources().getString(R.string.cesta_total_eu));
                }
                else
                {
                    tallaT.setText(getResources().getString(R.string.cesta_talla));
                    unidadesT.setText(getResources().getString(R.string.cesta_unidades));
                    preciosT.setText(getResources().getString(R.string.cesta_unitario));
                    txtTotT.setText(getResources().getString(R.string.cesta_total));
                }

                TextView brandText = (TextView) menuLayout.findViewById(R.id.productBrand);
                brandText.setText(String.valueOf(item.ItemBrand));

                TextView titleText = (TextView) menuLayout.findViewById(R.id.productName);
                titleText.setText(String.valueOf(item.ItemName));

                ImageView img = (ImageView) menuLayout.findViewById(R.id.imageCestaItem);
                LoadImage imgJson = new LoadImage(img,CompraConfirmacion.this);
                imgJson.execute(SERVER_URL+item.Image);

                TextView tallaText = (TextView) menuLayout.findViewById(R.id.productTalla);
                tallaText.setText(item.SizeDisplay);

                TextView precioText = (TextView) menuLayout.findViewById(R.id.productPrecio);
                precioText.setText(String.format("%.2f", item.Price)+ " €");

                TextView preciOriginalText = (TextView) menuLayout.findViewById(R.id.productPrecioOriginal);
                preciOriginalText.setPaintFlags(preciOriginalText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                preciOriginalText.setText(String.format("%.2f", item.OriginalPrice)+" €");

                TextView productTotal = (TextView) menuLayout.findViewById(R.id.productTotal);
                productTotal.setText(String.format("%.2f", item.Price*item.Units)+" €");

                sumaTotal += item.Price*item.Units;

                TextView cantidad = (TextView) menuLayout.findViewById(R.id.valorCantidad);

                cantidad.setText(String.valueOf(item.Units));

                container.addView(menuLayout);

                number++;
            }
            TextView textSumaTotal = (TextView) findViewById(R.id.PriceTotal);
            textSumaTotal.setText(sumaTotal+ " €");
        }
        else
        {
            TextView textSumaTotal = (TextView) findViewById(R.id.PriceTotal);
            textSumaTotal.setText("0,00 €");
        }

        lblPedido.setText("123456");
        lblFecha.setText("12/09/2015");
        lblCliente.setText(GlobalVars.UserData.Name);
        lblMovil.setText(GlobalVars.UserData.Telephone);
        if(GlobalVars.payMethod==0) {
            if (GlobalVars.idioma == 1)
                lblPago.setText(getResources().getString(R.string.compra_forma_pago_tarjeta_eu));
            else
                lblPago.setText(getResources().getString(R.string.compra_forma_pago_tarjeta));
        }
        else {
            if (GlobalVars.idioma == 1)
                lblPago.setText(getResources().getString(R.string.compra_forma_pago_reembolso_eu));
            else
                lblPago.setText(getResources().getString(R.string.compra_forma_pago_reembolso));
        }
        lblEntrega.setText(GlobalVars.DireccionEntrega);

        dataCharged = true;
        setRefreshing(false);

        GlobalVars.CestaData = null;
        GlobalVars.cesta_count = 0;
        GlobalVars.CestaID = -1;
        updateHotCount(GlobalVars.cesta_count);
    }
}
