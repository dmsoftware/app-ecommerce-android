package es.com.acc.appEcommerce.activitys;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;

import java.util.List;
import java.util.Locale;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.Pedidos;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Get_Orders;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

/**
 * Created by Imanol on 20/05/2015.
 */
public class MisPedidosActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{
    public String SERVER_URL = "http://images.kukimba.com/item/233x155/";
    private JSONTask_Get_Orders json;
    public LinearLayout container;
    public List<Pedidos> pedidos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Bundle b = getIntent().getExtras();
        //ProductID = b.getInt("keyID");

            setContentView(R.layout.activity_mis_pedidos);

            mNavigationDrawerFragment = (NavigationDrawerFragment)
                    getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

            // Set up the drawer.
            mNavigationDrawerFragment.setUp(
                    R.id.navigation_drawer,
                    (DrawerLayout) findViewById(R.id.drawer_layout),
                    CategoriasPrincipales, R.drawable.ic_drawer);

            container = (LinearLayout) findViewById(R.id.cellContainer);

            if (GlobalVars.idioma == 1) {
                mTitle = getResources().getString(R.string.pedido_title_eu);
            } else {
                mTitle = getResources().getString(R.string.pedido_title);
            }

            json = new JSONTask_Get_Orders(this, MisPedidosActivity.this, String.valueOf(GlobalVars.UserData.ID), GlobalVars.password, "0");
            json.execute();

            restoreActionBar();

    }

    @Override
    public void onStart() {
        super.onStart();

        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);

        String lag = "es";
        if(GlobalVars.idioma == 1)lag = "eu";
        String lagUser = "";
        if(GlobalVars.UserData!=null){
            lagUser = String.valueOf(GlobalVars.UserData.ID);
        }

        // Set screen name.
        tracker.setScreenName("VistaMisPedidos");

        // Send a screen view.
        tracker.send(new HitBuilders.ScreenViewBuilder()
                .setCustomDimension(1, lag)
                .setCustomDimension(2, lagUser)
                .setCustomDimension(3, "(not set)")
                .setCustomDimension(4, "(not set)")
                .setCustomDimension(5, "(not set)")
                .setCustomDimension(6, "(not set)")
                .setCustomDimension(7, "(not set)")
                .setCustomDimension(8, Locale.getDefault().getLanguage())
                .build());
    }

    public void xmlCharged(List<Pedidos> _pedidos)
    {
        dataCharged = true;
        setRefreshing(false);

        pedidos = _pedidos;

        if(pedidos!=null) {
            if (container != null) {
                container.removeAllViews();
            } else {
                container = (LinearLayout) findViewById(R.id.cellContainer);
            }

            for (int number = 0; number < pedidos.size(); number++) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout menuLayout = (LinearLayout) inflater.inflate(R.layout.cell_pedidos, null, false);

                TextView pedido = (TextView) menuLayout.findViewById(R.id.txtPedido);
                TextView fecha = (TextView) menuLayout.findViewById(R.id.txtFecha);
                TextView modo = (TextView) menuLayout.findViewById(R.id.txtModoPago);

                TextView gastoTotal = (TextView) menuLayout.findViewById(R.id.GastoTotal);

                TextView precioTotal = (TextView) menuLayout.findViewById(R.id.PriceTotal);

                if(GlobalVars.idioma == 1)
                {
                    pedido.setText(getResources().getString(R.string.pedido_numero_eu)+String.valueOf(pedidos.get(number).IdERP));
                    fecha.setText(getResources().getString(R.string.pedido_fecha_eu)+pedidos.get(number).Date);
                    modo.setText(getResources().getString(R.string.pedido_modo_eu)+pedidos.get(number).PaymentMethod);

                    //gastos.setText(getResources().getString(R.string.cesta_gastos_eu));
                    gastoTotal.setText(getResources().getString(R.string.cesta_total_iva_eu));
                }
                else
                {
                    pedido.setText(getResources().getString(R.string.pedido_numero)+String.valueOf(pedidos.get(number).IdERP));
                    fecha.setText(getResources().getString(R.string.pedido_fecha)+pedidos.get(number).Date);
                    modo.setText(getResources().getString(R.string.pedido_modo)+pedidos.get(number).PaymentMethod);

                    //gastos.setText(getResources().getString(R.string.cesta_gastos));
                    gastoTotal.setText(getResources().getString(R.string.cesta_total_iva));
                }

                LinearLayout pedidoItems = (LinearLayout) menuLayout.findViewById(R.id.pedidoItemContainer);
                float sumaTotal = 0;
                int number2 = 0;
                for (number2 = 0; number2 < pedidos.get(number).Items.length; number2++) {
                    if(pedidos.get(number).Items[number2].ItemId < 100)
                    {
                        LinearLayout itemLayout = (LinearLayout) inflater.inflate(R.layout.cell_pedido_gasto, null, false);

                        TextView gastoEnv = (TextView) itemLayout.findViewById(R.id.GastoEnvio);
                        TextView precioEnv = (TextView) itemLayout.findViewById(R.id.PriceEnvio);

                        gastoEnv.setText(pedidos.get(number).Items[number2].ItemName);
                        precioEnv.setText(String.valueOf(pedidos.get(number).Items[number2].OriginalPrice));

                        pedidoItems.addView(itemLayout);
                    }
                    else
                    {
                        LinearLayout itemLayout = (LinearLayout) inflater.inflate(R.layout.cell_item_pedido, null, false);

                        TextView brandText = (TextView) itemLayout.findViewById(R.id.productBrand);
                        brandText.setText(String.valueOf(pedidos.get(number).Items[number2].ItemName));

                        TextView titleText = (TextView) itemLayout.findViewById(R.id.productName);
                        if(pedidos.get(number).Items[number2].returnUnits > 0){
                            if(pedidos.get(number).Items[number2].Units > pedidos.get(number).Items[number2].returnUnits){
                                titleText.setText(String.valueOf(pedidos.get(number).Items[number2].returnUnits)+" unidades devueltas el "+pedidos.get(number).Items[number2].returnDate);
                            }
                            else{
                                titleText.setText("Articulo devuelto el "+pedidos.get(number).Items[number2].returnDate);
                            }
                            titleText.setTextColor(getResources().getColor(R.color.error_text));
                        }
                        else {
                            titleText.setText(String.valueOf(pedidos.get(number).Items[number2].Id));
                        }

                        TextView producto = (TextView) itemLayout.findViewById(R.id.txtName);
                        TextView talla = (TextView) itemLayout.findViewById(R.id.txtTalla);
                        TextView unidades = (TextView) itemLayout.findViewById(R.id.txtCantidad);
                        TextView precio = (TextView) itemLayout.findViewById(R.id.txtPrecio);
                        TextView total = (TextView) itemLayout.findViewById(R.id.txtTotal);

                        if(GlobalVars.idioma == 1)
                        {
                            producto.setText(getResources().getString(R.string.cesta_producto_eu));
                            talla.setText(getResources().getString(R.string.cesta_talla_eu));
                            unidades.setText(getResources().getString(R.string.cesta_unidades_eu));
                            precio.setText(getResources().getString(R.string.cesta_unitario_eu));
                            total.setText(getResources().getString(R.string.cesta_total_eu));
                        }
                        else
                        {
                            producto.setText(getResources().getString(R.string.cesta_producto));
                            talla.setText(getResources().getString(R.string.cesta_talla));
                            unidades.setText(getResources().getString(R.string.cesta_unidades));
                            precio.setText(getResources().getString(R.string.cesta_unitario));
                            total.setText(getResources().getString(R.string.cesta_total));
                        }

                        ImageView img = (ImageView) itemLayout.findViewById(R.id.imageCestaItem);
                        LoadImage imgJson = new LoadImage(img, MisPedidosActivity.this);
                        imgJson.execute(SERVER_URL + pedidos.get(number).Items[number2].Image);

                        TextView tallaText = (TextView) itemLayout.findViewById(R.id.productTalla);
                        tallaText.setText(pedidos.get(number).Items[number2].SizeDisplay);

                        TextView cantidadText = (TextView) itemLayout.findViewById(R.id.productCantidad);
                        cantidadText.setText(String.valueOf(pedidos.get(number).Items[number2].Units));

                        TextView precioText = (TextView) itemLayout.findViewById(R.id.productPrecio);
                        precioText.setText(String.valueOf(pedidos.get(number).Items[number2].Price) + " €");

                        TextView preciOriginalText = (TextView) itemLayout.findViewById(R.id.productPrecioOriginal);
                        preciOriginalText.setPaintFlags(preciOriginalText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        preciOriginalText.setText(String.valueOf(pedidos.get(number).Items[number2].OriginalPrice) + " €");

                        TextView productTotal = (TextView) itemLayout.findViewById(R.id.productTotal);
                        productTotal.setText(String.valueOf(pedidos.get(number).Items[number2].Price * pedidos.get(number).Items[number2].Units) + " €");

                        sumaTotal += pedidos.get(number).Items[number2].Price * pedidos.get(number).Items[number2].Units;

                        /*LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(GlobalVars.screenWidth, GlobalVars.screenWidth / 5 - 2);
                        params2.setMargins(0, 1, 0, 1);
                        pedidoItems.addView(itemLayout, params2);*/
                        pedidoItems.addView(itemLayout);
                    }
                }

                precioTotal.setText(String.valueOf(sumaTotal));

                /*LinearLayout resulView = (LinearLayout) menuLayout.findViewById(R.id.resultBar);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(0, GlobalVars.screenWidth/4*number2 , 0, 5);
                resulView.setLayoutParams(layoutParams);*/

                container.addView(menuLayout);
            }
        }
    }
}
