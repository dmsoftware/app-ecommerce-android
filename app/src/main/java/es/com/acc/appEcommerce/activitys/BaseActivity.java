package es.com.acc.appEcommerce.activitys;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.activitys.WebView.AvisoLegalWebViewActivity;
import es.com.acc.appEcommerce.activitys.WebView.CalidadWebActivity;
import es.com.acc.appEcommerce.activitys.WebView.CuidadoWebActivity;
import es.com.acc.appEcommerce.activitys.WebView.GarantiaSatisfaccionActivity;
import es.com.acc.appEcommerce.activitys.WebView.GastosEnvioActivity;
import es.com.acc.appEcommerce.activitys.WebView.PreguntasWebActivity;
import es.com.acc.appEcommerce.activitys.WebView.PrivacidadWebActivity;
import es.com.acc.appEcommerce.activitys.WebView.QuienesSomosWebActivity;
import es.com.acc.appEcommerce.activitys.WebView.VentajasWebActivity;
import es.com.acc.appEcommerce.entities.AddUserResponse;
import es.com.acc.appEcommerce.entities.Address;
import es.com.acc.appEcommerce.entities.CategoriaRow;
import es.com.acc.appEcommerce.entities.CestaCompra;
import es.com.acc.appEcommerce.entities.FiltrosRow;
import es.com.acc.appEcommerce.entities.ItemCategories;
import es.com.acc.appEcommerce.entities.UserLogin;
import es.com.acc.appEcommerce.entities.WebNode;
import es.com.acc.appEcommerce.utils.DataBaseHandler;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

/**
 * Created by Imanol on 27/04/2015.
 */
public class BaseActivity extends AppCompatActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    public NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    public CharSequence mTitle;
    public String[] MainSections;
    public List<String> CategoriasPrincipales = new ArrayList<String>();

    private AlertDialog ad = null;

    //public NetworkChangeReceiver yourBR;

    public MenuItem ProgressBar = null;

    private int mNotificationsCount = 0;

    public boolean dataCharged = false;

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    public TextView cesta_counter_t = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        analytics = GoogleAnalytics.getInstance(BaseActivity.this);
        tracker = analytics.newTracker("UA-44405535-4"); // Send hits to tracker id UA-XXXX-Y
        tracker.enableAdvertisingIdCollection(true);

        for (CategoriaRow CaTemp : GlobalVars.Categorias) {
            CategoriasPrincipales.add(CaTemp.principal.Name);
        }

        int lag = GlobalVars.Categorias.size() + 20;
        //int lag = 24;
        MainSections = new String[lag];

        if(GlobalVars.idioma == 1) {
            MainSections[0] = getResources().getString(R.string.menu_section1_eu);
            MainSections[1] = getResources().getString(R.string.menu_section0_eu);
            MainSections[2] = "Gizona";
            MainSections[3] = "Emakumea";
            MainSections[4] = "Mutikoa";
            MainSections[5] = "Neskatoa";
            MainSections[6] = "Markak";
            MainSections[7] = "Outlet";
            //MainSections[7] = getResources().getString(R.string.menu_section2_eu);
            MainSections[8]  = getResources().getString(R.string.menu_section3_eu);
            MainSections[9]  = getResources().getString(R.string.cuenta_misdatos_eu);
            MainSections[10]  = getResources().getString(R.string.cuenta_misdirecciones_eu);
            MainSections[11] = getResources().getString(R.string.cuenta_mispedidos_eu);
            MainSections[12] = getResources().getString(R.string.menu_favoritos_eu);
            MainSections[13] = getResources().getString(R.string.menu_section4_eu);
            MainSections[14] = getResources().getString(R.string.menu_section5_eu);
            MainSections[15] = getResources().getString(R.string.menu_section6_eu);
            MainSections[16] = getResources().getString(R.string.menu_section7_eu);
            MainSections[17] = getResources().getString(R.string.menu_section8_eu);
            MainSections[18] = getResources().getString(R.string.menu_section9_eu);
            MainSections[19] = getResources().getString(R.string.menu_section10_eu);
            MainSections[20] = getResources().getString(R.string.menu_section11_eu);
            MainSections[21] = getResources().getString(R.string.menu_section12_eu);
            MainSections[22] = getResources().getString(R.string.menu_section13_eu);
            MainSections[23] = getResources().getString(R.string.menu_section14_eu);
            MainSections[24] = getResources().getString(R.string.menu_section15_eu);
        } else {
            MainSections[0] = getResources().getString(R.string.menu_section1);
            MainSections[1] = getResources().getString(R.string.menu_section0);

            for (int i = 0; i < GlobalVars.Categorias.size(); i++) {
                MainSections[i+2] = GlobalVars.Categorias.get(i).principal.Name;
            }
            int kont = GlobalVars.Categorias.size()+2;

            //MainSections[kont] = getResources().getString(R.string.menu_section2);
            MainSections[kont]  = getResources().getString(R.string.menu_section3);
            MainSections[kont+1]  = getResources().getString(R.string.cuenta_misdatos);
            MainSections[kont+2]  = getResources().getString(R.string.cuenta_misdirecciones);
            MainSections[kont+3] = getResources().getString(R.string.cuenta_mispedidos);
            MainSections[kont+4] = getResources().getString(R.string.menu_favoritos);
            MainSections[kont+5] = getResources().getString(R.string.menu_section4);
            MainSections[kont+6] = getResources().getString(R.string.menu_section5);
            MainSections[kont+7] = getResources().getString(R.string.menu_section6);
            MainSections[kont+8] = getResources().getString(R.string.menu_section7);
            MainSections[kont+9] = getResources().getString(R.string.menu_section8);
            MainSections[kont+10] = getResources().getString(R.string.menu_section9);
            MainSections[kont+11] = getResources().getString(R.string.menu_section10);
            MainSections[kont+12] = getResources().getString(R.string.menu_section11);
            MainSections[kont+13] = getResources().getString(R.string.menu_section12);
            MainSections[kont+14] = getResources().getString(R.string.menu_section13);
            MainSections[kont+15] = getResources().getString(R.string.menu_section14);
            MainSections[kont+16] = getResources().getString(R.string.menu_section15);
        }

        /*yourBR = new NetworkChangeReceiver();
        yourBR.inicializar(this);
        IntentFilter callInterceptorIntentFilter = new IntentFilter("android.intent.action.ANY_ACTION");
        getBaseContext().registerReceiver(yourBR, callInterceptorIntentFilter);*/

        // Run a task to fetch the notifications count
        //new FetchCountTask().execute();
    }

    public void goBack()
    {
        Intent intent = NavUtils.getParentActivityIntent(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        NavUtils.navigateUpTo(this, intent);
        finish();
        freeMemory();
        //NavUtils.navigateUpFromSameTask(this);
    }

    public void freeMemory(){
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        if(GlobalVars.refresh_menu){
            GlobalVars.myDB = new DataBaseHandler(this, GlobalVars.db_name, GlobalVars.db_version);

            GlobalVars.myDB.close();
            GlobalVars.refresh_menu = false;
        }

        return true;
    }

    /*@Override
    public void onStart() {
        super.onStart();
        // The rest of your onStart() code.
        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);
    }

    @Override
    public void onStop() {
        super.onStop();
        // The rest of your onStop() code.
        //EasyTracker.getInstance(this).activityStop(this);  // Add this method.

        //getBaseContext().unregisterReceiver(yourBR);
    }*/

    public void createNetworkFailure()
    {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(BaseActivity.this);
        myAlertDialog.setCancelable(true);
        if(GlobalVars.idioma == 1) {
            myAlertDialog.setTitle(getResources().getString(R.string.net_error_dialog_eu));
            myAlertDialog.setMessage(getResources().getString(R.string.net_error_dialog_message_eu));
            myAlertDialog.setPositiveButton(getResources().getString(R.string.net_error_dialog_ko_eu), new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface arg0, int arg1) {
                    // do something when the OK button is clicked
                    BaseActivity.this.finish();
                    System.exit(0);
                }
            });
            myAlertDialog.setNegativeButton(getResources().getString(R.string.net_error_dialog_ok_eu), new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface arg0, int arg1) {
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                }
            });
        }
        else
        {
            myAlertDialog.setTitle(getResources().getString(R.string.net_error_dialog));
            myAlertDialog.setMessage(getResources().getString(R.string.net_error_dialog_message));
            myAlertDialog.setPositiveButton(getResources().getString(R.string.net_error_dialog_ko), new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface arg0, int arg1) {
                    // do something when the OK button is clicked
                    BaseActivity.this.finish();
                    System.exit(0);
                }
            });
            myAlertDialog.setNegativeButton(getResources().getString(R.string.net_error_dialog_ok), new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface arg0, int arg1) {
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                }
            });
        }
        ad = myAlertDialog.show();
    }

    public void deleteNetworkFailure()
    {
        if(ad!=null) {
            ad.dismiss();
        }
    }


    public void xmlDirectionsCharged(List<Address> userAdresses)
    {
    }

    public void xmlWebCharged(WebNode result)
    {

    }

    public void xmlFiltrsCharged(List<FiltrosRow> Filtrs)
    {
    }

    public void xmlLoginCharged(UserLogin login)
    {
    }

    public void InicioClick (String id, String psw)
    {
    }

    public void RecordarClicked (String id)
    {
    }

    public void xmlDirAdded (AddUserResponse response)
    {
    }

    public void xmlItemCatCharged(List<ItemCategories> cats)
    {}

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        /*FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();*/
        if(position>0) {
            GlobalVars.momentSection = position;
            GlobalVars.momentSub = 0;
            //mTitle = MainSections[GlobalVars.momentSection];

            Intent nextScreen;
            if(position==1)
            {
                nextScreen = new Intent(getApplicationContext(), DestacadosActivity.class);
                startActivity(nextScreen);
                //finish();
            }
            else if (position - 2 < GlobalVars.Categorias.size()) {
                nextScreen = new Intent(getApplicationContext(), SubCategoriaActivity.class);
                startActivity(nextScreen);
                //finish();
            }else if(position == GlobalVars.Categorias.size()+2){
                nextScreen = new Intent(getApplicationContext(), CuentaActivity.class);
                startActivity(nextScreen);
                //finish();
            }else if(position == GlobalVars.Categorias.size()+3){
                if(GlobalVars.UserData.ID  < 1)
                {
                    nextScreen = new Intent(getApplicationContext(), CuentaActivity.class);
                    startActivity(nextScreen);
                }
                else {
                    nextScreen = new Intent(getApplicationContext(), MisDatosActivity.class);
                    startActivity(nextScreen);
                }
                //finish();
            }else if(position == GlobalVars.Categorias.size()+4) {
                if (GlobalVars.UserData.ID < 1) {
                    nextScreen = new Intent(getApplicationContext(), CuentaActivity.class);
                    startActivity(nextScreen);
                }
                else {
                    nextScreen = new Intent(getApplicationContext(), MisDireccionesActivity.class);
                    startActivity(nextScreen);
                }
                //finish();
            }else if(position == GlobalVars.Categorias.size()+5){
                if(GlobalVars.UserData.ID  < 1)
                {
                    nextScreen = new Intent(getApplicationContext(), CuentaActivity.class);
                    startActivity(nextScreen);
                }else {
                    nextScreen = new Intent(getApplicationContext(), MisPedidosActivity.class);
                    startActivity(nextScreen);
                }
                //finish();
            }else if(position == GlobalVars.Categorias.size()+6){
                if(GlobalVars.UserData.ID  < 1)
                {
                    nextScreen = new Intent(getApplicationContext(), CuentaActivity.class);
                    startActivity(nextScreen);
                }else {
                    nextScreen = new Intent(getApplicationContext(), FavoritoActivity.class);
                    startActivity(nextScreen);
                }
                //finish();
            }else if(position == GlobalVars.Categorias.size()+7){
                nextScreen = new Intent(getApplicationContext(), ConfiguracionViewActivity.class);
                startActivity(nextScreen);
                //finish();
            /*}else if(position == GlobalVars.Categorias.size()+6){
                //cabecera KUKIMBA clickado

                GlobalVars.momentSection = GlobalVars.Categorias.size()+10;
                GlobalVars.momentSub = 0;

                nextScreen = new Intent(getApplicationContext(), QuienesSomosWebActivity.class);
                startActivity(nextScreen);
                finish();*/
            }else if(position == GlobalVars.Categorias.size()+9){
                //nextScreen = new Intent(getApplicationContext(), ContactoActivity.class);
                nextScreen = new Intent(getApplicationContext(), ContactoActivity.class);
                startActivity(nextScreen);
                //finish();
            }else if(position == GlobalVars.Categorias.size()+10){
                nextScreen = new Intent(getApplicationContext(), AvisoLegalWebViewActivity.class);
                startActivity(nextScreen);
                //finish();
            }else if(position == GlobalVars.Categorias.size()+11){
                nextScreen = new Intent(getApplicationContext(), PrivacidadWebActivity.class);
                startActivity(nextScreen);
                //finish();
            }else if(position == GlobalVars.Categorias.size()+12){
                nextScreen = new Intent(getApplicationContext(), QuienesSomosWebActivity.class);
                startActivity(nextScreen);
                //finish();
            }else if(position == GlobalVars.Categorias.size()+13){
                nextScreen = new Intent(getApplicationContext(), PreguntasWebActivity.class);
                startActivity(nextScreen);
                //finish();
            }else if(position == GlobalVars.Categorias.size()+14){
                nextScreen = new Intent(getApplicationContext(), VentajasWebActivity.class);
                startActivity(nextScreen);
                //finish();
            }else if(position == GlobalVars.Categorias.size()+15){
                nextScreen = new Intent(getApplicationContext(), CalidadWebActivity.class);
                startActivity(nextScreen);
                //finish();
            }else if(position == GlobalVars.Categorias.size()+16){
                nextScreen = new Intent(getApplicationContext(), CuidadoWebActivity.class);
                startActivity(nextScreen);
                //finish();
            }else if(position == GlobalVars.Categorias.size()+17){
                nextScreen = new Intent(getApplicationContext(), GastosEnvioActivity.class);
                startActivity(nextScreen);
                //finish();
            }else if(position == GlobalVars.Categorias.size()+18){
                nextScreen = new Intent(getApplicationContext(), GarantiaSatisfaccionActivity.class);
                startActivity(nextScreen);
                //finish();
            }
        }
        else
        {
            /*GlobalVars.momentSection = 1;
            GlobalVars.momentSub = 0;
            //mTitle = MainSections[GlobalVars.momentSection];

            Intent nextScreen;
            nextScreen = new Intent(getApplicationContext(), SubCategoriaActivity.class);
            startActivity(nextScreen);
            finish();*/
        }
        /* //Put your id to your next Intent*/
    }

    public void onSectionAttached(int number) {
        mTitle = MainSections[number-1];
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        //actionBar.setIcon(R.drawable.icono);
        actionBar.setTitle(mTitle);
    }

    public void  onBackPressed ()
    {
        if(mNavigationDrawerFragment.isDrawerOpen())
        {
            AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(BaseActivity.this);
            if(GlobalVars.idioma == 1) {
                myAlertDialog.setTitle(getResources().getString(R.string.exit_dialog_eu));
                myAlertDialog.setMessage(getResources().getString(R.string.exit_dialog_message_eu));
                myAlertDialog.setPositiveButton(getResources().getString(R.string.dialog_ok_eu), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        // do something when the OK button is clicked
                        BaseActivity.this.finish();
                        System.exit(0);
                    }
                });
                myAlertDialog.setNegativeButton(getResources().getString(R.string.dialog_ko_eu), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        // do something when the Cancel button is clicked
                    }
                });
            }
            else
            {
                myAlertDialog.setTitle(getResources().getString(R.string.exit_dialog));
                myAlertDialog.setMessage(getResources().getString(R.string.exit_dialog_message));
                myAlertDialog.setPositiveButton(getResources().getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        // do something when the OK button is clicked
                        BaseActivity.this.finish();
                        System.exit(0);
                    }
                });
                myAlertDialog.setNegativeButton(getResources().getString(R.string.dialog_ko), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        // do something when the Cancel button is clicked
                    }
                });
            }
            myAlertDialog.show();
        }
        else
        {
            mNavigationDrawerFragment.open();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.

            getMenuInflater().inflate(R.menu.main, menu);

            ProgressBar = (MenuItem) menu.findItem(R.id.cargando);
            ProgressBar.setActionView(R.layout.action_bar_loader);
            if(dataCharged)
                ProgressBar.setVisible(false);
            else
                ProgressBar.setVisible(true);

            MenuItem share = (MenuItem) menu.findItem(R.id.comparte);
            share.setVisible(false);

            MenuItem item =(MenuItem) menu.findItem(R.id.carrit);
            MenuItemCompat.setActionView(item, R.layout.action_bar_notifitcation_icon);
            View menu_hotlist = (View) MenuItemCompat.getActionView(item);

            cesta_counter_t = (TextView) menu_hotlist.findViewById(R.id.hotlist_hot);
            updateHotCount(GlobalVars.cesta_count);

            new MyMenuItemStuffListener(menu_hotlist) {
                @Override
                public void onClick(View v) {
                    carritoClicked();
                }
            };

            restoreActionBar();
            return true;
        }

        return super.onCreateOptionsMenu(menu);
    }

    public void carritoClicked()
    {
        if(GlobalVars.UserData!=null) {
            //if(GlobalVars.cesta_count > 0) {
                Intent nextScreen = new Intent(getApplicationContext(), CestaActivity.class);
                startActivity(nextScreen);
            /*}else{
                if (GlobalVars.idioma == 1)
                    Toast.makeText(this, getResources().getString(R.string.producto_cesta_vacia_eu), Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(this, getResources().getString(R.string.producto_cesta_vacia), Toast.LENGTH_LONG).show();
            }*/
        }
        else
        {
            if (GlobalVars.idioma == 1)
                Toast.makeText(this, getResources().getString(R.string.producto_toast_login_eu), Toast.LENGTH_LONG).show();
            else
                Toast.makeText(this, getResources().getString(R.string.producto_toast_login), Toast.LENGTH_LONG).show();
        }
    }

    // call the updating code on the main thread,
    // so we can call this asynchronously
    public void updateHotCount(final int new_hot_number) {
        GlobalVars.cesta_count = new_hot_number;
        if (cesta_counter_t == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (new_hot_number == 0)
                    cesta_counter_t.setVisibility(View.INVISIBLE);
                else {
                    cesta_counter_t.setVisibility(View.VISIBLE);
                    cesta_counter_t.setText(Integer.toString(new_hot_number));
                }
            }
        });
    }

    public void setRefreshing(boolean refreshing) {
        if(ProgressBar == null) return;
        if(refreshing) {
            ProgressBar.setVisible(true);
            ProgressBar.setActionView(R.layout.action_bar_loader);
        }
        else {
            ProgressBar.setVisible(false);
            ProgressBar.setActionView(null);
            //invalidateOptionsMenu();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.favorito) {
            //Toast.makeText(BaseActivity.this, "Favoritos", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (id == R.id.carrit) {
            carritoClicked();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    Updates the count of notifications in the ActionBar.
     */
    private void updateNotificationsBadge(int count) {
        mNotificationsCount = count;

        // force the ActionBar to relayout its MenuItems.
        // onCreateOptionsMenu(Menu) will be called again.
        invalidateOptionsMenu();
    }

    @Override
    protected void onDestroy()
    {
        if(GlobalVars.myDB != null){
            GlobalVars.myDB.close();
        }
        //finish();
        super.onDestroy();
    }

    public void xmlCestaCharged(CestaCompra ces)
    {
        GlobalVars.CestaData = ces;
    }

    class FetchCountTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... params) {
            // example count. This is where you'd
            // query your data store for the actual count.
            return 88;
        }

        @Override
        public void onPostExecute(Integer count) {
            updateNotificationsBadge(count);
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

       @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            int lag = getArguments().getInt(ARG_SECTION_NUMBER);
            /*View rootView;
            switch (lag) {
                case 1:
                    rootView = inflater.inflate(R.layout.fragment_main, container, false);
                    break;
                default:
                    rootView = inflater.inflate(R.layout.fragment_category, container, false);
                    break;
            }
            return rootView;*/
           return null;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            //((BaseActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
            //Log.d("Base Activity", String.valueOf(GlobalVars.momentSection));
            ((BaseActivity) activity).onSectionAttached(GlobalVars.momentSection);
        }
    }

    public class LoadImage extends AsyncTask<String, String, Bitmap>
    {
        public ImageView imagen;
        private Context context;

        public LoadImage(ImageView img, Context mContext)
        {
            this.imagen = img;
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        protected Bitmap doInBackground(String... args) {
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image)
        {
            if(image != null){
                imagen.setImageBitmap(image);
            }
        }
    }

    /*public static class NetworkChangeReceiver extends BroadcastReceiver {

        public static BaseActivity myBase = null;
        public void inicializar(BaseActivity Base)
        {
            myBase = Base;
        }

        @Override
        public void onReceive(final Context context, final Intent intent) {

            if(NetworkUtil.getConnectivityStatusString(context))
            {
                myBase.deleteNetworkFailure();
            }
            else
            {
                myBase.createNetworkFailure();
            }
        }
    }*/

    public String EukarazkoHitzulpena(String val)
    {
        String lag;

        switch (val)
        {
            case "Alpargatas":
                lag = "Abarketak";
                break;
            case "Bailarinas":
                lag = "Dantzariak";
                break;
            case "Botas":
                lag = "Botak";
                break;
            case "Botines":
                lag = "Botatxoak";
                break;
            case "Deportivas":
                lag = "Kiroletakoak";
                break;
            case "Katiuskas":
                lag = "Katiuskak";
                break;
            case "Mocasines":
                lag = "Mokasinak";
                break;
            case "Náuticos":
                lag = "Nautikoak";
                break;
            case "Sandalias":
                lag = "Sandaliak";
                break;
            case "Zapatillas de casa":
                lag = "Etxeko zapatilak";
                break;
            case "Zapatos casual":
                lag = "Kasual zapatak";
                break;
            case "Zapatos de vestir":
                lag = "Jazteko zapatak";
                break;
            case "Zuecos":
                lag = "Eskalapoiak";
                break;
            case "Nueva temporada":
                lag = "Denboraldi berria";
                break;
            case "Marca A-Z":
                lag = "A-Z Marka";
                break;
            case "Marca Z-A":
                lag = "Z-A Marka";
                break;
            case "Mayor oferta":
                lag = "Beherapen haundiena";
                break;
            case "Menor oferta":
                lag = "Beherapen txikiena";
                break;
            case "Precio más bajo primero":
                lag = "Salneurri txikiena lehen";
                break;
            case "Precio más alto primero":
                lag = "Salneurri haundiena lehen";
                break;
            case "Tipo de zapato":
                lag = "Zapata mota";
                break;
            case "Alto de tacón y suela":
                lag = "Takoi altuera eta zorua";
                break;
            case "Marcas":
                lag = "Markak";
                break;
            case "Material":
                lag = "Materiala";
                break;
            case "Precio":
                lag = "Salneurria";
                break;
            case "Talla":
                lag = "Neurria";
                break;
            case "Colores":
                lag = "Koloreak";
                break;
            case "Opciones":
                lag = "Aukerak";
                break;
            case "Suela plana":
                lag = "Zoru laua";
                break;
            case "Cuña":
                lag = "Ziria";
                break;
            case "Tejido":
                lag = "Ehuna";
                break;
            case "Piel grabada":
                lag = "Larru grabatua";
                break;
            case "Piel lisa":
                lag = "Larru laua";
                break;
            case "Suela de goma":
                lag = "Gomazko zorua";
                break;
            default:
                lag = null;
                break;
        }

        if(lag==null) {
            return val;
        }
        else {
            return lag;
        }
    }

    static abstract class MyMenuItemStuffListener implements View.OnClickListener {
        private View view;

        MyMenuItemStuffListener(View view) {
            this.view = view;
            view.setOnClickListener(this);
        }

        @Override abstract public void onClick(View v);
    }
}
