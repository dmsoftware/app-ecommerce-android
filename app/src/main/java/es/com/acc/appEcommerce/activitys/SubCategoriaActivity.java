package es.com.acc.appEcommerce.activitys;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.FiltrosRow;
import es.com.acc.appEcommerce.entities.Producto;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Filtros;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Productos;
import es.com.acc.appEcommerce.views.FiltroDialog;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;
import es.com.acc.appEcommerce.views.RadioDialog;

public class SubCategoriaActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, RadioDialog.DialogListener, FiltroDialog.DialogListener
{
    private static final String TAG = "dialog";
    private JSONTask_Productos json;
    private JSONTask_Filtros jsonFiltros;
    public String[] SecundarySections;
    public String[] OrderOptions;
    public int secLength = 0;
    public int ordersLength = 0;
    //public int filtersLength = 0;
    public Button botCategorias;
    public Button botOrden;
    public Button botFiltros;
    public List<Producto> Productos;
    public List<FiltrosRow> Filtros;

    public String momentOrder="asc";
    public String momentFilter="";
    public String mainCategory="";
    public String momentCategory="";
    public String maxResults="20";
    public String curPage="1";

    public int DialogType = 0;
    public RelativeLayout container;
    public String SERVER_URL = "http://images.kukimba.com/item/233x155/";

    public ScrollView Scroller;
    public boolean scrollStarted = false;

    public FiltroDialog filtroFragment = null;


    @Override
    public void onStart() {
        super.onStart();

        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);
    }

    @Override
    public void  onBackPressed ()
    {
        if(dataCharged==true)goBack();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.comparte) {
            //Toast.makeText(ProductSheetActivity.this, "Compartir", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (id == R.id.favorito) {
            //Toast.makeText(ProductSheetActivity.this, "Favoritos", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (id == R.id.carrit) {
            carritoClicked();
            return true;
        }
        else if (id == android.R.id.home) {
            if(dataCharged==true) goBack();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_subcategoria);

        container =  (RelativeLayout) findViewById(R.id.cellContainer);

        if(GlobalVars.momentSection - 2 > GlobalVars.Categorias.size())
        {
            GlobalVars.momentSection = 2;
        }

        //mTitle = GlobalVars.Categorias.get(GlobalVars.momentSection-1).principal.Name;
        mTitle = MainSections[GlobalVars.momentSection];
        mainCategory = GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.MerchantID;

        secLength = GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.size();
        SecundarySections = new String[secLength];
        for (int i = 0; i < secLength; i++) {
            if(GlobalVars.idioma==1)
                SecundarySections[i] = EukarazkoHitzulpena(GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.get(i).principal.Name);
            else
                SecundarySections[i] = GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.get(i).principal.Name;
        }

        ordersLength = GlobalVars.Ordenes.size();
        OrderOptions = new String[ordersLength];
        for (int i = 0; i < ordersLength; i++) {
            if(GlobalVars.idioma==1)
                OrderOptions[i] = EukarazkoHitzulpena(GlobalVars.Ordenes.get(i).Name);
            else
                OrderOptions[i] = GlobalVars.Ordenes.get(i).Name;

        }
        momentOrder = GlobalVars.Ordenes.get(GlobalVars.momentOrd).ID;

        botCategorias=(Button)findViewById(R.id.butSubcategoria);
        if(secLength>0) {
            botCategorias.setText(SecundarySections[GlobalVars.momentSub]);
            momentCategory = GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.get(GlobalVars.momentSub).principal.MerchantID;
        }
        else
        {
            if(GlobalVars.idioma == 1)
                botCategorias.setText(getResources().getString(R.string.subcategoria_but_no_categoria_eu));
            else
                botCategorias.setText(getResources().getString(R.string.subcategoria_but_no_categoria));
        }

        botOrden=(Button)findViewById(R.id.butOrden);
        if(GlobalVars.idioma == 1)
            botOrden.setText(getResources().getString(R.string.subcategoria_but_orden_eu));
        else
            botOrden.setText(getResources().getString(R.string.subcategoria_but_orden));


        botFiltros=(Button)findViewById(R.id.butFiltros);
        if(GlobalVars.idioma == 1)
            botFiltros.setText(getResources().getString(R.string.subcategoria_but_filtro_eu));
        else
            botFiltros.setText(getResources().getString(R.string.subcategoria_but_filtro));


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

            // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                    R.id.navigation_drawer,
                    (DrawerLayout) findViewById(R.id.drawer_layout),
                    CategoriasPrincipales,R.drawable.ic_drawer);

        Scroller = (ScrollView)findViewById(R.id.scroller);
        Scroller.post(new Runnable(){

            @Override
            public void run() {
                ViewTreeObserver observer = Scroller.getViewTreeObserver();
                observer.addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener(){

                    @Override
                    public void onScrollChanged()
                    {
                        if(scrollStarted) {
                            int posY = Scroller.getChildAt(0).getHeight() - Scroller.getScrollY();
                            //System.out.println(" :::::: onScrollChanged: >> " + Scroller.getChildAt(0).getHeight() + " :: " + Scroller.getScrollY() + " :: " + GlobalVars.screenWidth * 2);
                            if (posY < GlobalVars.screenWidth * 2) {
                                scrollStarted = false;
                                chargeNextGroup();
                            }
                        }
                        //int scrollX = Scroller.getScrollX();
                        //int scrollY = Scroller.getScrollY();
                    }});
            }});

        setRefreshing(true);

        dataCharged = false;

        momentFilter = momentCategory;

        String lag = "es";
        if(GlobalVars.idioma == 1)lag = "eu";
        String lagUser = "";
        if(GlobalVars.UserData!=null){
            lagUser = String.valueOf(GlobalVars.UserData.ID);
        }

        if(GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.Name.compareTo("Outlet")==0)
        {
            String lagSub = GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.MerchantID;

            tracker.setScreenName("VistaCategorias > "+
                    lagSub.replaceAll("\\D+","")+" | "+
                    GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.Name);

            lagSub = String.valueOf(GlobalVars.Categorias.get(GlobalVars.momentSection - 2).principal.MerchantID);

            // Send a screen view.
            tracker.send(new HitBuilders.ScreenViewBuilder()
                    .setCustomDimension(1, lag)
                    .setCustomDimension(2, lagUser)
                    .setCustomDimension(3, "(not set)")
                    .setCustomDimension(4, "(not set)")
                    .setCustomDimension(5, "(not set)")
                    .setCustomDimension(6, "(not set)")
                    .setCustomDimension(7, lagSub.replaceAll("\\D+", "") + "|" +
                            GlobalVars.Categorias.get(GlobalVars.momentSection - 2).principal.Name)
                    .setCustomDimension(8, Locale.getDefault().getLanguage())
                    .build());
        }
        else
        {
            String lagSub = GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.get(GlobalVars.momentSub).principal.MerchantID;
            tracker.setScreenName("VistaCategorias > "+
                    GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.Name+" > "+
                    lagSub.replaceAll("\\D+","")+" | "+
                    GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.get(GlobalVars.momentSub).principal.Name);

            lagSub = GlobalVars.Categorias.get(GlobalVars.momentSection - 2).secundarios.get(GlobalVars.momentSub).principal.MerchantID;
            String lagSub2 = GlobalVars.Categorias.get(GlobalVars.momentSection - 2).principal.MerchantID;
            // Send a screen view.
            tracker.send(new HitBuilders.ScreenViewBuilder()
                    .setCustomDimension(1, lag)
                    .setCustomDimension(2, lagUser)
                    .setCustomDimension(3, "(not set)")
                    .setCustomDimension(4, "(not set)")
                    .setCustomDimension(5, String.valueOf(lagSub.replaceAll("\\D+", ""))
                            + "|" + GlobalVars.Categorias.get(GlobalVars.momentSection - 2).secundarios.get(GlobalVars.momentSub).principal.Name)
                    .setCustomDimension(6, "(not set)")
                    .setCustomDimension(7, String.valueOf(lagSub2.replaceAll("\\D+", "")) + "|" +
                            GlobalVars.Categorias.get(GlobalVars.momentSection - 2).principal.Name)
                    .setCustomDimension(8, Locale.getDefault().getLanguage())
                    .build());
        }
        // Set screen name.



        String lagMC = mainCategory;

        if(mainCategory.equals("ma")) {
            lagMC = "";
        }

        json = new JSONTask_Productos(this,SubCategoriaActivity.this,lagMC,momentFilter,momentOrder,maxResults,curPage);
        json.execute();


        jsonFiltros = new JSONTask_Filtros(this,SubCategoriaActivity.this,lagMC,momentFilter);
        //jsonFiltros = new JSONTask_Filtros(this,SubCategoriaActivity.this,mainCategory,"");
        jsonFiltros.execute();
    }


    public void chargeNextGroup()
    {
        int lag = Integer.valueOf(curPage) + 1;
        curPage = Integer.toString(lag);
        //System.out.println(" ::::::::::::::::::::::::::::: chargeNextGroup:  "+curPage);
        setRefreshing(true);
        //json = new JSONTask_Productos(this,SubCategoriaActivity.this,mainCategory,momentFilter,momentOrder,maxResults,curPage);

        String lagMC = mainCategory;

        if(mainCategory.equals("ma")) {
            lagMC = "";
        }

        json = new JSONTask_Productos(this,SubCategoriaActivity.this,lagMC,momentFilter,momentOrder,maxResults,curPage);
        json.execute();
    }

    public void xmlCharged(List<Producto> Prods)
    {
        int oldIndex = 0;
        if(Productos == null)
        {
            Productos = new ArrayList<Producto>();
            for(Producto pro: Prods)
            {
                Productos.add(pro);
            }
        }
        else
        {
            oldIndex = Productos.size();
            for(Producto pro: Prods)
            {
                Productos.add(pro);
            }
        }

        int scrollHeight = Scroller.getChildAt(0).getHeight();

        for (int number = oldIndex; number < Productos.size(); number++)
        {
            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            RelativeLayout menuLayout = (RelativeLayout) inflater.inflate(R.layout.product_cell, null, false);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(GlobalVars.screenWidth/2-2, (int) (GlobalVars.screenWidth/1.5)-1);
            RelativeLayout cont = (RelativeLayout)menuLayout.findViewById(R.id.productContainer);
            cont.setLayoutParams(params);
            //cont.setId(Productos.get(number).ID);
            cont.setId(number);

            ImageView img = (ImageView) menuLayout.findViewById(R.id.imagen);
            //img.getLayoutParams().height = (int) (GlobalVars.screenWidth/2.8/1.5);
            //img.getLayoutParams().width = (int) (GlobalVars.screenWidth/2.8);

            TextView descripcion = (TextView) menuLayout.findViewById(R.id.brand);
            descripcion.setText(Productos.get(number).Brand);

            TextView name = (TextView) menuLayout.findViewById(R.id.name);
            name.setText(Productos.get(number).Name);

            TextView precio = (TextView) menuLayout.findViewById(R.id.price);
            //precio.setText(Float.toString(Productos.get(number).Price)+" €");
            precio.setText( String.format( "%.2f", Productos.get(number).Price)+" €");

            TextView precioOriginal = (TextView) menuLayout.findViewById(R.id.originalprice);
            if(Productos.get(number).Price != Productos.get(number).OriginalPrice)
            {
                //precioOriginal.setText(Float.toString(Productos.get(number).OriginalPrice) + " €");
                precioOriginal.setText( String.format( "%.2f", Productos.get(number).OriginalPrice)+" €");
                precioOriginal.setPaintFlags(precioOriginal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                TextView rebajaText = (TextView) menuLayout.findViewById(R.id.rebajatext);
                int lag = (int) Math.floor((100 - Productos.get(number).Price * 100 / Productos.get(number).OriginalPrice));
                rebajaText.setText(lag+" %");
            }
            else
            {
                precioOriginal.setVisibility(View.INVISIBLE);
                RelativeLayout rebajaPanel = (RelativeLayout) menuLayout.findViewById(R.id.rebajapanel);
                rebajaPanel.setVisibility(View.INVISIBLE);
            }
            //menuLayout.setId(Productos.get(number).ID);

            LinearLayout botonC = (LinearLayout)menuLayout.findViewById(R.id.botonCelda);
            botonC.setId(number);
            //menuLayout.setId(number);

            RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(GlobalVars.screenWidth/2-2, (int) (GlobalVars.screenWidth/1.8)-1);
            int x = 2 + ((number-oldIndex)%2) * (GlobalVars.screenWidth/2 - 1);
            int y = (int) (scrollHeight + 1 + ((number-oldIndex)/2) * GlobalVars.screenWidth/1.8);
            params2.setMargins(x, y, 1, 0);
            container.addView(menuLayout,params2);

            String pathURL = SERVER_URL;
            pathURL += Productos.get(number).Image[0];
            //pathURL += MerchID;

            //System.out.println(" ::::::::::::::::::::::::::::: load image:  "+number+" >> "+Productos.get(number).Image[0]);

            Product product = new Product()
                    .setId(String.valueOf(Productos.get(number).ID))
                    .setName(Productos.get(number).Name)
                    .setCategory(momentCategory)
                    .setBrand(Productos.get(number).Brand)
                    .setVariant(mainCategory)
                    .setPosition(number);
            HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                    .addImpression(product, "Listado Productos");

            tracker.setScreenName("Listado Productos");
            tracker.send(builder.build());

            LoadImage imgJson = new LoadImage(img,SubCategoriaActivity.this);
            imgJson.execute(pathURL);
        }
        scrollStarted = true;

        dataCharged = true;
        setRefreshing(false);

        //sendAnalitycHit("SubCategoria","es",String.valueOf(GlobalVars.UserData.ID),"","",momentCategory,"",mainCategory,"es");
    }

    @Override
    public void xmlFiltrsCharged(List<FiltrosRow> Filtrs)
    {
        //filtersLength = Filtrs.size();
        Filtros = Filtrs;
        //if((filtroFragment!=null) && (filtersLength>0))
        if(filtroFragment!=null)
        {
            filtroFragment.setFiltros(Filtros);
        }
    }

    public void subcategoriaClick(View view)
    {
        if(secLength>0) {
            DialogType = 0;
            if(GlobalVars.idioma == 1)
                showDialog(getResources().getString(R.string.subcategoria_but_sel_cat_eu), SecundarySections, GlobalVars.momentSub);
            else
                showDialog(getResources().getString(R.string.subcategoria_but_sel_cat), SecundarySections, GlobalVars.momentSub);
        }
    }

    public void ordenClick(View view) {
        if(ordersLength>0)
        {
            DialogType = 1;
            if(GlobalVars.idioma == 1)
                showDialog(getResources().getString(R.string.subcategoria_but_sel_ord_eu),OrderOptions,GlobalVars.momentOrd);
            else
                showDialog(getResources().getString(R.string.subcategoria_but_sel_ord),OrderOptions,GlobalVars.momentOrd);
        }
    }

    public void filtrosClick(View view) {
        if(dataCharged==true)
        {
            DialogType = 2;
            if(GlobalVars.idioma == 1)
                showDialog(getResources().getString(R.string.subcategoria_but_sel_fil_eu),OrderOptions,0);
            else
                showDialog(getResources().getString(R.string.subcategoria_but_sel_fil),OrderOptions,0);

        }
    }

    public void showDialog(String titulo, String[] values, int selectedItem) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        AlertDialog dialog;
        if(DialogType == 2)
        {
            filtroFragment = new FiltroDialog();
            filtroFragment.setValues(this,titulo,Filtros,getApplicationContext());
            filtroFragment.show(fragmentManager, TAG);
            dialog = (AlertDialog) filtroFragment.getDialog();
        }
        else
        {
            RadioDialog radioFragment = new RadioDialog();
            radioFragment.setValues(titulo,values,selectedItem);
            radioFragment.show(fragmentManager, TAG);
            dialog = (AlertDialog) radioFragment.getDialog();
        }
    }

    public void getFilters(String value)
    {
        momentFilter = value;

        String lagMC = mainCategory;

        if(mainCategory.equals("ma")) {
            lagMC = "";
        }

        jsonFiltros = new JSONTask_Filtros(this,SubCategoriaActivity.this,lagMC,momentFilter);
        jsonFiltros.execute();
    }

    public void onClick(View view)
    {
        try {
            Intent intent = new Intent(SubCategoriaActivity.this, ProductSheetActivity.class);

            int number = view.getId();

            Product product = new Product()
                    .setId(String.valueOf(Productos.get(number).ID))
                    .setName(Productos.get(number).Name)
                    .setCategory(momentCategory)
                    .setBrand(Productos.get(number).Brand)
                    .setVariant(mainCategory)
                    .setPosition(number);
            ProductAction productAction = new ProductAction(ProductAction.ACTION_CLICK)
                    .setProductActionList("ver detalle de producto");
            HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                    .addProduct(product)
                    .setProductAction(productAction);

            tracker.setScreenName("Listado Productos");
            tracker.send(builder.build());

            GlobalVars.ProductID = Productos.get(number).ID;
            startActivity(intent);
            //finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onDialogPositiveClick(DialogFragment dialog, int value) {
        // User touched the dialog's positive button
        //Toast.makeText(getApplicationContext(), String.valueOf(value), Toast.LENGTH_SHORT).show();
        if(DialogType==0) {
            GlobalVars.momentSub = value;
            botCategorias.setText(SecundarySections[value]);
            momentFilter = GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.get(value).principal.MerchantID;
            momentCategory = momentFilter;
            //System.out.println(" ::::::::::::::::::::::::::::: onDialogPositiveClick: >> "+momentCategory);
            container.removeAllViews();
            Productos = null;
            curPage = "1";
            setRefreshing(true);

            String lagMC = mainCategory;

            if(mainCategory.equals("ma")) {
                lagMC = "";
            }

            jsonFiltros = new JSONTask_Filtros(this,SubCategoriaActivity.this,lagMC,momentFilter);
            jsonFiltros.execute();
            json = new JSONTask_Productos(this, SubCategoriaActivity.this, lagMC, momentFilter, momentOrder,maxResults,curPage);
            json.execute();
        }
        else if(DialogType==1) {
            momentOrder = GlobalVars.Ordenes.get(value).ID;
            GlobalVars.momentOrd = value;
            container.removeAllViews();
            Productos = null;
            curPage = "1";
            setRefreshing(true);

            String lagMC = mainCategory;

            if(mainCategory.equals("ma")) {
                lagMC = "";
            }

            json = new JSONTask_Productos(this, SubCategoriaActivity.this, lagMC, momentFilter, momentOrder,maxResults,curPage);
            json.execute();
        }
        else if(DialogType==2) {
            container.removeAllViews();
            if(value>-1) {
                GlobalVars.momentSub = value;
                botCategorias.setText(SecundarySections[value]);
            }
            Productos = null;
            curPage = "1";
            setRefreshing(true);

            String lagMC = mainCategory;

            if(mainCategory.equals("ma")) {
                lagMC = "";
            }

            json = new JSONTask_Productos(this, SubCategoriaActivity.this, lagMC, momentFilter, momentOrder,maxResults,curPage);
            json.execute();
        }
    }
}