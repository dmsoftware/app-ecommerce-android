package es.com.acc.appEcommerce.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.activitys.BaseActivity;
import es.com.acc.appEcommerce.entities.Filtro;
import es.com.acc.appEcommerce.entities.FiltrosRow;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.RangeSeekBar;

import static android.graphics.Color.parseColor;

/**
 * Created by Imanol on 30/04/2015.
 */
class filters
{
    public ArrayList<String> vals;
    public boolean inited = false;
}

public class FiltroDialog extends DialogFragment implements View.OnClickListener
{
    private String[] mainFilters;
    private String[] mainSelections;
    private filters[] filterOptions;
    private String myTitle;
    private int choice = -1;

    public String ColoresValue = "";

    public boolean dialogInited = false;

    public View myView;

    public List<FiltrosRow> Filtros;

    private DialogListener listener;

    public LinearLayout Contenedor;

    public Context context;

    public String selMinVal;
    public String selMaxVal;
    public TextView txt2;
    public TextView txt3;

    boolean priceChanged = false;

    public BaseActivity suCa;

    public interface DialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, int cho);
        public void getFilters(String value);
    }

    public void setValues(BaseActivity Ca, String tit, List<FiltrosRow> Filtrs, Context cont)
    {
        suCa = Ca;
        myTitle = tit;
        setFiltros(Filtrs);
        //context = cont;
    }

    public  void setFiltros(List<FiltrosRow> Filtrs)
    {
        Filtros = Filtrs;
        mainFilters = new String[Filtrs.size()];
        mainSelections = new String[Filtrs.size()];
        filterOptions = new filters[Filtrs.size()];

        int kont1 = 0;
        for( FiltrosRow filt: Filtrs)
        {
            if(GlobalVars.idioma == 1)
                mainFilters[kont1] = suCa.EukarazkoHitzulpena(filt.Name);
            else
                mainFilters[kont1] = filt.Name;


            mainSelections[kont1] = "";
            filterOptions[kont1] = new filters();
            filterOptions[kont1].vals = new ArrayList<String>();
            filterOptions[kont1].vals.add("Sin selección");
            for( Filtro fil: filt.Value)
            {

                if(GlobalVars.idioma == 1)
                    filterOptions[kont1].vals.add(suCa.EukarazkoHitzulpena(fil.Name));
                else
                    filterOptions[kont1].vals.add(fil.Name);
            }
            kont1++;
        }
        if(dialogInited) {
            refreshView();
        }

    }

    // Override the Fragment.onAttach() method to instantiate the
    // NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = activity;
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the
            // host
            listener = (DialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement DialogListener");
        }
    }

    public void sendFilters()
    {
        String lag = "";
        Boolean first = true;

        int kont = 0;
        for( FiltrosRow filt: Filtros)
        {
            switch (filt.Type)
            {
                case "cmb":
                    if(mainSelections[kont].length()>0)
                    {
                        if(first) {
                            first = false;
                        }
                        else {
                            lag += ";";
                        }
                        lag += mainSelections[kont];
                    }
                    break;
                case "chk":
                    if(ColoresValue.length()>0)
                    {
                        if(first) {
                            first = false;
                        }
                        else {
                            lag += ";";
                        }
                        lag +=  ColoresValue;
                    }
                    break;
                case "sli":
                    if(priceChanged==true) {
                        if (first) {
                            first = false;
                        } else {
                            lag += ";";
                        }
                        lag += "pr" + selMinVal + "-" + selMaxVal;
                    }
                    break;
            }
            kont++;
        }
        listener.getFilters(lag);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
    //public void init()

        //ContextThemeWrapper context = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Dialog_NoActionBar);
        //AlertDialog.Builder builder = new AlertDialog.Builder(context);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);

        myView = inflater.inflate(R.layout.filtros_view, null);
        Contenedor = (LinearLayout) myView.findViewById(R.id.contenedor);

        refreshView();

        dialogInited = true;
        priceChanged = false;

        if(GlobalVars.idioma == 1) {
            builder.setTitle(myTitle)
                    .setView(myView)
                    .setPositiveButton(R.string.dialog_ok_eu,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialogInited = false;
                                    listener.onDialogPositiveClick(FiltroDialog.this, choice);
                                }
                            })
                    .setNegativeButton(R.string.dialog_ko_eu,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    //listener.onDialogNegativeClick(RadioDialog.this);
                                }
                            });
        }
        else
        {
            builder.setTitle(myTitle)
                    .setView(myView)
                    .setPositiveButton(R.string.dialog_ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialogInited = false;
                                    listener.onDialogPositiveClick(FiltroDialog.this, choice);
                                }
                            })
                    .setNegativeButton(R.string.dialog_ko,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    //listener.onDialogNegativeClick(RadioDialog.this);
                                }
                            });
        }
        return builder.create();
    }

    public void refreshView()
    {
        int kont1 = 0;

        Contenedor.removeAllViews();

        txt2 = new TextView(context);
        txt3 = new TextView(context);

        for( FiltrosRow filt: Filtros)
        {
            switch (filt.Type)
            {
                case "cmb":
                    filterOptions[kont1].inited = false;
                    TextView txt = new TextView(context);
                    int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, px);
                    params.setMargins(20, 20, 20, 0);
                    txt.setLayoutParams(params);
                    txt.setGravity(Gravity.CENTER);
                    txt.setText(mainFilters[kont1]);
                    txt.setTypeface(null, Typeface.BOLD);

                    params.setMargins(20, 0, 20, 20);
                    Spinner spin1 = new Spinner(context);
                    spin1.setLayoutParams(params);

                    //ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item,  filterOptions[kont1].vals);
                    //spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(context, R.layout.my_dialog_spinner_item, filterOptions[kont1].vals);
                    spinnerArrayAdapter1.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    spin1.setAdapter(spinnerArrayAdapter1);
                    //if (!dialogInited) {
                        //spin1.setOnTouchListener(new AdapterView.OnTouchListener() {
                    spin1.setId(kont1);
                    spin1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                                // your code here
                                if (filterOptions[parentView.getId()].inited) {
                                    if (position > 0) {
                                        if (parentView.getId() == 0) {
                                            choice = position - 1;
                                        }
                                        mainSelections[parentView.getId()] = Filtros.get(parentView.getId()).Value.get(position - 1).Id;
                                    }
                                    else
                                    {
                                        if (parentView.getId() == 0) {
                                            choice = -1;
                                        }
                                        mainSelections[parentView.getId()] = "";
                                    }
                                    sendFilters();
                                }
                                else {
                                    if (position > 0) {
                                        if (parentView.getId() == 0) {
                                            choice = position - 1;
                                        }
                                        mainSelections[parentView.getId()] = Filtros.get(parentView.getId()).Value.get(position - 1).Id;
                                    }
                                    filterOptions[parentView.getId()].inited = true;
                                }
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                    });
                    Contenedor.addView(txt);
                    Contenedor.addView(spin1);
                    if(filt.Selected != "")
                    {
                        for(Filtro fil: Filtros.get(kont1).Value) {
                            if(fil.Id.equals(filt.Selected))
                            {
                                spin1.setSelection(Filtros.get(kont1).Value.indexOf(fil)+1);
                            }
                        }
                    }
                    //}

                    break;
                case "sli":
                    txt = new TextView(context);
                    px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
                    params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, px);
                    params.setMargins(20, 10, 20, 0);
                    txt.setLayoutParams(params);
                    txt.setGravity(Gravity.CENTER);
                    txt.setText(mainFilters[kont1]);
                    txt.setTypeface(null, Typeface.BOLD);

                    LinearLayout linLay = new LinearLayout(context);
                    LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, px);
                    params.setMargins(20, 0, 20, 0);
                    linLay.setOrientation(LinearLayout.HORIZONTAL);
                    linLay.setLayoutParams(params2);

                    String lag1= filterOptions[kont1].vals.get(1).replace(",", ".");
                    String lag2= filterOptions[kont1].vals.get(2).replace(",", ".");

                    float minVal = 0;
                    if(lag1.length()>0) minVal = Float.parseFloat(lag1);

                    float maxVal = 0;
                    if(lag1.length()>0)maxVal = Float.parseFloat(lag2);

                    int px2 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
                    LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(0, px2);
                    params3.weight = 0.5f;
                    //params3.setMargins(20, 10, 0, 0);
                    txt2.setLayoutParams(params3);
                    txt2.setGravity(Gravity.CENTER_HORIZONTAL);
                    selMinVal = String.valueOf(minVal);
                    txt2.setText(String.format( "%.2f",minVal)+" €");
                    linLay.addView(txt2);

                    //params3.setMargins(0, 10, 20, 0);
                    txt3.setLayoutParams(params3);
                    txt3.setGravity(Gravity.CENTER_HORIZONTAL);
                    selMaxVal = String.valueOf(maxVal);
                    txt3.setText(String.format( "%.2f",maxVal)+" €");
                    linLay.addView(txt3);

                    // create RangeSeekBar as Integer range between 20 and 75
                    RangeSeekBar<Integer> seekBar = new RangeSeekBar<Integer>((int)minVal, (int)maxVal, context);
                    seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
                        @Override
                        public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                            // handle changed range values
                            Log.i("seekBar", "User selected new range values: MIN=" + minValue + ", MAX=" + maxValue);
                            priceChanged = true;
                            selMaxVal = String.valueOf(maxValue);
                            selMinVal = String.valueOf(minValue);
                            txt2.setText(String.valueOf(minValue)+",00 €");
                            txt3.setText(String.valueOf(maxValue)+",00 €");
                            sendFilters();
                        }
                    });

                    if(filt.Selected != "")
                    {
                        String lag = filt.Selected.replace("pr","");
                        String[] splited = lag.split("-");
                        seekBar.setSelectedMinValue(Math.round(Float.valueOf(splited[0])));
                        seekBar.setSelectedMaxValue(Math.round(Float.valueOf(splited[1])));
                        selMaxVal = splited[0];
                        selMinVal = splited[1];
                        txt2.setText(String.valueOf(splited[0])+",00 €");
                        txt3.setText(String.valueOf(splited[1])+",00 €");
                    }

                    Contenedor.addView(txt);
                    Contenedor.addView(linLay);
                    Contenedor.addView(seekBar);
                    break;
                case "chk":

                    txt = new TextView(context);
                    //px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics());
                    //px2 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getResources().getDisplayMetrics());
                    px = GlobalVars.screenWidth/13;
                    px2 = GlobalVars.screenWidth/9;
                    params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, px2);
                    params.setMargins(20, 10, 20, 0);
                    txt.setLayoutParams(params);
                    txt.setGravity(Gravity.CENTER);
                    txt.setText(mainFilters[kont1]);
                    txt.setTypeface(null, Typeface.BOLD);

                    LinearLayout Botonera = new LinearLayout(context);
                    //final Button[] rb = new Button[filt.Value.size()];
                    //Button rg = new Button(context); //create the RadioGroup
                    //rg.setOrientation(RadioGroup.HORIZONTAL);//or RadioGroup.VERTICAL
                    Botonera.setLayoutParams(params);
                    Botonera.setId(kont1);

                    int kont = 0;
                    for( Filtro Fil: filt.Value)
                    {
                        String color="#ffffffff";
                        switch (Fil.Id)
                        {
                            case "cl5":
                                color="#ffd1b411";
                                break;
                            case "cl6":
                                color="#ff5a3c15";
                                break;
                            case "cl2":
                                color="#ffcc5312";
                                break;
                            case "cl9":
                                color="#ffb80c2d";
                                break;
                            case "cl10":
                                color="#ff503ec7";
                                break;
                            case "cl4":
                                color="#ff151516";
                                break;
                            case "cl1":
                                color="#ffa2a2a3";
                                break;
                            case "cl3":
                                color="#ff067812";
                                break;
                            case "cl8":
                                color="#ffff22b8";
                                break;
                            case "cl7":
                                color="#ff123456";
                                break;
                        }

                        if(filt.Selected.equals(Fil.Id))
                            params = new LinearLayout.LayoutParams(px2, px2);
                        else
                            params = new LinearLayout.LayoutParams(px, px);
                        params.setMargins(5, 0, 0, 0);

                        Button rb = new Button(context);
                        Botonera.addView(rb); //the RadioButtons are added to the radioGroup instead of the layout
                        rb.setBackgroundColor(parseColor(color));
                        rb.setOnClickListener(this);
                        rb.setLayoutParams(params);
                        rb.setId(kont);
                        kont++;
                    }

                    Contenedor.addView(txt);
                    Contenedor.addView(Botonera);//you add the whole RadioGroup to the layout
                    break;
            }
            kont1++;
        }
    }

    @Override
    public void onClick(View v) {

        int lag = ((View) v.getParent()).getId();

        if(Filtros.get(lag).Selected.equals(Filtros.get(lag).Value.get(v.getId()).Id))
            ColoresValue="";
        else
            ColoresValue = Filtros.get(lag).Value.get(v.getId()).Id;

        sendFilters();
    }
}