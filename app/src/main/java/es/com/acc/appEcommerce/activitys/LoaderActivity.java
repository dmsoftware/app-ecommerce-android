package es.com.acc.appEcommerce.activitys;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;

import java.util.List;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.CategoriaRow;
import es.com.acc.appEcommerce.entities.CestaCompra;
import es.com.acc.appEcommerce.entities.Orden;
import es.com.acc.appEcommerce.entities.UserLogin;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Categorias;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Cesta;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Ordenes;

/**
 * Created by Imanol on 27/04/2015.
 */
public class LoaderActivity extends AppCompatActivity
{
    private JSONTask_Ordenes jsonOrder;
    private JSONTask_Categorias json;
    private JSONTask_Cesta jsonCesta;

    @Override
    protected void onStart()
    {
        super.onStart();

        setContentView(R.layout.activity_loader);

        if(checkInternetConnection())
        {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            GlobalVars.screenWidth = size.x;
            GlobalVars.screenHeight = size.y;

            SharedPreferences mPrefs= getSharedPreferences("mypref", 0);

            GlobalVars.UserData = new UserLogin();
            GlobalVars.UserData.Birthdate = mPrefs.getString("birthdate", "");
            GlobalVars.UserData.Email = mPrefs.getString("email", "");
            GlobalVars.UserData.Genre = mPrefs.getInt("genre", 0);
            GlobalVars.UserData.ID = mPrefs.getInt("id", -1);
            GlobalVars.UserData.Name = mPrefs.getString("name", "");
            GlobalVars.UserData.Surname1 = mPrefs.getString("surname1", "");
            GlobalVars.UserData.Surname2 = mPrefs.getString("surname2", "");
            GlobalVars.UserData.Telephone = mPrefs.getString("telephone", "");
            GlobalVars.UserData.addToNewsletter = mPrefs.getBoolean("addnewsletter", false);
            GlobalVars.password = mPrefs.getString("password", "");

            GlobalVars.CestaID = mPrefs.getInt("cestaid",-1);

            GlobalVars.idioma = mPrefs.getInt("idioma",-1);

            //Aqui solicitamos el primer servicio web

            //jsonOrder = new JSONTask_Ordenes(this,LoaderActivity.this);
            //jsonOrder.execute();
        }
        else
        {
            AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(LoaderActivity.this);
            myAlertDialog.setTitle("Error de conexión");
            myAlertDialog.setMessage("Se ha producido un error en la red. Por favor, comprueba tu conexión.");
            myAlertDialog.setPositiveButton("Terminar", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface arg0, int arg1) {
                    // do something when the OK button is clicked
                    LoaderActivity.this.finish();
                    System.exit(0);
                }});
            myAlertDialog.setNegativeButton("Configuración de conexión", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface arg0, int arg1) {
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                }});
            myAlertDialog.show();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        // The rest of your onStop() code.
        //EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        SharedPreferences mPrefs= getSharedPreferences("mypref", 0);

        GlobalVars.UserData = new UserLogin();
        String lagPath = mPrefs.getString("shareImage", "");
        if(lagPath.length()>0) {
            Uri shareImage = Uri.parse(lagPath);
            getContentResolver().delete(shareImage, null, null);
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    public void xmlOrdenesCharged(List<Orden> Orders) {
        //Categorias =  new ArrayList<CategoriaRow>();
        GlobalVars.Ordenes = Orders;

        json = new JSONTask_Categorias(this,LoaderActivity.this);
        json.execute();
    }

    public void xmlCharged(List<CategoriaRow> Cat) {
        //Categorias =  new ArrayList<CategoriaRow>();
        GlobalVars.Categorias = Cat;
        //System.out.println(" ::::::::::::::::::::::::::::: openWebURL : >> "+Categorias);

        if(GlobalVars.UserData != null) {
            jsonCesta = new JSONTask_Cesta(this, LoaderActivity.this,GlobalVars.CestaID,GlobalVars.UserData.ID);
            jsonCesta.execute();
        }
        else
        {
            Intent nextScreen;
            if(GlobalVars.idioma == -1){
                GlobalVars.momentSection = 10;
                nextScreen = new Intent(getApplicationContext(), ConfiguracionViewActivity.class);
            }
            else {
                nextScreen = new Intent(getApplicationContext(), DestacadosActivity.class);
            }
            startActivity(nextScreen);
            finish();
        }
    }

    public void xmlCestaCharged(CestaCompra ces)
    {
        GlobalVars.CestaData = ces;

        if(GlobalVars.CestaData!=null)
            GlobalVars.cesta_count = GlobalVars.CestaData.Items.length;

        //Intent nextScreen = new Intent(getApplicationContext(), SubCategoriaActivity.class);
        Intent nextScreen = new Intent(getApplicationContext(), DestacadosActivity.class);
        startActivity(nextScreen);
        finish();
    }

    public boolean checkInternetConnection()
    {
        ConnectivityManager connectivity = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null)
        {
            NetworkInfo[] inf = connectivity.getAllNetworkInfo();
            if (inf != null)
                for (int i = 0; i < inf.length; i++)
                    if (inf[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
        }
        return false;
    }
}
