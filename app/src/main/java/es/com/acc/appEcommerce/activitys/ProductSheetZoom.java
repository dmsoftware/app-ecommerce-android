package es.com.acc.appEcommerce.activitys;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.Producto;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.pagers.CirclePageIndicator;
import es.com.acc.appEcommerce.utils.pagers.ExtendedPageAdapter;
import es.com.acc.appEcommerce.utils.pagers.ExtendedViewPager;

/**
 * Created by Imanol on 12/05/2015.
 */
public class ProductSheetZoom extends BaseActivity{

    private ExtendedPageAdapter pageAdapter;
    private ExtendedViewPager pager;
    private Button boton;

    public String SERVER_URL = "http://images.kukimba.com/item/1000x667/";

    public Producto Product;
    public int CellID;
    public String cellImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle b = getIntent().getExtras();
        CellID = b.getInt("cellID");
        cellImage = b.getString("cellImage");

        setContentView(R.layout.activity_productzoom);

        loadData();
    }

    @Override
    public void  onBackPressed ()
    {
        goBack();
    }

    @Override
    protected void onDestroy()
    {
        if(GlobalVars.myDB != null){
            GlobalVars.myDB.close();
        }

        pageAdapter.freeBitmaps();

        //finish();
        super.onDestroy();
    }
   /* @Override
    public void onStart() {
        super.onStart();
        // The rest of your onStart() code.
        easyTracker = EasyTracker.getInstance(this);
        // Set the custom metric to be incremented by 5 using its index.
        String lag = "es";
        if(GlobalVars.idioma == 1)lag = "eu";
        String lagUser = "";
        if(GlobalVars.UserData!=null){
            lagUser = String.valueOf(GlobalVars.UserData.ID);
        }
        easyTracker.send(MapBuilder
                        .createAppView()
                        .set(Fields.SCREEN_NAME, "VistaProductoDetalle")
                        .set(Fields.customDimension(1), Locale.getDefault().getLanguage())
                        .set(Fields.customDimension(2), lagUser)
                        .set(Fields.customDimension(3), "(not set)")
                        .set(Fields.customDimension(4), "(not set)")
                        .set(Fields.customDimension(5), "(not set)")
                        .set(Fields.customDimension(6), "(not set)")
                        .set(Fields.customDimension(7), "(not set)")
                        .set(Fields.customDimension(8), lag)
                        .build()
        );
    }*/

    public void makeClick(int id)
    {
        goBack();
    }

    public void loadData()
    {
        //restoreActionBar();

        List<String> urls = new ArrayList<String>();
        String url = SERVER_URL + cellImage;
        urls.add(url);
        url = url.replace("_1","_2");
        urls.add(url);
        url = url.replace("_2","_3");
        urls.add(url);
        url = url.replace("_3","_4");
        urls.add(url);

        pager = (ExtendedViewPager) findViewById(R.id.pager);
        try {
            //pageAdapter = new ExtendedPageAdapter(ProductSheetZoom.this, urls, GlobalVars.screenWidth, GlobalVars.screenHeight,pager);
            pageAdapter = new ExtendedPageAdapter(ProductSheetZoom.this, urls, GlobalVars.screenWidth, GlobalVars.screenHeight,pager);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        pager.setAdapter(pageAdapter);
        //pager.setId(contador);
        //contador++;
        /*
        pager.getLayoutParams().width = GlobalVars.screenWidth;
        pager.getLayoutParams().height = GlobalVars.screenHeight-150;
        */
        pager.getLayoutParams().width = GlobalVars.screenWidth;
        pager.getLayoutParams().height = GlobalVars.screenHeight;

        boton = (Button) findViewById(R.id.botonZoom);
        boton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                makeClick(pager.getCurrentItem());
            }
        });

        CirclePageIndicator mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        //mIndicator.setPadding(0, GlobalVars.screenHeight-80,0,0);
        //mIndicator.setPadding(0, GlobalVars.screenHeight-80,0,0);
        mIndicator.setViewPager(pager);
        //mIndicator.setRadius(7.0f);
        mIndicator.setRadius(GlobalVars.screenWidth/80);
    }
}
