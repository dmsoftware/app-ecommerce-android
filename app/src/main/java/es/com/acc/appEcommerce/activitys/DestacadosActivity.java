package es.com.acc.appEcommerce.activitys;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.CategoriaRow;
import es.com.acc.appEcommerce.entities.Destacados;
import es.com.acc.appEcommerce.entities.ItemCategories;
import es.com.acc.appEcommerce.entities.Producto;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Get_Destacados;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_ItemCategories;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Remarketing;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

/**
 * Created by Imanol on 14/07/2015.
 */
public class DestacadosActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{
    private JSONTask_Get_Destacados jsonDestacados;
    private JSONTask_Remarketing jsonRemarketing;
    private JSONTask_ItemCategories jsonItemCats;

    public String SERVER_URL = "http://images.appEcommerce.com/item/640x427/";

    public LinearLayout container;
    public RelativeLayout relContainer;

    public boolean clicked = false;

    @Override
    public void onStart() {
        super.onStart();

        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);
    }

    @Override
    protected void onResume()
    {
        // TODO Auto-generated method stub
        super.onResume();

        if(dataCharged) {
            clicked = false;
            setRefreshing(false);
        }

        freeMemory();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        clicked = false;

        setContentView(R.layout.activity_destacados);

        container = (LinearLayout) findViewById(R.id.destacadosContainer);
        relContainer = (RelativeLayout) findViewById(R.id.relacionados);

        TextView txt = (TextView)findViewById(R.id.txtRecomendado);

        GlobalVars.momentSection = 1;

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                CategoriasPrincipales,R.drawable.ic_drawer);

        if (GlobalVars.idioma == 1) {
            mTitle = getResources().getString(R.string.menu_section0_eu);
            txt.setText(getResources().getString(R.string.appEcommerce_recomienda_eu));
        } else {
            mTitle = getResources().getString(R.string.menu_section0);
            txt.setText(getResources().getString(R.string.appEcommerce_recomienda));
        }

        setRefreshing(true);
        dataCharged = false;

        jsonDestacados = new JSONTask_Get_Destacados(this, DestacadosActivity.this);
        jsonDestacados.execute();
    }

    public void xmlDestacadosCharged (List<Destacados> destacados)
    {
        for (final Destacados item : destacados) {
            //JSONTask_Cesta json = new JSONTask_Cesta(this,CestaActivity.this, item.getId());
            //json.execute();
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout menuLayout = (LinearLayout) inflater.inflate(R.layout.cell_destacado, null, false);

            LinearLayout clickImage = (LinearLayout) menuLayout.findViewById(R.id.clickElement);

            clickImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(item.Conditions[0].contains("of"))
                        {
                            GlobalVars.momentSection = GlobalVars.Categorias.size()+1;
                            GlobalVars.momentSub = 0;
                        }
                        else if(item.Conditions[0].contains("ma"))
                        {
                            GlobalVars.momentSection = GlobalVars.Categorias.size();

                            int pos = 0;

                            for (CategoriaRow catId: GlobalVars.Categorias.get(4).secundarios)
                            {
                                if(catId.principal.MerchantID.equals(item.Conditions[0]))
                                {
                                    GlobalVars.momentSub = pos;
                                    break;
                                }
                                pos++;
                            }
                        }

                        Intent intent = new Intent(DestacadosActivity.this, SubCategoriaActivity.class);
                        startActivity(intent);
                    }
                });

            ImageView image = (ImageView) menuLayout.findViewById(R.id.imageDestacado);
            image.getLayoutParams().width = GlobalVars.screenWidth - 18;
            image.getLayoutParams().height = (int) ((GlobalVars.screenWidth - 18)*0.477);
            image.requestLayout();

            LoadImage imgJson = new LoadImage(image,DestacadosActivity.this);
            imgJson.execute(item.Image);

            //TextView text = (TextView) menuLayout.findViewById(R.id.txtDestacado);
            //text.setText(item.Name);

            container.addView(menuLayout);
        }

        jsonRemarketing = new JSONTask_Remarketing(this, DestacadosActivity.this, String.valueOf(GlobalVars.UserData.ID));
        jsonRemarketing.execute();
    }

    public void RemarketinCharged(List<Producto> Products)
    {
        for (int number = 0; number < Products.size() ; number++)
        {
            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            RelativeLayout menuLayout = (RelativeLayout) inflater.inflate(R.layout.product_cell, null, false);

            ImageView img = (ImageView) menuLayout.findViewById(R.id.imagen);

            TextView descripcion = (TextView) menuLayout.findViewById(R.id.brand);
            descripcion.setText(Products.get(number).Brand);
            //descripcion.setText(String.valueOf(number));

            TextView name = (TextView) menuLayout.findViewById(R.id.name);
            name.setText(Products.get(number).Name);

            TextView precio = (TextView) menuLayout.findViewById(R.id.price);
            precio.setText(String.format("%.2f", Products.get(number).Price)+" €");

            TextView precioOriginal = (TextView) menuLayout.findViewById(R.id.originalprice);
            if(Products.get(number).Price != Products.get(number).OriginalPrice)
            {
                //precioOriginal.setText(Float.toString(Product.related[number].OriginalPrice) + " €");
                precioOriginal.setText( String.format( "%.2f", Products.get(number).OriginalPrice)+" €");
                precioOriginal.setPaintFlags(precioOriginal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                TextView rebajaText = (TextView) menuLayout.findViewById(R.id.rebajatext);
                int lag = (int) (100 - Math.floor(Products.get(number).Price * 100 / Products.get(number).OriginalPrice));
                rebajaText.setText(lag+" %");
            }
            else
            {
                precioOriginal.setVisibility(View.INVISIBLE);
                RelativeLayout rebajaPanel = (RelativeLayout) menuLayout.findViewById(R.id.rebajapanel);
                rebajaPanel.setVisibility(View.INVISIBLE);
            }

            LinearLayout botonC = (LinearLayout)menuLayout.findViewById(R.id.botonCelda);
            botonC.setId(Products.get(number).ID);
            //menuLayout.setId(Products.get(number).ID);

            RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams((GlobalVars.screenWidth/2 -2),(int)(GlobalVars.screenWidth/1.8-1));
            int x = 2 + number%2 * (GlobalVars.screenWidth/2 -1);
            int y = 1 + number/2 * (int)(GlobalVars.screenWidth/1.8);
            params2.setMargins(x, y, 1, 0);
            relContainer.addView(menuLayout,params2);

            String pathURL = SERVER_URL;
            pathURL += Products.get(number).Image[0];

            LoadImage imgJson = new LoadImage(img,DestacadosActivity.this);
            imgJson.execute(pathURL);
        }

        setRefreshing(false);
        dataCharged = true;
    }

    public void onClick(View view)
    {
        if(clicked==false) {
            setRefreshing(true);
            dataCharged = false;
            clicked=true;
            GlobalVars.ProductID = view.getId();
            jsonItemCats = new JSONTask_ItemCategories(this, DestacadosActivity.this, GlobalVars.ProductID);
            jsonItemCats.execute();
        }
    }

    @Override
    public void xmlItemCatCharged(List<ItemCategories> cats)
    {
        for( ItemCategories cat: cats) {
            if (cat.Id.contains("co")) {
                int pos = 2;
                for (CategoriaRow catId : GlobalVars.Categorias) {
                    if (catId.principal.MerchantID.equals(cat.Id)) {
                        GlobalVars.momentSection = pos;
                        break;
                    }
                    pos++;
                }
            }
        }
        for( ItemCategories cat: cats)
        {
            if (cat.Id.contains("co")) {

            }
            else
            {
                int pos = 0;
                for (CategoriaRow catId: GlobalVars.Categorias.get(GlobalVars.momentSection).secundarios)
                {
                    if(catId.principal.MerchantID.equals(cat.Id))
                    {
                        GlobalVars.momentSub = pos;
                        break;
                    }
                    pos++;
                }
            }
        }

        GlobalVars.returnToDestacado = true;

        Intent intent = new Intent(DestacadosActivity.this, ProductSheetActivity.class);
        startActivity(intent);

        setRefreshing(false);
        dataCharged = true;
    }
}
