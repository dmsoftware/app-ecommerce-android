package es.com.acc.appEcommerce.activitys;

import android.app.LocalActivityManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.UserLogin;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Login;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

/**
 * Created by Imanol on 25/06/2015.
 */
public class CompraIdentificacionActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{
    TabHost tabHost;
    LocalActivityManager mLocalActivityManager;

    private JSONTask_Login loginjson;

    @Override
    public void onStart() {
        super.onStart();

        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(GlobalVars.UserData.ID > -1){
            Intent nextScreen;
            nextScreen = new Intent(getApplicationContext(), CompraPagoActivity.class);
            startActivity(nextScreen);
            finish();
        }
        else {
            setContentView(R.layout.activity_compra_identificacion);

            mNavigationDrawerFragment = (NavigationDrawerFragment)
                    getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

            // Set up the drawer.
            mNavigationDrawerFragment.setUp(
                    R.id.navigation_drawer,
                    (DrawerLayout) findViewById(R.id.drawer_layout),
                    CategoriasPrincipales,R.drawable.ic_action_previous_item);

            TextView identificacion= (TextView) findViewById(R.id.leyendaIdentificacion);
            TextView pago= (TextView) findViewById(R.id.leyendaPagoEnvio);
            TextView confirma= (TextView) findViewById(R.id.leyendaConfirmacion);

            if (GlobalVars.idioma == 1) {
                mTitle = getResources().getString(R.string.compra_title_eu);
                identificacion.setText(getResources().getString(R.string.compra_identificacion_eu));
                pago.setText(getResources().getString(R.string.compra_pago_eu));
                confirma.setText(getResources().getString(R.string.compra_confirmacion_eu));
            }
            else{
                mTitle = getResources().getString(R.string.compra_title);
                identificacion.setText(getResources().getString(R.string.compra_identificacion));
                pago.setText(getResources().getString(R.string.compra_pago));
                confirma.setText(getResources().getString(R.string.compra_confirmacion));
            }

            tabHost = (TabHost) findViewById(android.R.id.tabhost);
            initTabs(savedInstanceState);
        }

        dataCharged = true;
        setRefreshing(false);
    }

    @Override
    public void  onBackPressed ()
    {
        if(dataCharged)NavUtils.navigateUpFromSameTask(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            if(dataCharged==true)NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void  InicioClick (String id, String psw)
    {
        setRefreshing(true);
        GlobalVars.password = psw;
        loginjson = new JSONTask_Login(this,CompraIdentificacionActivity.this,id,psw);
        loginjson.execute();
    }

    @Override
    public void xmlLoginCharged(UserLogin log)
    {
        if(log.ID == -1)
        {
            if(GlobalVars.idioma == 1)
                Toast.makeText(CompraIdentificacionActivity.this, getResources().getString(R.string.cuenta_login_ko_eu), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(CompraIdentificacionActivity.this, getResources().getString(R.string.cuenta_login_ko), Toast.LENGTH_SHORT).show();
        }
        else {
            GlobalVars.UserData = log;

            SharedPreferences mPrefs = getSharedPreferences("mypref", 0);

            SharedPreferences.Editor mEditor = mPrefs.edit();
            mEditor.putString("birthdate", log.Birthdate).commit();
            mEditor.putString("email", log.Email).commit();
            mEditor.putInt("genre", log.Genre).commit();
            mEditor.putInt("id", log.ID).commit();
            mEditor.putString("name", log.Name).commit();
            mEditor.putString("surname1", log.Surname1).commit();
            mEditor.putString("surname2", log.Surname2).commit();
            mEditor.putString("telephone", log.Telephone).commit();
            mEditor.putBoolean("addnewsletter", log.addToNewsletter).commit();
            mEditor.putString("password", GlobalVars.password).commit();

            tabHost.setVisibility(View.INVISIBLE);

            Intent nextScreen;
            nextScreen = new Intent(getApplicationContext(), CompraPagoActivity.class);
            startActivity(nextScreen);
            finish();
        }
        setRefreshing(false);
    }
    /*@Override
    protected void onResume() {
        mLocalActivityManager.dispatchResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mLocalActivityManager.dispatchPause(isFinishing());
        super.onPause();
    }*/

    private void initTabs(Bundle savedInstanceState) {
        Resources res = getResources();                     // Resource object to get Drawables
        mLocalActivityManager = new LocalActivityManager(this, false);
        mLocalActivityManager.dispatchCreate(savedInstanceState);
        tabHost.setup(mLocalActivityManager);

        TabHost.TabSpec spec;                               // Resusable TabSpec for each tab
        Intent intent;                                      // Reusable Intent for each tab

        intent = new Intent().setClass(this, InicioSesionActivity.class);

        String lag;
        if(GlobalVars.idioma == 1)
            lag = getResources().getString(R.string.cuenta_inicio_eu);
        else
            lag = getResources().getString(R.string.cuenta_inicio);

        spec = tabHost.newTabSpec("inicio").setIndicator(lag,
                res.getDrawable(R.drawable.cuenta))
                .setContent(intent);
        tabHost.addTab(spec);

        if(GlobalVars.idioma == 1)
            lag = getResources().getString(R.string.cuenta_nuevo_eu);
        else
            lag = getResources().getString(R.string.cuenta_nuevo);

        intent = new Intent().setClass(this, NuevoClienteActivity.class);
        spec = tabHost.newTabSpec("nuevo").setIndicator(lag,
                res.getDrawable(R.drawable.categorias))
                .setContent(intent);
        tabHost.addTab(spec);

        TabWidget widget = tabHost.getTabWidget();

        for(int i = 0; i < widget.getChildCount(); i++) {
            View v = widget.getChildAt(i);

            // Look for the title view to ensure this is an indicator and not a divider.
            TextView tv = (TextView)v.findViewById(android.R.id.title);
            if(tv == null) {
                continue;
            }
            v.setBackgroundResource(R.drawable.apptheme_tab_indicator_holo);
        }

        tabHost.setCurrentTab(0);

        setRefreshing(false);
    }
}
