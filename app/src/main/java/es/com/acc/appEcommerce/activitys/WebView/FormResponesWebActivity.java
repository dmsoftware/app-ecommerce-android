package es.com.acc.appEcommerce.activitys.WebView;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.DrawerLayout;
import android.webkit.WebSettings;
import android.webkit.WebView;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.activitys.BaseActivity;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

public class FormResponesWebActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dataCharged = true;

        setContentView(R.layout.activity_webview);

        mTitle = MainSections[GlobalVars.momentSection];

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                CategoriasPrincipales,R.drawable.ic_drawer);
        WebView myWebView = (WebView) this.findViewById(R.id.webView);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {

            // Get endResult
            String htmlString = extras.getString("htmlString", "");
            //myWebView.loadData(htmlString, "text/html", "utf-8");

            String htmlData = "<link rel=\"stylesheet\" type=\"text/css\" href=\"skin49.css\" />" + htmlString;
// lets assume we have /assets/style.css file
            myWebView.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html", "UTF-8", null);

        }
    }

    @Override
    public void  onBackPressed ()
    {
        NavUtils.navigateUpFromSameTask(this);
    }
}