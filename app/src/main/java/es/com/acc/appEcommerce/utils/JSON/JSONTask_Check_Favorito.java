package es.com.acc.appEcommerce.utils.JSON;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import es.com.acc.appEcommerce.activitys.ProductSheetActivity;
import es.com.acc.appEcommerce.entities.ProductoEsFavorito;


public class JSONTask_Check_Favorito extends AsyncTask<Void, Void, String>{

    private Context context;
    private ProductSheetActivity subCa;
    private static final String TAG = "PostFetcher";
    private Boolean CatsLoaded = false;
    public static final String SERVER_URL = "-- url del servicio --";
    private ProductoEsFavorito Favorito;

    private String UserID;
    private String ItemID;

    public JSONTask_Check_Favorito(ProductSheetActivity A, Context mContext, String userID, String itemID){
        this.subCa = A;
        this.context = mContext;

        this.UserID = userID;
        this.ItemID = itemID;
        CatsLoaded = false;
        Favorito = null;
    }

    private void handlePostsList(ProductoEsFavorito posts)
    {
        this.Favorito = posts;
    }

    private void failedLoadingPosts() {
        //Log.d("Error", "Post Failed");
    }

    @Override
    protected void onPostExecute(String result)
    {
        if((CatsLoaded==false)&&(Favorito!=null)) {
            CatsLoaded = true;
            subCa.xmlEsFavoritoCharged(Favorito);
        }
    }

     @Override
     protected String doInBackground(Void... params) {
         try {
             //Create an HTTP client
             HttpClient client = new DefaultHttpClient();

             String pathURL = SERVER_URL;
             pathURL += "&userID=";
             pathURL += UserID;
             pathURL += "&itemID=";
             pathURL += ItemID;

             HttpPost post = new HttpPost(pathURL);
             System.out.println(" ::::::::::::::::::::::::::::: POST Check Favoritos : >> "+pathURL);

             //Perform the request and check the status code
             HttpResponse response = client.execute(post);
             StatusLine statusLine = response.getStatusLine();

             if(statusLine.getStatusCode() == 200)
             {
                 HttpEntity entity = response.getEntity();
                 InputStream content = entity.getContent();

                 try {
                     //Read the server response and attempt to parse it as JSON
                     Reader reader = new InputStreamReader(content);

                     GsonBuilder gsonBuilder = new GsonBuilder();
                     gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                     Gson gson = gsonBuilder.create();

                     //System.out.println(" ::::::::::::::::::::::::::::: reader GSON 1 : >> "+gson.toString());

                     ProductoEsFavorito fav = gson.fromJson(reader, ProductoEsFavorito.class);
                     content.close();

                     //System.out.println(" ::::::::::::::::::::::::::::: reader GSON  2: >> "+reader);

                     handlePostsList(fav);
                 } catch (Exception ex) {
                     Log.e(TAG, "Failed to parse JSON due to: " + ex);
                     failedLoadingPosts();
                 }
             } else {
                 Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                 failedLoadingPosts();
             }
         } catch(Exception ex) {
             Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
             failedLoadingPosts();
         }
         return null;
     }

}
