package es.com.acc.appEcommerce.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

import es.com.acc.appEcommerce.utils.pagers.CustomPageAdapter;

/**
 * Created by Imanol on 13/05/2015.
 */
public class ImageLoader extends AsyncTask<String, String, Bitmap> {
    public ImageView outImg;
    public CustomPageAdapter ProdSh;
    public View vie;
    public ViewPager RootPager;

    public ImageLoader(CustomPageAdapter PS, View vi, ViewPager pag){
        super();
        ProdSh = PS;
        vie = vi;
        RootPager = pag;
    }


    @Override
    protected Bitmap doInBackground(String... args) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap image)
    {
        if(image != null)
        {
            int pageIndex = ProdSh.addView (vie,image);
        }
    }

}
