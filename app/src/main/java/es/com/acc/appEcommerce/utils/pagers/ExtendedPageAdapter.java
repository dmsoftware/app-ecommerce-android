package es.com.acc.appEcommerce.utils.pagers;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.utils.ImageLoader;

/**
 * Created by Imanol on 15/05/2015.
 */
public class ExtendedPageAdapter extends CustomPageAdapter
{
    public ArrayList<Bitmap> Bitmaps = new ArrayList<Bitmap>();

    public ExtendedPageAdapter(Context context, List<String> urls, int anchura, int altura, ViewPager pag) {
        super(context, urls, anchura, altura, pag);
    }

    @Override
    public void initLoaders()
    {
        for(final String url:urls)
        {
            mLayoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View page = mLayoutInflater.inflate(R.layout.product_extended_image_cell, null);

            ProgressBar proBar = (ProgressBar)page.findViewById(R.id.imageLoadingPanel);
            proBar.setVisibility(View.VISIBLE);

            ImageLoader imgLod = new ImageLoader(ExtendedPageAdapter.this,page,RootPager);
            imgLod.execute(url);
        }
    }

    @Override
    public int addView (View v, Bitmap img)
    {
        return addView (v, img, views.size());
    }

    @Override
    public int addView (View v, Bitmap img, int position)
    {
        ProgressBar proBar = (ProgressBar)v.findViewById(R.id.imageLoadingPanel);
        proBar.setVisibility(View.GONE);

        Bitmaps.add(position,img);
        views.add (position, v);
        notifyDataSetChanged();
        return position;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, final int position)
    {
        View v = views.get (position);
        v.setId(position);

        LinearLayout v2 = (LinearLayout)v.findViewById(R.id.prodImg);
        TouchImageView img = new TouchImageView(collection.getContext());
        img.setImageBitmap(Bitmaps.get(position));

        v2.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        collection.addView (v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public void freeBitmaps()
    {
        for( Bitmap bit:Bitmaps)
        {
            bit.recycle();
            bit = null;
        }
    }
}