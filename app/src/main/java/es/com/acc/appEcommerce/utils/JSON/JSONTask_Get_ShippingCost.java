package es.com.acc.appEcommerce.utils.JSON;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import es.com.acc.appEcommerce.activitys.CompraPagoActivity;
import es.com.acc.appEcommerce.entities.ShippingCost;


public class JSONTask_Get_ShippingCost extends AsyncTask<Void, Void, String>
{
    private CompraPagoActivity PrSh;
    private static final String TAG = "PostFetcher";
    private Boolean UserLoaded = false;
    public static final String SERVER_URL = "-- url del servicio --";
    private ShippingCost cost;

    private String cestaID;
    private String methPago;
    private String methEnvio;
    private String provinceID;
    private String coste;

    public JSONTask_Get_ShippingCost(CompraPagoActivity A, Context mContext, String cesta, String pago, String envio, String provincia, String cost)
    {
        this.PrSh = A;
        UserLoaded = false;
        cestaID = cesta;
        methPago=pago;
        methEnvio=envio;
        provinceID=provincia;
        coste=cost;
    }

    private void handlePostsList(ShippingCost post)
    {
        this.cost = post;
    }

    //private void failedLoadingPosts()
    //{
    //    Log.d("Error", "Post Failed");
    //}

    @Override
    protected void onPostExecute(String result)
    {
        if(UserLoaded==false)
        {
            UserLoaded = true;
            PrSh.xmlCosteEnvioCharged(this.cost);
        }
    }

     @Override
     protected String doInBackground(Void... params)
     {
         try {
             //Create an HTTP client
             HttpClient client = new DefaultHttpClient();

             String pathURL = SERVER_URL;
             pathURL += "&ShoppingCartID=";
             pathURL +=  cestaID;
             pathURL += "&PaymentID=";
             pathURL += methPago;
             pathURL += "&ShippingID=";
             pathURL +=  methEnvio;
             pathURL += "&ProvinceID=";
             pathURL +=  provinceID;
             pathURL += "&OrderTotalAmount=";
             pathURL +=  coste;

             System.out.println(" ::::::::::::::::::::::::::::: Get coste envio: >> "+pathURL);

             HttpPost post = new HttpPost(pathURL);

             //Perform the request and check the status code
             HttpResponse response = client.execute(post);
             StatusLine statusLine = response.getStatusLine();
             if(statusLine.getStatusCode() == 200) {
                 HttpEntity entity = response.getEntity();
                 InputStream content = entity.getContent();

                 try {
                     //Read the server response and attempt to parse it as JSON
                     Reader reader = new InputStreamReader(content);

                     GsonBuilder gsonBuilder = new GsonBuilder();
                     gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                     Gson gson = gsonBuilder.create();

                     ShippingCost co = gson.fromJson(reader, ShippingCost.class);
                     content.close();

                     handlePostsList(co);
                 } catch (Exception ex) {
                     //Log.e(TAG, "Failed to parse JSON due to: " + ex);
                     //failedLoadingPosts();
                 }
             } else {
                 //Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                 //failedLoadingPosts();
             }
         } catch(Exception ex) {
             //Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
             //failedLoadingPosts();
         }
         return null;
     }
}
