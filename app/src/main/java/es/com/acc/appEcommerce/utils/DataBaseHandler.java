package es.com.acc.appEcommerce.utils;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import es.com.acc.appEcommerce.activitys.BaseActivity;
import es.com.acc.appEcommerce.entities.CestaProducto;
import es.com.acc.appEcommerce.entities.ProductosFavoritos;

/**
 * Created by Imanol on 22/05/2015.
 */
public class DataBaseHandler extends SQLiteOpenHelper {

    public DataBaseHandler(BaseActivity A, String db_name, int db_version)
    {
        super(A, db_name, null, db_version);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        DeleteDB(db);

        this.createTable(db, "Eco_Cesta", new HashMap<String, String>() {
        {
            put("id", "INTEGER PRIMARY KEY AUTOINCREMENT");
            put("codigointerno", "INT(5)");
            put("talla", "INT(5)");
            put("cantidad", "INT(5)");
        }});

        this.createTable(db, "Eco_Favoritos", new HashMap<String, String>() {
            {
                put("id", "INTEGER PRIMARY KEY AUTOINCREMENT");
                put("codigointerno", "INT(5)");
            }});
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        DeleteDB(db);
        this.onCreate(db);
    }

    public void DeleteDB(SQLiteDatabase db)
    {
        db.execSQL("DROP TABLE IF EXISTS Eco_Cesta");
    }

    private void createTable(SQLiteDatabase sqLiteDatabase, String tabla, HashMap<String, String> campos){
        String sentenceSQL = " (";
        String sentenceSQLValues = "";
        Iterator<?> it = campos.entrySet().iterator();
        while (it.hasNext()) {
            @SuppressWarnings("rawtypes")
            Map.Entry e = (Map.Entry)it.next();
            sentenceSQLValues += e.getKey() + " " + e.getValue() + ", ";
        }
        sentenceSQL += sentenceSQLValues.substring(0, sentenceSQLValues.length()-2);
        sentenceSQL += ")";
        try {
            sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS "
                    + tabla
                    + sentenceSQL);

        }catch(Exception e) {
            Log.e("Error", "Error", e);
        }
    }

    private int getLastId(String tabla)
    {
        int lastId = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT ROWID FROM " + tabla + " ORDER BY ROWID DESC LIMIT 1";
        Cursor c = db.rawQuery(query, null);
        if (c != null && c.moveToFirst()) {
            lastId = c.getInt(0);
        }
        return lastId;
    }

    /**********Productos Cesta*****************/

    public int addProductoCesta(CestaProducto cesta)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("codigointerno",cesta.getId());
        values.put("talla",cesta.getTalla());
        values.put("cantidad",cesta.getCantidad());

        db.insert("Eco_Cesta", null, values);
        db.close();

        return getLastId("Eco_Cesta");
    }

    public int updateProductoCesta(CestaProducto cesta)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("codigointerno",cesta.getId());
        values.put("talla",cesta.getTalla());
        values.put("cantidad",cesta.getCantidad());

        return db.update("Eco_Cesta", values, "codigointerno = ?",
                new String[] { String.valueOf(cesta.getId()) });
    }

    public CestaProducto getProductoCesta(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("Eco_Cesta", new String[] { "id", "codigointerno", "talla", "cantidad"}, "codigointerno = ?",
                new String[] { String.valueOf(id) }, null, null, null, null);

        CestaProducto cesta = null;
        if ((cursor != null) && (cursor.moveToFirst())) {
            cesta = new CestaProducto(Integer.parseInt(cursor.getString(1)), cursor.getString(2), Integer.parseInt(cursor.getString(3)));
        }

        return cesta;
    }

    public void deleteProductoCesta(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("Eco_Cesta", "codigointerno = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }

    public int countProductosCesta()
    {
        List<CestaProducto> cestaL = new ArrayList<CestaProducto>();
        String selectQuery = "SELECT C.id, C.codigointerno, C.talla, C.cantidad FROM Eco_Cesta AS C ORDER BY C.id ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        return cursor.getCount();
    }

    public List<CestaProducto> getAllProductoCesta()
    {
        List<CestaProducto> cestaL = new ArrayList<CestaProducto>();
        String selectQuery = "SELECT C.id, C.codigointerno, C.talla, C.cantidad FROM Eco_Cesta AS C ORDER BY C.id ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                CestaProducto cestaP = new CestaProducto();
                cestaP.setId(Integer.parseInt(cursor.getString(1)));
                cestaP.setTalla(cursor.getString(2));
                cestaP.setCantidad(Integer.parseInt(cursor.getString(3)));
                cestaL.add(cestaP);
            } while (cursor.moveToNext());
        }
        return cestaL;
    }

    /**********Favoritos*****************/

    public int addFavorito(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        //values.put("id", cabeImg.getId());
        values.put("codigointerno", id);

        db.insert("Eco_Favoritos", null, values);
        db.close();

        return getLastId("Eco_Favoritos");
    }

    public boolean getFichaFavorita(int id) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("Eco_Favoritos", new String[] { "id",
                        "codigointerno", }, "codigointerno = ?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        boolean lag = false;

        if(cursor.getCount()>0)
        {
            lag = true;
        }

        return lag;
    }

    public void deleteFichaFavorita(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("Eco_Favoritos", "codigointerno = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }

    public List<ProductosFavoritos> getAllFavoritas(String lang)
    {
        List<ProductosFavoritos> favList = new ArrayList<ProductosFavoritos>();
        String selectQuery = "SELECT C.id, C.codigointerno FROM Eco_Favoritos AS C ORDER BY C.id ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                ProductosFavoritos favs = new ProductosFavoritos();
                favs.setId(Integer.parseInt(cursor.getString(1)));
                favList.add(favs);
            } while (cursor.moveToNext());
        }
        return favList;
    }
}
