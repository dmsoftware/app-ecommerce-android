package es.com.acc.appEcommerce.entities;

/**
 * Created by Imanol on 30/04/2015.
 */
public class Province
{
    public int Country;
    public int ID;
    public int LocalCode;
    public String Name;

    public Province() {

    }
}
