package es.com.acc.appEcommerce.activitys;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.utils.GlobalVars;

/**
 * Created by Imanol on 20/05/2015.
 */
public class InicioSesionActivity extends Activity {

    public TextView email;
    public TextView password;
    public Button bot;
    public Button recordar;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cuenta_inicio);
        email = (TextView)findViewById(R.id.email);
        password = (TextView)findViewById(R.id.password);
        bot = (Button)findViewById(R.id.boton);
        recordar = (Button)findViewById(R.id.recordarBoton);

        if(GlobalVars.idioma == 1) {
            email.setHint(getResources().getString(R.string.cuenta_email_eu));
            password.setHint(getResources().getString(R.string.cuenta_password_eu));
            bot.setText(getResources().getString(R.string.cuenta_inicio_eu));
            recordar.setText(getResources().getString(R.string.cuenta_recordar));
        }
        else {
            email.setHint(getResources().getString(R.string.cuenta_email));
            password.setHint(getResources().getString(R.string.cuenta_password));
            bot.setText(getResources().getString(R.string.cuenta_inicio));
            recordar.setText(getResources().getString(R.string.cuenta_recordar_eu));
        }
    }

    public void InicioClicked(View view)
    {
        ((BaseActivity)getParent()).InicioClick(email.getText().toString(), password.getText().toString());
    }

    public void RecordarClicked(View view)
    {
        ((BaseActivity)getParent()).RecordarClicked(email.getText().toString());
    }

    public void  onBackPressed () {
        ((CuentaActivity)getParent()).onBackPressed();
    }

}
