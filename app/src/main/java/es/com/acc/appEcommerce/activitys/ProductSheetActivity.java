package es.com.acc.appEcommerce.activitys;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.ShareActionProvider;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.CestaCompra;
import es.com.acc.appEcommerce.entities.ProductStock;
import es.com.acc.appEcommerce.entities.Producto;
import es.com.acc.appEcommerce.entities.ProductoEsFavorito;
import es.com.acc.appEcommerce.entities.Tallas;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Add_Favorito;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Check_Favorito;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Delete_Favorito;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Modificar_Cesta;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_ProductSheet;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Stock;
import es.com.acc.appEcommerce.utils.pagers.CirclePageIndicator;
import es.com.acc.appEcommerce.utils.pagers.CustomPageAdapter;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;
import es.com.acc.appEcommerce.views.RadioDialog;

/**
 * Created by Imanol on 12/05/2015.
 */
public class ProductSheetActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, RadioDialog.DialogListener, ShareActionProvider.OnShareTargetSelectedListener {

    private CustomPageAdapter pageAdapter;
    private ViewPager pager;
    private Button botonZoom;
    private Button botonFav;
    private Button botonPlayStop;
    private Boolean pagerPlaying;

    private JSONTask_ProductSheet json;
    private JSONTask_Stock jsonStock;
    private JSONTask_Modificar_Cesta jsonCesta;
    private JSONTask_Check_Favorito jsonIsFav;
    private JSONTask_Add_Favorito jsonAddFav;
    private JSONTask_Delete_Favorito jsonDeleteFav;

    public RelativeLayout container;
    public ProgressBar lodPanel;

    public String SERVER_URL = "http://images.kukimba.com/item/640x427/";
    public String BaseImageName = "";

    public ScrollView Scroller;
    public boolean scrollStarted = false;

    public Producto Product;
    //public int ProductID;

    public TextView LeyendaCategoria;
    public TextView LeyendaSubCategoria;
    public TextView LeyendaMarca;
    public TextView LeyendaProducto;

    public TextView BrandT;
    public TextView NameT;
    public TextView PrizeT;
    public ImageView GoreT;
    public TextView DescriT;
    public Spinner SizeS;

    public Spinner cantS;

    public LinearLayout PanelDispo;
    public TextView DispoTxt;
    public TextView SizeText;
    public TextView CantiT;
    public ImageView StckImg;
    public ImageView DispoImg;

    public RelativeLayout relatedItems;

    public int ProductSizePos;
    public int ProductCant;
    public ProductStock productStock;
    public List<String> Medidas;
    public List<String> Cantidades;

    public boolean LoadingCant;

    public static DialogInterface dialog = null;

    private android.support.v7.widget.ShareActionProvider mShareActionProvider = null;
    private Intent mShareIntent;

    public Uri shareImage = null;

    public MenuItem share;

    private CountDownTimer timer = null;

    private Timer swipeTimer;

    private int currentPage;

    private boolean esFavorito = false;

    DisplayMetrics displayMetrics;

    @Override
    public void onStart() {
        super.onStart();

        if(mNavigationDrawerFragment.isDrawerOpen())
           mNavigationDrawerFragment.selectItem(0);

        pagerPlaying = true;

        displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) ProductSheetActivity.this.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        swipeTimer.cancel();
        /*if(shareImage!=null)
        {
            SharedPreferences mPrefs = getSharedPreferences("mypref", 0);
            SharedPreferences.Editor mEditor = mPrefs.edit();
            mEditor.putString("shareImage", "").commit();

            getContentResolver().delete(shareImage, null, null);
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }*/
    }

    public void sendAnalytics(String valor, String coleccionID, String coleciconName, String categoriaID, String categoriaName, String marcaID, String marcaName)
    {
        String lag = "es";
        if(GlobalVars.idioma == 1)lag = "eu";
        String lagUser = "";
        if(GlobalVars.UserData!=null){
            lagUser = String.valueOf(GlobalVars.UserData.ID);
        }

        // Set screen name.
        tracker.setScreenName("VistaProducto->"+valor);

        // Send a screen view.
        tracker.send(new HitBuilders.ScreenViewBuilder()
                .setCustomDimension(1, lag)
                .setCustomDimension(2, lagUser)
                .setCustomDimension(3, "(not set)")
                .setCustomDimension(4, "(not set)")
                .setCustomDimension(5, categoriaID + "|" + categoriaName)
                .setCustomDimension(6, marcaID + "|" + marcaName)
                .setCustomDimension(7, coleccionID + "|" + coleciconName)
                .setCustomDimension(8, Locale.getDefault().getLanguage())
                .build());
    }

    @Override
    public void  onBackPressed ()
    {
        if(dataCharged==true)productGoBack();
    }

    public void productGoBack()
    {
        if(GlobalVars.returnToDestacado == true)
        {
            GlobalVars.returnToDestacado = false;
            Intent intent = new Intent(ProductSheetActivity.this, DestacadosActivity.class);
            startActivity(intent);
        }
        else if(GlobalVars.returnToFavorito == true)
        {
            GlobalVars.returnToFavorito = false;
            Intent intent = new Intent(ProductSheetActivity.this, FavoritoActivity.class);
            startActivity(intent);
        }
        else goBack();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Bundle b = getIntent().getExtras();
        //ProductID = b.getInt("keyID");

        setContentView(R.layout.activity_productsheet);
        lodPanel = (ProgressBar) findViewById(R.id.loadingPanel);

        container =  (RelativeLayout) findViewById(R.id.cellContainer);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                CategoriasPrincipales,R.drawable.ic_action_previous_item);

        LeyendaCategoria = (TextView)findViewById(R.id.leyendaCategoria);
        LeyendaSubCategoria = (TextView)findViewById(R.id.leyendaSubCategoria);
        LeyendaMarca = (TextView)findViewById(R.id.leyendaMarca);
        LeyendaProducto = (TextView)findViewById(R.id.leyendaProducto);

        BrandT = (TextView)findViewById(R.id.BrandText);
        NameT = (TextView)findViewById(R.id.nameText);
        PrizeT = (TextView)findViewById(R.id.PrizeText);

        GoreT = (ImageView)findViewById(R.id.GoreTex);

        GoreT.setVisibility(View.INVISIBLE);

        DescriT = (TextView)findViewById(R.id.DescriText);
        SizeS  = (Spinner)findViewById(R.id.SizeSpinner);

        cantS = (Spinner)findViewById(R.id.CantSpinner);
        cantS.setVisibility(View.INVISIBLE);

        SizeText = (TextView)findViewById(R.id.SizeText);
        if(GlobalVars.idioma == 1)
            SizeText.setText(getResources().getString(R.string.producto_talla_eu));
        else
            SizeText.setText(getResources().getString(R.string.producto_talla));

        TextView PrizeLabel = (TextView)findViewById(R.id.PrizeLabel);
        if(GlobalVars.idioma == 1)
            PrizeLabel.setText(getResources().getString(R.string.producto_precio_eu));
        else
            PrizeLabel.setText(getResources().getString(R.string.producto_precio));

        CantiT  = (TextView)findViewById(R.id.CantidadText);
        if(GlobalVars.idioma == 1)
            CantiT.setText(getResources().getString(R.string.producto_cantidad_eu));
        else
            CantiT.setText(getResources().getString(R.string.producto_cantidad));
        CantiT.setVisibility(View.INVISIBLE);

        DispoTxt = (TextView)findViewById(R.id.DispoText);
        StckImg = (ImageView)findViewById(R.id.StockImage);
        DispoImg = (ImageView)findViewById(R.id.DispoImage);
        PanelDispo = (LinearLayout)findViewById(R.id.Disponivilidad);
        PanelDispo.setVisibility(View.INVISIBLE);

        TextView RelatedText = (TextView)findViewById(R.id.relatedText);
        if(GlobalVars.idioma == 1)
            RelatedText.setText(getResources().getString(R.string.producto_relacionados_eu));
        else
            RelatedText.setText(getResources().getString(R.string.producto_relacionados));


        Button cestaBut = (Button)findViewById(R.id.botonCesta);
        if(GlobalVars.idioma == 1)
            cestaBut.setText(getResources().getString(R.string.producto_but_cesta_eu));
        else
            cestaBut.setText(getResources().getString(R.string.producto_but_cesta));

        botonFav = (Button) findViewById(R.id.botonFav);

        if(GlobalVars.UserData.ID>-1) {

            botonFav.setId(GlobalVars.ProductID);

            jsonIsFav = new JSONTask_Check_Favorito(this, ProductSheetActivity.this, String.valueOf(GlobalVars.UserData.ID), String.valueOf(GlobalVars.ProductID));
            jsonIsFav.execute();

            botonFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (esFavorito) {
                        esFavorito = false;
                        botonFav.setBackgroundResource(R.drawable.fav_empty);
                        deleteFav();
                    } else {
                        esFavorito = true;
                        botonFav.setBackgroundResource(R.drawable.fav_full);
                        addFav();
                    }
                }
            });
        }
        else
        {
            botonFav.setVisibility(View.INVISIBLE);
        }
        relatedItems = (RelativeLayout)findViewById(R.id.relacionados);

        lodPanel.setVisibility(View.VISIBLE);
        setRefreshing(true);

        dataCharged = false;

        ProductSizePos = -1;
        ProductCant = -1;

        json = new JSONTask_ProductSheet(this,ProductSheetActivity.this,GlobalVars.ProductID);
        json.execute();
    }


    public void addFav()
    {
        jsonAddFav = new JSONTask_Add_Favorito(this, ProductSheetActivity.this, String.valueOf(GlobalVars.UserData.ID), String.valueOf(GlobalVars.ProductID));
        jsonAddFav.execute();
    }

    public void deleteFav()
    {
        jsonDeleteFav = new JSONTask_Delete_Favorito(this, ProductSheetActivity.this, String.valueOf(GlobalVars.UserData.ID), String.valueOf(GlobalVars.ProductID));
        jsonDeleteFav.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.

            getMenuInflater().inflate(R.menu.main, menu);

            ProgressBar = (MenuItem) menu.findItem(R.id.cargando);
            ProgressBar.setActionView(R.layout.action_bar_loader);
            if(dataCharged)
                ProgressBar.setVisible(false);
            else
                ProgressBar.setVisible(true);

            share = (MenuItem) menu.findItem(R.id.comparte);
            //share.setVisible(false);
            // Fetch and store ShareActionProvider
            mShareActionProvider = (android.support.v7.widget.ShareActionProvider) MenuItemCompat.getActionProvider(share);
            mShareActionProvider.setShareHistoryFileName(ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
            //mShareActionProvider.setShareIntent(getDefaultShareIntent());
            mShareActionProvider.setOnShareTargetSelectedListener(this);

            share.setVisible(false);

            MenuItem item =(MenuItem) menu.findItem(R.id.carrit);
            MenuItemCompat.setActionView(item, R.layout.action_bar_notifitcation_icon);
            View menu_hotlist = (View) MenuItemCompat.getActionView(item);

            cesta_counter_t = (TextView) menu_hotlist.findViewById(R.id.hotlist_hot);
            updateHotCount(GlobalVars.cesta_count);

            new MyMenuItemStuffListener(menu_hotlist) {
                @Override
                public void onClick(View v) {
                    carritoClicked();
                }
            };

            restoreActionBar();
            return true;
        }

        return super.onCreateOptionsMenu(menu);
    }

    public void setShareImage(Bitmap btm)
    {
        if(share!=null) {
            shareImage = getImageUri(ProductSheetActivity.this, btm);
            share.setVisible(true);
            mShareActionProvider.setShareIntent(getDefaultShareIntent());
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage)
    {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        Random generator = new Random();
        String path = "";
        String fname = "Image.jpg";
        File file = new File(myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            inImage.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        /*ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        //String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Kukimba - "+Product.Brand+":"+Product.Name, null);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Kukimba", null);*/
        Uri outputFileUri = Uri.fromFile( file );


        /*SharedPreferences mPrefs = getSharedPreferences("mypref", 0);
        SharedPreferences.Editor mEditor = mPrefs.edit();
        mEditor.putString("shareImage", path).commit();*/

        //return Uri.parse(path);
        return outputFileUri;
    }

    /** Returns a share intent */
    private Intent getDefaultShareIntent(){
        Uri uri = Uri.parse(SERVER_URL + BaseImageName);

        String text;

        //String lag1 = String.valueOf(Product.Price);
        //lag1 = lag1.replace(".",",");
        String lag1 = String.format( "%.2f", Product.Price);

        System.out.println(" ::::::::::::::::::::::::::::: getDefaultShareIntent: >> "+GlobalVars.momentSection+" :: "+GlobalVars.momentSub);

        if(Product.Price < Product.OriginalPrice) {
            //String lag2 = String.valueOf(Product.OriginalPrice);
            //lag2 = lag2.replace(".",",");
            String lag2 = String.format( "%.2f", Product.OriginalPrice);
            if(GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.Name.compareTo("Outlet")==0)
                text = GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.Name + " " + Product.Brand + " modelo " + Product.Name + " antes " +lag2+ " € ahora por tan solo " + lag1 + " €.\n\nEchales un vistazo en " + uri.toString();
            else
                text = GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.get(GlobalVars.momentSub).principal.Name + " " + Product.Brand + " modelo " + Product.Name + " antes " +lag2+ " € ahora por tan solo " + lag1 + " €.\n\nEchales un vistazo en " + uri.toString();

        }
        else {
            if(GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.Name.compareTo("Outlet")==0)
                text = GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.Name + " " + Product.Brand + " modelo " + Product.Name + " por " + lag1 + " €.\n\nEchales un vistazo en " + uri.toString();
            else
                text = GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.get(GlobalVars.momentSub).principal.Name + " " + Product.Brand + " modelo " + Product.Name + " por " + lag1 + " €.\n\nEchales un vistazo en " + uri.toString();
        }
        Intent intent = new Intent(Intent.ACTION_SEND);
        //Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Kukimba\n\n");
        intent.putExtra(Intent.EXTRA_TEXT,text);
        intent.putExtra(Intent.EXTRA_STREAM, shareImage);
        intent.setType("image/jpg");
        //intent.setType("text/plain");
        //intent.setType("*/*");;
        return intent;
    }

    @Override
    public boolean onShareTargetSelected(ShareActionProvider source,
                                         Intent intent) {
        Toast.makeText(this, intent.getComponent().toString(),
                Toast.LENGTH_LONG).show();

        String lag = "es";
        if(GlobalVars.idioma == 1)lag = "eu";

        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("compartir")
                .setAction(lag)
                .setLabel(Product.Brand + "-" + Product.Name)
                .build());

        return(false);
    }

    private void setShareIntent(Intent shareIntent) {

        mShareActionProvider.setShareIntent(shareIntent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.comparte) {
            //Toast.makeText(ProductSheetActivity.this, "Compartir", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (id == R.id.favorito) {
            //Toast.makeText(ProductSheetActivity.this, "Favoritos", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (id == R.id.carrit) {
            carritoClicked();
            return true;
        }
        else if (id == android.R.id.home)
        {
            if(dataCharged==true)productGoBack();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayUseLogoEnabled(true);
        //actionBar.setIcon(R.drawable.icono);
        actionBar.setDisplayShowHomeEnabled(true);
        //actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(mTitle);
    }

    public void onDialogPositiveClick(DialogFragment dialog, int value) {
        // User touched the dialog's positive button
        //String[] meds = new String[Medidas.size()];
        //meds = Medidas.toArray(meds);
        ProductSizePos = value;
        CestaClicked(getWindow().getDecorView().findViewById(android.R.id.content));
    }

    public void CestaClicked(View view)
    {
        try {
            if ((ProductSizePos < 0)||(ProductCant < 0)) {

                if (GlobalVars.idioma == 1)
                    Toast.makeText(this, getResources().getString(R.string.producto_toast_talla_eu), Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(this, getResources().getString(R.string.producto_toast_talla), Toast.LENGTH_LONG).show();

            } else {

                String categoriaLag = "";

                if(GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.Name.compareTo("Outlet")==0)
                    categoriaLag = "Outlet";
                else
                    categoriaLag = GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.get(GlobalVars.momentSub).principal.Name;

                Product product = new Product()
                        .setId(String.valueOf(Product.ID))
                        .setName(Product.Name)
                        .setCategory(categoriaLag)
                        .setBrand(String.valueOf(ProductCant+1))
                        .setVariant(Product.Size[ProductSizePos-1].Name);

                ProductAction productAction = new ProductAction(ProductAction.ACTION_CLICK)
                        .setProductActionList("Producto añadido a cesta");
                HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                        .addProduct(product)
                        .setProductAction(productAction);

                tracker.setScreenName("VistaProducto > "+LeyendaCategoria.getText().toString()+" > "+LeyendaSubCategoria.getText().toString()+" > "+Product.Brand+" > "+Product.ID+"|"+Product.Name);
                tracker.send(builder.build());

                jsonCesta = new JSONTask_Modificar_Cesta(this, ProductSheetActivity.this, GlobalVars.ProductID, GlobalVars.CestaID, GlobalVars.UserData.ID, ProductCant+1, productStock.AvailableUnits, Product.Size[ProductSizePos-1].Id, Product.Size[ProductSizePos-1].Name, productStock.AvailableUnits);
                jsonCesta.execute();
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

    }

    public void xmlEsFavoritoCharged(ProductoEsFavorito esFav) {
        if (esFav.status == 1) {
            esFavorito = true;
            botonFav.setBackgroundResource(R.drawable.fav_full);
        } else {
            esFavorito = false;
            botonFav.setBackgroundResource(R.drawable.fav_empty);
        }
    }
    public void xmlCestaCharged(CestaCompra ces)
    {
        GlobalVars.CestaData = ces;
        if(GlobalVars.CestaData!=null) {
            GlobalVars.CestaID = GlobalVars.CestaData.Id;
            GlobalVars.cesta_count = GlobalVars.CestaData.Items.length;
            updateHotCount(GlobalVars.cesta_count);

            SharedPreferences mPrefs= getSharedPreferences("mypref", 0);

            SharedPreferences.Editor mEditor = mPrefs.edit();
            mEditor.putInt("cestaid", GlobalVars.CestaID).commit();
        }
        //Toast.makeText(this, (ProductCant+1)+" unidades de "+Product.Brand+">"+Product.Name+" añadido a cesta.", Toast.LENGTH_SHORT).show();

        Toast cheatSheet;
        if (GlobalVars.idioma == 1)
        {
            if(ProductCant > 0)
                cheatSheet = Toast.makeText(this, Product.Brand + ">" + Product.Name + "-ren\n\n"+(ProductCant + 1) + " unitate \n\nsaskiratuak", Toast.LENGTH_SHORT);
            else
                cheatSheet = Toast.makeText(this, Product.Brand + ">" + Product.Name + "-ren\n\n"+(ProductCant + 1) + " unitate bat \n\nsaskiratua", Toast.LENGTH_SHORT);
        }
        else
        {
            if(ProductCant > 0)
                cheatSheet = Toast.makeText(this, (ProductCant + 1) + " unidades de\n\n" + Product.Brand + ">" + Product.Name + "\n\nañadidas a la cesta.", Toast.LENGTH_SHORT);
            else
                cheatSheet = Toast.makeText(this, "Una unidad de\n\n" + Product.Brand + ">" + Product.Name + "\n\nañadida a cesta.", Toast.LENGTH_SHORT);
        }
        cheatSheet.setGravity(Gravity.TOP | Gravity.RIGHT, 0, 65);
        cheatSheet.show();
    }

    public void makeClick(int id)
    {
        System.out.println(" ::::::::::::::::::::::::::::: Product Cell Click: >> "+id);
        Intent intent = new Intent(ProductSheetActivity.this, ProductSheetZoom.class);
        Bundle b = new Bundle();
        b.putInt("cellID", id); //Your id
        b.putString("cellImage", BaseImageName);
        b.putString("cellTitle", (String) mTitle);
        intent.putExtras(b); //Put your id to your next Intent
        startActivity(intent);
        //finish();
    }

    public void onClick(View view)
    {
        Intent intent = new Intent(ProductSheetActivity.this, ProductSheetActivity.class);
        /*Bundle b = new Bundle();
        b.putInt("keyID", view.getId()); //Your id
        intent.putExtras(b); //Put your id to your next Intent*/
        GlobalVars.ProductID = view.getId();
        startActivity(intent);
        //finish();
    }

    public void callStock()
    {
        setRefreshing(true);
        jsonStock = new JSONTask_Stock(this,ProductSheetActivity.this,GlobalVars.ProductID,Product.Size[ProductSizePos-1].Id,ProductCant+1);
        jsonStock.execute();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void xmlStockCharged(ProductStock ProdStck)
    {
        setRefreshing(false);
        productStock = ProdStck;
        PanelDispo.setVisibility(View.VISIBLE);
        CantiT.setVisibility(View.VISIBLE);
        cantS.setVisibility(View.VISIBLE);
        Cantidades = new ArrayList<String>();
        for( int i = 1; i <= ProdStck.AvailableUnits; i++)
        {
            Cantidades.add(String.valueOf(i));
        }
        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(getApplicationContext(), R.layout.my_spinner_item, Cantidades);
        spinnerArrayAdapter1.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        cantS.setAdapter(spinnerArrayAdapter1);

        if(ProductCant > -1)
        {
            cantS.setSelection(ProductCant);
        }
        else
        {
            cantS.setSelection(0);
        }

        //spin1.setOnTouchListener(new AdapterView.OnTouchListener() {
        cantS.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if(LoadingCant)
                {
                    LoadingCant = false;
                }
                else  {
                    ProductCant = position;
                    LoadingCant = true;
                    callStock();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        StckImg.setImageResource(R.drawable.stock_camion);
        if(timer!=null)
            timer.cancel();
        DispoTxt.setText(Html.fromHtml(ProdStck.Description));

        final Pattern ptrn = Pattern.compile("<countdown>(.+?)</countdown>");
        final Matcher mtchr = ptrn.matcher(ProdStck.Description);
        final String message = ProdStck.Description;
        while (mtchr.find()) {
            final String result = message.substring(mtchr.start(), mtchr.end()).replace("<countdown>", "").replace("</countdown>", "");

            StckImg.setImageResource(R.drawable.stock_24h);
            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");

            // replace with your start date string
            Date d = null;
            try {
                d = df.parse(result);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Long time = d.getTime();

            timer = new CountDownTimer(time, 1000) {

                public void onTick(long millisUntilFinished) {
                    String hms =String.format("%02d:%02d:%02d",
                            TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) -
                                    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)), // The change is in this line
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));

                    DispoTxt.setText(Html.fromHtml(message.replace(result, hms)));
                }

                public void onFinish() {
                    DispoTxt.setText("Tiempo agotado. Por favor refresque la página.");
                }
            }.start();

            break;
        }

        switch (ProdStck.Id)
        {
            case 0:
                DispoImg.setImageResource(R.drawable.red_light);
                break;
            case 1:
                DispoImg.setImageResource(R.drawable.orange_light);
                break;
            default:
                DispoImg.setImageResource(R.drawable.green_light);
                break;
        }
        DispoImg = (ImageView)findViewById(R.id.DispoImage);

    }

    public void xmlCharged(Producto Prod)
    {
        dataCharged = true;

        Product = Prod;
        //mTitle = Product.Brand+" > "+ Product.Name;
        mTitle = Product.Name;
        restoreActionBar();
        lodPanel.setVisibility(View.GONE);
        setRefreshing(false);



        LeyendaCategoria.setText(MainSections[GlobalVars.momentSection]);
        if(GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.Name.compareTo("Outlet")==0)
        {
            LeyendaSubCategoria.setText("Outlet");

            String lagSub = GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.MerchantID;
            sendAnalytics(" >" + LeyendaCategoria.getText().toString() + " >" + LeyendaSubCategoria.getText().toString() + " >" + Product.Brand + " >" + Product.ID + "|" + Product.Name,
                    lagSub.replaceAll("\\D+", ""),
                    MainSections[GlobalVars.momentSection],
                    lagSub.replaceAll("\\D+", ""),
                    GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.Name,
                    String.valueOf(Product.BrandID),
                    Product.Brand
            );
        }
        else {
            if (GlobalVars.idioma == 1)
                LeyendaSubCategoria.setText(EukarazkoHitzulpena(GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.get(GlobalVars.momentSub).principal.Name));
            else
                LeyendaSubCategoria.setText(GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.get(GlobalVars.momentSub).principal.Name);

            String lagSub = GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.MerchantID;
            String lagSub2 = GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.get(GlobalVars.momentSub).principal.MerchantID;

            sendAnalytics(" >" + LeyendaCategoria.getText().toString() + " >" + LeyendaSubCategoria.getText().toString() + " >" + Product.Brand + " >" + Product.ID + "|" + Product.Name,
                    lagSub.replaceAll("\\D+", ""),
                    MainSections[GlobalVars.momentSection],
                    lagSub2.replaceAll("\\D+", ""),
                    GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.get(GlobalVars.momentSub).principal.Name,
                    String.valueOf(Product.BrandID),
                    Product.Brand
            );
        }

        LeyendaMarca.setText(Product.Brand);
        LeyendaProducto.setText(Product.Name);

        List<String> urls = new ArrayList<String>();
        if(Prod.Image!=null) {
            BaseImageName = Prod.Image[0];
            String url = SERVER_URL + BaseImageName;
            urls.add(url);
            url = url.replace("_1", "_2");
            urls.add(url);
            url = url.replace("_2", "_3");
            urls.add(url);
            url = url.replace("_3", "_4");
            urls.add(url);
        }

        pager = (ViewPager) findViewById(R.id.pager);
        try {
            pageAdapter = new CustomPageAdapter(ProductSheetActivity.this, urls, GlobalVars.screenWidth, (int) (GlobalVars.screenWidth*0.667),pager);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        pager.setAdapter(pageAdapter);
        //pager.setId(contador);
        //contador++;
        pager.getLayoutParams().width = GlobalVars.screenWidth;
        pager.getLayoutParams().height = (int) (GlobalVars.screenWidth*0.667);

        botonZoom = (Button) findViewById(R.id.botonZoom);
        botonZoom.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                makeClick(pager.getCurrentItem());
            }
        });

        CirclePageIndicator mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator.setPadding(0, (int) (GlobalVars.screenWidth*0.7),0,0);
        mIndicator.setViewPager(pager);
        //mIndicator.setRadius(7.0f);
        mIndicator.setRadius(GlobalVars.screenWidth/80);

        //PagerView Timer

        final Handler handler = new Handler();

        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == 4) {
                    currentPage = 0;
                }
                pager.setCurrentItem(currentPage++, true);
            }
        };

        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1, 5000);

        botonPlayStop = (Button) findViewById(R.id.playStop);
        botonPlayStop.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if(pagerPlaying==true)
                {
                    swipeTimer.cancel();
                    botonPlayStop.setBackgroundResource(R.drawable.but_play);
                    pagerPlaying = false;
                }
                else
                {
                    swipeTimer = new Timer();
                    swipeTimer.schedule(new TimerTask() {

                        @Override
                        public void run() {
                            handler.post(Update);
                        }
                    }, 1, 5000);
                    botonPlayStop.setBackgroundResource(R.drawable.but_pause);
                    pagerPlaying = true;
                }
            }
        });

        BrandT.setText(Product.Brand);
        NameT.setText(Product.Name);
        DescriT.setText(Product.Description);
        //PrizeT.setText(Float.toString(Product.Price)+" €");
        PrizeT.setText( String.format( "%.2f", Product.Price)+" €");
        if(Product.Goretex) {
            GoreT.setVisibility(View.VISIBLE);
        }
        else{
            GoreT.setVisibility(View.INVISIBLE);
        }

        Medidas = new ArrayList<String>();
        if(GlobalVars.idioma == 1)
            Medidas.add(getResources().getString(R.string.producto_elegir_talla_eu));
        else
            Medidas.add(getResources().getString(R.string.producto_elegir_talla));

        for( Tallas si: Product.Size)
        {
            Medidas.add(si.Name);
        }

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(getApplicationContext(), R.layout.my_spinner_item, Medidas);
        spinnerArrayAdapter1.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        SizeS.setAdapter(spinnerArrayAdapter1);

        //spin1.setOnTouchListener(new AdapterView.OnTouchListener() {
        SizeS.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if(position>0) {
                    ProductSizePos = position;
                    ProductCant = 0;
                    callStock();
                }
                else if(ProductSizePos > -1)
                {
                    SizeS.setSelection(ProductSizePos);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        //relatedItems.getLayoutParams().width = Math.round(201* (Product.related.length-1) * displayMetrics.density);
        //relatedItems.getLayoutParams().height = Math.round(260 * displayMetrics.density);

        for (int number = 0; number < Product.related.length ; number++)
        {
            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            RelativeLayout menuLayout = (RelativeLayout) inflater.inflate(R.layout.product_cell, null, false);

            /*LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(GlobalVars.screenWidth/4-2, (int) (GlobalVars.screenWidth/3)-1);
            LinearLayout cont = (LinearLayout)menuLayout.findViewById(R.id.productContainer);
            cont.setLayoutParams(params);*/

            ImageView img = (ImageView) menuLayout.findViewById(R.id.imagen);
            //img.getLayoutParams().height = (int) (GlobalVars.screenWidth/2.8/1.5);
            //img.getLayoutParams().width = (int) (GlobalVars.screenWidth/2.8);

            TextView descripcion = (TextView) menuLayout.findViewById(R.id.brand);
            descripcion.setText(Product.related[number].Brand);
            //descripcion.setText(String.valueOf(number));

            TextView name = (TextView) menuLayout.findViewById(R.id.name);
            name.setText(Product.related[number].Name);

            TextView precio = (TextView) menuLayout.findViewById(R.id.price);
            precio.setText(String.format("%.2f", Product.related[number].Price)+" €");

            TextView precioOriginal = (TextView) menuLayout.findViewById(R.id.originalprice);
            if(Product.related[number].Price != Product.related[number].OriginalPrice)
            {
                //precioOriginal.setText(Float.toString(Product.related[number].OriginalPrice) + " €");
                precioOriginal.setText( String.format( "%.2f", Product.related[number].OriginalPrice)+" €");
                precioOriginal.setPaintFlags(precioOriginal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                TextView rebajaText = (TextView) menuLayout.findViewById(R.id.rebajatext);
                int lag = (int) (100 - Math.floor(Product.related[number].Price * 100 / Product.related[number].OriginalPrice));
                rebajaText.setText(lag+" %");
            }
            else
            {
                precioOriginal.setVisibility(View.INVISIBLE);
                RelativeLayout rebajaPanel = (RelativeLayout) menuLayout.findViewById(R.id.rebajapanel);
                rebajaPanel.setVisibility(View.INVISIBLE);
            }

            LinearLayout botonC = (LinearLayout)menuLayout.findViewById(R.id.botonCelda);
            botonC.setId(Product.related[number].ID);
            //menuLayout.setId(Product.related[number].ID);

            /*RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(GlobalVars.screenWidth/4-2, (int) (GlobalVars.screenWidth/3.6)-1);
            int x = 2 + number%4 * GlobalVars.screenWidth/4 -1;
            int y = (int) (2 + number/4 *GlobalVars.screenWidth/3.6);
            params2.setMargins(x, y, 1, 0);*/

            RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams((GlobalVars.screenWidth/2 -2),(int)(GlobalVars.screenWidth/1.8-1));
            int x = 2 + number%2 * (GlobalVars.screenWidth/2 -1);
            int y = 1 + number/2 * (int)(GlobalVars.screenWidth/1.8);
            params2.setMargins(x, y, 1, 0);
            relatedItems.addView(menuLayout,params2);

            String pathURL = SERVER_URL;
            pathURL += Product.related[number].Image[0];

            String categoriaLag = "";

            if(GlobalVars.Categorias.get(GlobalVars.momentSection-2).principal.Name.compareTo("Outlet")==0)
                categoriaLag = "Outlet";
            else
                categoriaLag = GlobalVars.Categorias.get(GlobalVars.momentSection-2).secundarios.get(GlobalVars.momentSub).principal.Name;

            com.google.android.gms.analytics.ecommerce.Product product = new Product()
                    .setId(String.valueOf(Product.related[number].ID))
                    .setName(Product.related[number].Name)
                    .setCategory(categoriaLag)
                    .setBrand(Product.related[number].Brand)
                    .setVariant(MainSections[GlobalVars.momentSection])
                    .setPosition(number);
            HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                    .addImpression(product, "Productos_Relacionados");

            tracker.setScreenName("VistaProducto");
            tracker.send(builder.build());

            LoadImage imgJson = new LoadImage(img,ProductSheetActivity.this);
            imgJson.execute(pathURL);
        }
    }
}


