package es.com.acc.appEcommerce.activitys;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.CestaCompra;
import es.com.acc.appEcommerce.entities.CestaItem;
import es.com.acc.appEcommerce.entities.Producto;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Modificar_Cesta;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Remove_Cesta;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

/**
 * Created by Imanol on 20/05/2015.
 */
public class CestaActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{
    public LinearLayout container;
    public int itemKont;
    public String SERVER_URL = "http://images.kukimba.com/item/233x155/";
    private JSONTask_Modificar_Cesta jsonCesta;
    private JSONTask_Remove_Cesta removeJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Bundle b = getIntent().getExtras();
        //ProductID = b.getInt("keyID");

        setContentView(R.layout.activity_cesta);

        dataCharged = true;

        TextView gastoEnvT =  (TextView) findViewById(R.id.GastoEnvio);
        TextView gastoToT =  (TextView) findViewById(R.id.GastoTotal);
        TextView txtCompr =  (TextView) findViewById(R.id.txtCompra);
        TextView botCompr =  (TextView) findViewById(R.id.botonCompra);



        if(GlobalVars.idioma == 1)
        {
            mTitle = getResources().getString(R.string.cesta_title_eu);
            gastoEnvT.setText(getResources().getString(R.string.cesta_gastos_eu));
            gastoToT.setText(getResources().getString(R.string.cesta_total_iva_eu));
            txtCompr.setText(getResources().getString(R.string.cesta_trusted_text_eu));
            botCompr.setText(getResources().getString(R.string.cesta_finalizar_compra_eu));
        }
        else
        {
            mTitle = getResources().getString(R.string.cesta_title);
            gastoEnvT.setText(getResources().getString(R.string.cesta_gastos));
            gastoToT.setText(getResources().getString(R.string.cesta_total_iva));
            txtCompr.setText(getResources().getString(R.string.cesta_trusted_text));
            botCompr.setText(getResources().getString(R.string.cesta_finalizar_compra));
        }

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                CategoriasPrincipales,R.drawable.ic_drawer);

        feelView();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);
        sendCestaAnalytics("inicio");
    }

    public void sendCestaAnalytics(String val)
    {
        String lag = "es";
        if(GlobalVars.idioma == 1)lag = "eu";
        String lagUser = "";
        if(GlobalVars.UserData!=null){
            lagUser = String.valueOf(GlobalVars.UserData.ID);
        }

        // Set screen name.
        tracker.setScreenName("VistaCesta");

        // Send a screen view.
        tracker.send(new HitBuilders.ScreenViewBuilder()
                .setCustomDimension(1, lag)
                .setCustomDimension(2, lagUser)
                .setCustomDimension(3, "(not set)")
                .setCustomDimension(4, "(not set)")
                .setCustomDimension(5, val)
                .setCustomDimension(6, "(not set)")
                .setCustomDimension(7, "Cesta de la compra")
                .setCustomDimension(8, Locale.getDefault().getLanguage())
                .build());
    }


    public void feelView()
    {
        if(container!=null)
        {
            container.removeAllViews();
        }
        else {
            container = (LinearLayout) findViewById(R.id.cellContainer);
        }

        itemKont = 0;
        if(GlobalVars.CestaData!=null)
        {
            int number = 0;
            float sumaTotal = 0;
            for (final CestaItem item : GlobalVars.CestaData.Items) {
                //JSONTask_Cesta json = new JSONTask_Cesta(this,CestaActivity.this, item.getId());
                //json.execute();
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout menuLayout = (LinearLayout) inflater.inflate(R.layout.cell_cesta, null, false);

                LinearLayout containerProd = (LinearLayout) menuLayout.findViewById(R.id.conatinerProducto);
                /*containerProd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(CestaActivity.this, ProductSheetActivity.class);
                        GlobalVars.ProductID = item.ItemId;
                        startActivity(intent);
                    }
                });*/

                TextView tallaT =  (TextView) menuLayout.findViewById(R.id.txtTalla);
                TextView unidadesT =  (TextView) menuLayout.findViewById(R.id.txtSpineer);
                TextView preciosT =  (TextView) menuLayout.findViewById(R.id.txtPrecio);
                TextView txtTotT =  (TextView) menuLayout.findViewById(R.id.txtTotal);

                if(GlobalVars.idioma == 1)
                {
                    tallaT.setText(getResources().getString(R.string.cesta_talla_eu));
                    unidadesT.setText(getResources().getString(R.string.cesta_unidades_eu));
                    preciosT.setText(getResources().getString(R.string.cesta_unitario_eu));
                    txtTotT.setText(getResources().getString(R.string.cesta_total_eu));
                }
                else
                {
                    tallaT.setText(getResources().getString(R.string.cesta_talla));
                    unidadesT.setText(getResources().getString(R.string.cesta_unidades));
                    preciosT.setText(getResources().getString(R.string.cesta_unitario));
                    txtTotT.setText(getResources().getString(R.string.cesta_total));
                }

                TextView brandText = (TextView) menuLayout.findViewById(R.id.productBrand);
                brandText.setText(String.valueOf(item.ItemBrand));

                TextView titleText = (TextView) menuLayout.findViewById(R.id.productName);
                titleText.setText(String.valueOf(item.ItemName));

                ImageView img = (ImageView) menuLayout.findViewById(R.id.imageCestaItem);
                LoadImage imgJson = new LoadImage(img,CestaActivity.this);
                imgJson.execute(SERVER_URL+item.Image);

                TextView tallaText = (TextView) menuLayout.findViewById(R.id.productTalla);
                tallaText.setText(item.SizeDisplay);

                TextView precioText = (TextView) menuLayout.findViewById(R.id.productPrecio);
                precioText.setText(String.format( "%.2f", item.Price)+ " €");

                TextView preciOriginalText = (TextView) menuLayout.findViewById(R.id.productPrecioOriginal);
                preciOriginalText.setPaintFlags(preciOriginalText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                preciOriginalText.setText(String.format( "%.2f", item.OriginalPrice)+ " €");

                TextView productTotal = (TextView) menuLayout.findViewById(R.id.productTotal);
                productTotal.setText(String.format( "%.2f", item.Price*item.Units)+ " €");

                sumaTotal += item.Price*item.Units;

                final Spinner cantidad = (Spinner) menuLayout.findViewById(R.id.spinner);

                List<String> cantidadItem = new ArrayList<String>();

                for(int i=1; i <= item.Stock; i++)
                {
                    cantidadItem.add(String.valueOf(i));
                }

                ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(getApplicationContext(), R.layout.my_spinner_item, cantidadItem);
                spinnerArrayAdapter1.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                cantidad.setAdapter(spinnerArrayAdapter1);

                cantidad.setSelection(item.Units-1);

                cantidad.post(new Runnable() {
                    public void run() {
                        cantidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                                // your code here
                                //if(position == 0)
                                //{
                                //    callRemoveCesta(item.ItemId, item.SizeId);
                                //}
                                //else
                                //{
                                    callModificaCesta(item.ItemId,position+1,item.Stock,item.SizeId,item.SizeDisplay,item.Availability);
                                //}
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });
                    }
                });
                //spin1.setOnTouchListener(new AdapterView.OnTouchListener() {

                LinearLayout closeProd = (LinearLayout) menuLayout.findViewById(R.id.closeButton);
                closeProd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callRemoveCesta(item.ItemId, item.SizeId);
                    }
                });

                /*RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(GlobalVars.screenWidth , (int) (GlobalVars.screenWidth/2.5 -2));
                int x = 0;
                int y = (int) (number * GlobalVars.screenWidth/2.5);
                params2.setMargins(x, y, 0, 0);
                container.addView(menuLayout, params2);*/
                container.addView(menuLayout);

                number++;
            }
            TextView textSumaTotal = (TextView) findViewById(R.id.PriceTotal);
            textSumaTotal.setText(String.format("%.2f", sumaTotal)+" €");
        }
        else
        {
            TextView textSumaTotal = (TextView) findViewById(R.id.PriceTotal);
            textSumaTotal.setText("0,00 €");
        }
    }

    public void callModificaCesta(int prodID, int units, int stock, String sizeId, String sizeName, int avaUnits)
    {
        jsonCesta = new JSONTask_Modificar_Cesta(this, CestaActivity.this,prodID,GlobalVars.CestaID,GlobalVars.UserData.ID,units,stock,sizeId,sizeName,avaUnits);
        jsonCesta.execute();
    }

    public void callRemoveCesta(int prodID, String size)
    {
        final int parentId = prodID;
        final String parentSize = size;
        String lagOK;
        String lagKO;
        AlertDialog alertDialog = new AlertDialog.Builder(CestaActivity.this).create();
        if (GlobalVars.idioma == 1) {
            alertDialog.setTitle(getResources().getString(R.string.cesta_borrar_eu));
            alertDialog.setMessage(getResources().getString(R.string.cesta_confirmacion_eu));
            lagOK = getResources().getString(R.string.dialog_ok_eu);
            lagKO = getResources().getString(R.string.dialog_ko_eu);
        }
        else {
            alertDialog.setTitle(getResources().getString(R.string.cesta_borrar));
            alertDialog.setMessage(getResources().getString(R.string.cesta_confirmacion));
            lagOK = getResources().getString(R.string.dialog_ok);
            lagKO = getResources().getString(R.string.dialog_ko);
        }
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, lagOK,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        eraseProd(parentId,parentSize);
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, lagKO,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void eraseProd(int prodID, String size)
    {
        removeJson = new JSONTask_Remove_Cesta(this,CestaActivity.this,prodID,GlobalVars.CestaID,GlobalVars.UserData.ID,size);
        removeJson.execute();
    }

    public void xmlCestaCharged(CestaCompra ces)
    {
        GlobalVars.CestaData = ces;
        if(GlobalVars.CestaData!=null)
            GlobalVars.cesta_count = GlobalVars.CestaData.Items.length;
        else
            GlobalVars.cesta_count = 0;
        updateHotCount(GlobalVars.cesta_count);
        //Toast.makeText(this, "Elemento eliminado.", Toast.LENGTH_SHORT).show();
        feelView();
    }

    public void onClick(View view)
    {
        Intent intent = new Intent(CestaActivity.this, ProductSheetActivity.class);
        GlobalVars.ProductID = view.getId();
        startActivity(intent);
        //finish();
    }

    public void onClose(View view)
    {
        Intent intent = new Intent(CestaActivity.this, ProductSheetActivity.class);
        GlobalVars.ProductID = view.getId();
        startActivity(intent);
        //finish();
    }

    public void ComprarClicked(View view)
    {
        Intent nextScreen;
        nextScreen = new Intent(getApplicationContext(), CompraIdentificacionActivity.class);
        startActivity(nextScreen);
        finish();
    }


    public void xmlCharged(Producto Prod)
    {

    }

    @Override
    public void  onBackPressed ()
    {
        if(dataCharged)NavUtils.navigateUpFromSameTask(this);
    }
}
