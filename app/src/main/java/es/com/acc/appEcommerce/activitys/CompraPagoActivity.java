package es.com.acc.appEcommerce.activitys;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.activitys.WebView.TpvWebActivity;
import es.com.acc.appEcommerce.entities.AddUserResponse;
import es.com.acc.appEcommerce.entities.Address;
import es.com.acc.appEcommerce.entities.CestaItem;
import es.com.acc.appEcommerce.entities.ItemCategories;
import es.com.acc.appEcommerce.entities.RespuestaPedido;
import es.com.acc.appEcommerce.entities.ShippingCost;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Add_Direcciones;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Add_Order;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Get_Direcciones;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Get_ShippingCost;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Get_TipoEnvio;
import es.com.acc.appEcommerce.views.DireccionDialog;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

/**
 * Created by Imanol on 29/06/2015.
 */
public class CompraPagoActivity  extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, DireccionDialog.DialogListener
{

    private JSONTask_Get_Direcciones json;
    private JSONTask_Add_Direcciones jsonAdd;
    private JSONTask_Get_TipoEnvio jsonTipoEnvio;
    private JSONTask_Get_ShippingCost jsonShippingCost;
    private JSONTask_Add_Order jsonRespuestaPedido;

    List<Address> direcciones;

    private TextView identificacion;
    private TextView pago;
    private TextView confirma;
    private TextView formaPago;
    private RadioButton radioTarjeta;
    private RadioButton radioReembolso;
    private TextView formaEnvio;
    private Spinner spinnerEnvio;
    private TextView direccionEnvio;
    private Button anadirBot;
    private Button modificarBot;
    private Spinner spinnerDireccion;
    private TextView dirLarga;

    public LinearLayout container;
    public String SERVER_URL = "http://images.kukimba.com/item/233x155/";

    List<String> Direccion;
    List<String> TipoEnvio;

    float sumaTotal = 0;
    float costeEnvio = 0;

    TextView textEnvioTotal;
    TextView textSumaTotal;

    List<ItemCategories> tiposEnvio;
    int selectedTipoEnvio = -1;
    int SelectedProvince = -1;
    int SelectedAdressID = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_compra_pago);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                CategoriasPrincipales,R.drawable.ic_action_previous_item);

        identificacion= (TextView) findViewById(R.id.leyendaIdentificacion);
        pago= (TextView) findViewById(R.id.leyendaPagoEnvio);
        confirma= (TextView) findViewById(R.id.leyendaConfirmacion);

        formaPago= (TextView) findViewById(R.id.formaTxt);
        radioTarjeta = (RadioButton) findViewById(R.id.radioTarjeta);
        radioReembolso = (RadioButton) findViewById(R.id.radioReembolso);

        if(GlobalVars.payMethod == 1)
        {
            radioTarjeta.setChecked(true);
        }
        else
        {
            radioReembolso.setChecked(true);
        }

        formaEnvio= (TextView) findViewById(R.id.envioText);
        spinnerEnvio = (Spinner)findViewById(R.id.envioSpinner);

        direccionEnvio= (TextView) findViewById(R.id.direccionText);
        anadirBot = (Button) findViewById(R.id.botonAnadir);
        modificarBot = (Button) findViewById(R.id.botonModificar);
        spinnerDireccion = (Spinner)findViewById(R.id.direccionSpinner);
        dirLarga= (TextView) findViewById(R.id.direccionLargaText);

        TipoEnvio = new ArrayList<String>();
        Direccion = new ArrayList<String>();

        TextView gastoEnvT =  (TextView) findViewById(R.id.GastoEnvio);
        TextView gastoToT =  (TextView) findViewById(R.id.GastoTotal);
        TextView txtCompr =  (TextView) findViewById(R.id.txtCompra);
        TextView botCompr =  (TextView) findViewById(R.id.botonCompra);

        if (GlobalVars.idioma == 1) {
            mTitle = getResources().getString(R.string.compra_title_eu);
            identificacion.setText(getResources().getString(R.string.compra_identificacion_eu));
            pago.setText(getResources().getString(R.string.compra_pago_eu));
            confirma.setText(getResources().getString(R.string.compra_confirmacion_eu));

            formaPago.setText(getResources().getString(R.string.compra_forma_pago_eu));
            radioTarjeta.setText(getResources().getString(R.string.compra_forma_pago_tarjeta_eu));
            radioReembolso.setText(getResources().getString(R.string.compra_forma_pago_reembolso_eu));
            formaEnvio.setText(getResources().getString(R.string.compra_tipo_envio_eu));
            direccionEnvio.setText(getResources().getString(R.string.compra_direccion_eu));
            anadirBot.setText(getResources().getString(R.string.compra_direccion_anadir_eu));
            modificarBot.setText(getResources().getString(R.string.compra_direccion_modificar_eu));
            TipoEnvio.add(getResources().getString(R.string.compra_tipo_envio_seleccion_eu));
            Direccion.add(getResources().getString(R.string.compra_direccion_seleccion_eu));

            gastoEnvT.setText(getResources().getString(R.string.cesta_gastos_eu));
            gastoToT.setText(getResources().getString(R.string.cesta_total_iva_eu));
            txtCompr.setText(getResources().getString(R.string.cesta_trusted_text_eu));
            botCompr.setText(getResources().getString(R.string.compra_pagar_eu));
        }
        else{
            mTitle = getResources().getString(R.string.compra_title);
            identificacion.setText(getResources().getString(R.string.compra_identificacion));
            pago.setText(getResources().getString(R.string.compra_pago));
            confirma.setText(getResources().getString(R.string.compra_confirmacion));

            formaPago.setText(getResources().getString(R.string.compra_forma_pago));
            radioTarjeta.setText(getResources().getString(R.string.compra_forma_pago_tarjeta));
            radioReembolso.setText(getResources().getString(R.string.compra_forma_pago_reembolso));
            formaEnvio.setText(getResources().getString(R.string.compra_tipo_envio));
            direccionEnvio.setText(getResources().getString(R.string.compra_direccion));
            anadirBot.setText(getResources().getString(R.string.compra_direccion_anadir));
            modificarBot.setText(getResources().getString(R.string.compra_direccion_modificar));
            TipoEnvio.add(getResources().getString(R.string.compra_tipo_envio_seleccion));
            Direccion.add(getResources().getString(R.string.compra_direccion_seleccion));

            gastoEnvT.setText(getResources().getString(R.string.cesta_gastos));
            gastoToT.setText(getResources().getString(R.string.cesta_total_iva));
            txtCompr.setText(getResources().getString(R.string.cesta_trusted_text));
            botCompr.setText(getResources().getString(R.string.compra_pagar));
        }

        modificarBot.setVisibility(View.INVISIBLE);

        json = new JSONTask_Get_Direcciones(this, CompraPagoActivity.this, String.valueOf(GlobalVars.UserData.ID), GlobalVars.password);
        json.execute();
    }

    @Override
    public void xmlDirectionsCharged(List<Address> adreses)
    {
        dataCharged = true;
        setRefreshing(false);

        direcciones = adreses;

        dataCharged = true;
        setRefreshing(false);

        if(direcciones!=null) {

            feelView();

            if(adreses.size()>0)
            {
                modificarBot.setVisibility(View.VISIBLE);
            }

            int selected = 0;
            for (int number = 0; number < adreses.size(); number++) {
                Direccion.add(direcciones.get(number).Contact+" - "+direcciones.get(number).AddressName);
                if(direcciones.get(number).IsDefault)
                {
                    selected = number + 1;
                }
            }

            ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(getApplicationContext(), R.layout.my_spinner_item, Direccion);
            spinnerArrayAdapter2.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            spinnerDireccion.setAdapter(spinnerArrayAdapter2);
            //if (!dialogInited) {
            //spin1.setOnTouchListener(new AdapterView.OnTouchListener() {
            //spinnerEnvio.setId(kont1);
            spinnerDireccion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    // your code here
                    if(position>0) {
                        dirLarga.setText(direcciones.get(position-1).Contact + "\n" +
                                direcciones.get(position-1).AddressName + "\n" +
                                direcciones.get(position-1).City + "\n" +
                                direcciones.get(position-1).Zip);

                        SelectedProvince = direcciones.get(position-1).Province;

                        GlobalVars.DireccionEntrega = dirLarga.getText().toString();

                        SelectedAdressID = direcciones.get(position-1).Id;

                        callShippingCost();
                    }else
                    {
                        dirLarga.setText("");
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }
            });

            spinnerDireccion.setSelection(selected);

            jsonTipoEnvio = new JSONTask_Get_TipoEnvio(this, CompraPagoActivity.this, String.valueOf(GlobalVars.payMethod));
            jsonTipoEnvio.execute();
        }
    }

    public void xmlTipoEnvioCharged(List<ItemCategories> types)
    {
        tiposEnvio = types;
        TipoEnvio = new ArrayList<String>();

        for( ItemCategories cat: types) {
            TipoEnvio.add(cat.Name);
        }

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(getApplicationContext(), R.layout.my_spinner_item, TipoEnvio);
        spinnerArrayAdapter1.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinnerEnvio.setAdapter(spinnerArrayAdapter1);

        spinnerEnvio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                selectedTipoEnvio =  position;
                callShippingCost();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }

    public void callShippingCost()
    {
        if((selectedTipoEnvio>-1)&&(SelectedProvince>-1)) {
            //jsonShippingCost = new JSONTask_Get_ShippingCost(this, CompraPagoActivity.this, String.valueOf(GlobalVars.CestaID), GlobalVars.payMethod+1, Integer.parseInt(tiposEnvio.get(selectedTipoEnvio).Id), SelectedProvince, Math.round(sumaTotal));
            jsonShippingCost = new JSONTask_Get_ShippingCost(this, CompraPagoActivity.this, String.valueOf(GlobalVars.CestaID), String.valueOf(GlobalVars.payMethod + 1), tiposEnvio.get(selectedTipoEnvio).Id, String.valueOf(SelectedProvince), String.valueOf(sumaTotal).replace(".",","));
            jsonShippingCost.execute();
        }
    }

    public void xmlCosteEnvioCharged(ShippingCost cost)
    {
        textEnvioTotal.setText(String.format("%.2f", cost.Price)+" €");
        costeEnvio =  (float) cost.Price;
        float lag =sumaTotal + costeEnvio;
        textSumaTotal.setText(String.format("%.2f", lag)+" €");
    }

    public void ComprarClicked(View view)
    {
        GlobalVars.payMethod = 1;

        jsonRespuestaPedido = new JSONTask_Add_Order(this, CompraPagoActivity.this, String.valueOf(GlobalVars.CestaID), String.valueOf(GlobalVars.payMethod), tiposEnvio.get(selectedTipoEnvio).Id, String.valueOf(SelectedAdressID), String.valueOf(GlobalVars.UserData.ID), String.valueOf(GlobalVars.shopID));
        jsonRespuestaPedido.execute();
        /*Intent nextScreen;
        //nextScreen = new Intent(getApplicationContext(), CompraConfirmacion.class);
        nextScreen = new Intent(getApplicationContext(), TpvWebActivity.class);
        startActivity(nextScreen);
        finish();*/
    }

    public void xmlRespuestaPedidoCharged(RespuestaPedido respuesta)
    {
        Intent nextScreen;

        nextScreen = new Intent(getApplicationContext(), CompraConfirmacion.class);

        if(GlobalVars.payMethod == 1)
        {
            GlobalVars.TPVvalue = respuesta.TPVCode;
            nextScreen = new Intent(getApplicationContext(), TpvWebActivity.class);
        }
        startActivity(nextScreen);
        finish();
    }


    @Override
    public void onStart()
    {
        super.onStart();

        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);
    }

    public void feelView()
    {
        if(container!=null)
        {
            container.removeAllViews();
        }
        else {
            container = (LinearLayout) findViewById(R.id.cellContainer);
        }

        if(GlobalVars.CestaData!=null)
        {
            int number = 0;
            sumaTotal = 0;
            for (final CestaItem item : GlobalVars.CestaData.Items) {
                //JSONTask_Cesta json = new JSONTask_Cesta(this,CestaActivity.this, item.getId());
                //json.execute();
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout menuLayout = (LinearLayout) inflater.inflate(R.layout.cell_compra, null, false);

                TextView tallaT =  (TextView) menuLayout.findViewById(R.id.txtTalla);
                TextView unidadesT =  (TextView) menuLayout.findViewById(R.id.txtCantidad);
                TextView preciosT =  (TextView) menuLayout.findViewById(R.id.txtPrecio);
                TextView txtTotT =  (TextView) menuLayout.findViewById(R.id.txtTotal);

                if(GlobalVars.idioma == 1)
                {
                    tallaT.setText(getResources().getString(R.string.cesta_talla_eu));
                    unidadesT.setText(getResources().getString(R.string.cesta_unidades_eu));
                    preciosT.setText(getResources().getString(R.string.cesta_unitario_eu));
                    txtTotT.setText(getResources().getString(R.string.cesta_total_eu));
                }
                else
                {
                    tallaT.setText(getResources().getString(R.string.cesta_talla));
                    unidadesT.setText(getResources().getString(R.string.cesta_unidades));
                    preciosT.setText(getResources().getString(R.string.cesta_unitario));
                    txtTotT.setText(getResources().getString(R.string.cesta_total));
                }

                TextView brandText = (TextView) menuLayout.findViewById(R.id.productBrand);
                brandText.setText(String.valueOf(item.ItemBrand));

                TextView titleText = (TextView) menuLayout.findViewById(R.id.productName);
                titleText.setText(String.valueOf(item.ItemName));

                ImageView img = (ImageView) menuLayout.findViewById(R.id.imageCestaItem);
                LoadImage imgJson = new LoadImage(img,CompraPagoActivity.this);
                imgJson.execute(SERVER_URL+item.Image);

                TextView tallaText = (TextView) menuLayout.findViewById(R.id.productTalla);
                tallaText.setText(item.SizeDisplay);

                TextView precioText = (TextView) menuLayout.findViewById(R.id.productPrecio);
                precioText.setText(String.format("%.2f", item.Price)+ " €");

                TextView preciOriginalText = (TextView) menuLayout.findViewById(R.id.productPrecioOriginal);
                preciOriginalText.setPaintFlags(preciOriginalText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                preciOriginalText.setText(String.format("%.2f", item.OriginalPrice)+ " €");

                TextView productTotal = (TextView) menuLayout.findViewById(R.id.productTotal);
                productTotal.setText(String.format( "%.2f", item.Price*item.Units)+ " €");

                sumaTotal += item.Price*item.Units;

                TextView cantidad = (TextView) menuLayout.findViewById(R.id.valorCantidad);

                cantidad.setText(String.valueOf(item.Units));

                container.addView(menuLayout);

                number++;
            }
            textSumaTotal = (TextView) findViewById(R.id.PriceTotal);
            textSumaTotal.setText(String.format("%.2f", sumaTotal)+" €");

            textEnvioTotal = (TextView) findViewById(R.id.PriceEnvio);
        }
        else
        {
            TextView textSumaTotal = (TextView) findViewById(R.id.PriceTotal);
            textSumaTotal.setText("0,00 €");
        }
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioTarjeta:
                if (checked)
                    // Pirates are the best
                    GlobalVars.payMethod = 1;
                    break;
            case R.id.radioReembolso:
                if (checked)
                    // Ninjas rule
                    GlobalVars.payMethod = 2;
                    break;
        }

        jsonTipoEnvio = new JSONTask_Get_TipoEnvio(this, CompraPagoActivity.this, String.valueOf(GlobalVars.payMethod));
        jsonTipoEnvio.execute();
    }

    public void AnadirClicked(View view) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        DireccionDialog direccionFragment = new DireccionDialog();
        if (GlobalVars.idioma == 1)
            direccionFragment.setValues(getResources().getString(R.string.direcciones_dialog_anade_eu), null);
        else
            direccionFragment.setValues(getResources().getString(R.string.direcciones_dialog_anade), null);
        direccionFragment.show(fragmentManager, "Dialog");
        AlertDialog dialog = (AlertDialog) direccionFragment.getDialog();
    }

    public void ModificarClicked(View view) {
        if(spinnerDireccion.getSelectedItemPosition()>0) {
            FragmentManager fragmentManager = getSupportFragmentManager();

            DireccionDialog direccionFragment = new DireccionDialog();
            if (GlobalVars.idioma == 1)
                direccionFragment.setValues(getResources().getString(R.string.direcciones_dialog_modifica_eu), direcciones.get(spinnerDireccion.getSelectedItemPosition() - 1));
            else
                direccionFragment.setValues(getResources().getString(R.string.direcciones_dialog_modifica), direcciones.get(spinnerDireccion.getSelectedItemPosition() - 1));
            direccionFragment.show(fragmentManager, "Dialog");
            AlertDialog dialog = (AlertDialog) direccionFragment.getDialog();
        }
    }

    public void onDialogPositiveClick(DialogFragment dialog, Address dir) {
        // User touched the dialog's positive button
        //Toast.makeText(getApplicationContext(), String.valueOf(value), Toast.LENGTH_SHORT).show();
        jsonAdd = new JSONTask_Add_Direcciones(this, CompraPagoActivity.this, String.valueOf(dir.Id), String.valueOf(GlobalVars.UserData.ID), dir.Contact, dir.AddressName, dir.City, dir.Zip, String.valueOf(dir.Province), String.valueOf(dir.Country), dir.Observation, String.valueOf(dir.IsDefault));
        jsonAdd.execute();
    }

    @Override
    public void xmlDirAdded(AddUserResponse response) {
        Intent intent = new Intent(CompraPagoActivity.this, CompraPagoActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), String.valueOf(response.Error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void  onBackPressed ()
    {
        if(dataCharged)NavUtils.navigateUpFromSameTask(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.comparte) {
            //Toast.makeText(ProductSheetActivity.this, "Compartir", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (id == R.id.favorito) {
            //Toast.makeText(ProductSheetActivity.this, "Favoritos", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (id == R.id.carrit) {
            carritoClicked();
            return true;
        }
        else if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
