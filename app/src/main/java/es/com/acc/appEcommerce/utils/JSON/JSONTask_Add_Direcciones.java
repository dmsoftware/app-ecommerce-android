package es.com.acc.appEcommerce.utils.JSON;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import es.com.acc.appEcommerce.activitys.BaseActivity;
import es.com.acc.appEcommerce.entities.AddUserResponse;


public class JSONTask_Add_Direcciones extends AsyncTask<Void, Void, String>
{
    private Context context;
    private BaseActivity PrSh;
    private static final String TAG = "PostFetcher";
    private Boolean UserLoaded = false;
    public static final String SERVER_URL = "-- url del servicio --";
    private AddUserResponse userAdresses;

    private String pathURL;

    public JSONTask_Add_Direcciones(BaseActivity A, Context mContext, String _Id, String _UserID, String _Contact, String _AddressName, String _City, String _Zip, String _Province, String _Country, String _Observation, String _IsDefault)
    {
        pathURL = SERVER_URL;
        pathURL += "&Id=";
        pathURL +=  _Id;
        pathURL += "&UserId=";
        pathURL += _UserID;
        pathURL += "&Contact=";
        pathURL += _Contact;
        pathURL += "&AddressName=";
        pathURL += _AddressName;
        pathURL += "&City=";
        pathURL += _City;
        pathURL += "&Zip=";
        pathURL += _Zip;
        pathURL += "&Province=";
        pathURL += _Province;
        pathURL += "&Country=";
        pathURL += _Country;
        pathURL += "&Observation=";
        pathURL += _Observation;
        pathURL += "&IsDefault=";
        pathURL += _IsDefault;

        this.PrSh = A;
        this.context = mContext;
        UserLoaded = false;
    }

    private void handlePostsList(AddUserResponse posts)
    {
        this.userAdresses = posts;
    }

    //private void failedLoadingPosts()
    //{
    //    Log.d("Error", "Post Failed");
    //}

    @Override
    protected void onPostExecute(String result)
    {
        if(UserLoaded==false)
        {
            UserLoaded = true;
            PrSh.xmlDirAdded(userAdresses);
        }
    }

     @Override
     protected String doInBackground(Void... params)
     {
         try {
             //Create an HTTP client
             HttpClient client = new DefaultHttpClient();

             System.out.println(" ::::::::::::::::::::::::::::: Get direcciones: >> "+pathURL);

             HttpPost post = new HttpPost(pathURL);

             //Perform the request and check the status code
             HttpResponse response = client.execute(post);
             StatusLine statusLine = response.getStatusLine();
             if(statusLine.getStatusCode() == 200) {
                 HttpEntity entity = response.getEntity();
                 InputStream content = entity.getContent();

                 try {
                     //Read the server response and attempt to parse it as JSON
                     Reader reader = new InputStreamReader(content);

                     GsonBuilder gsonBuilder = new GsonBuilder();
                     gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                     Gson gson = gsonBuilder.create();

                     AddUserResponse log = gson.fromJson(reader, AddUserResponse.class);
                     content.close();

                     handlePostsList(log);
                 } catch (Exception ex) {
                     //Log.e(TAG, "Failed to parse JSON due to: " + ex);
                     //failedLoadingPosts();
                 }
             } else {
                 //Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                 //failedLoadingPosts();
             }
         } catch(Exception ex) {
             //Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
             //failedLoadingPosts();
         }
         return null;
     }
}
