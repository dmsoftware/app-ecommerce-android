package es.com.acc.appEcommerce.utils.JSON;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.com.acc.appEcommerce.activitys.LoaderActivity;
import es.com.acc.appEcommerce.entities.CategoriaRow;
import es.com.acc.appEcommerce.entities.Categories;


public class JSONTask_Categorias extends AsyncTask<Void, Void, String>{

    private Context context;
    private LoaderActivity Lod;
    private static final String TAG = "PostFetcher";
    private Boolean CatsLoaded = false;
    public static final String SERVER_URL = "-- url del servicio --";
    private List<CategoriaRow> Categorias = new ArrayList<CategoriaRow>();

    public JSONTask_Categorias(LoaderActivity A, Context mContext){
        this.Lod = A;
        this.context = mContext;
        CatsLoaded = false;
    }

    private void handlePostsList(List<Categories> posts) {

        //this.Categorias = posts;
        for(Categories post : posts)
        {
            //Log.d("Categorie", post.Name);
            if(post.Parent == -1)
            {
                CategoriaRow cat = new CategoriaRow();
                cat.principal = post;
                cat.secundarios = new ArrayList<CategoriaRow>();

                Categorias.add(cat);
            }
            else
            {
                CategoriaRow cat0 = new CategoriaRow();
                cat0.principal = post;
                //cat0.secundarios = new ArrayList<CategoriaRow>();
                CategoriaRow cat1 = Categorias.get(post.Parent-1);
                cat1.secundarios.add(cat0);
            }
        }
    }

    private void failedLoadingPosts() {
        //Log.d("Error", "Post Failed");
    }

    @Override
    protected void onPostExecute(String result)
    {
        if(CatsLoaded==false) {
            CatsLoaded = true;
            Lod.xmlCharged(Categorias);
        }
    }

     @Override
     protected String doInBackground(Void... params) {
         try {
             System.out.println(" ::::::::::::::::::::::::::::: Get categorias : >> "+SERVER_URL);
             //Create an HTTP client
             HttpClient client = new DefaultHttpClient();
             HttpPost post = new HttpPost(SERVER_URL);

             //Perform the request and check the status code
             HttpResponse response = client.execute(post);
             StatusLine statusLine = response.getStatusLine();
             if(statusLine.getStatusCode() == 200) {
                 HttpEntity entity = response.getEntity();
                 InputStream content = entity.getContent();

                 try {
                     //Read the server response and attempt to parse it as JSON
                     Reader reader = new InputStreamReader(content);

                     GsonBuilder gsonBuilder = new GsonBuilder();
                     gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                     Gson gson = gsonBuilder.create();
                     List<Categories> Categorias = Arrays.asList(gson.fromJson(reader, Categories[].class));
                     content.close();

                     handlePostsList(Categorias);
                 } catch (Exception ex) {
                     //Log.e(TAG, "Failed to parse JSON due to: " + ex);
                     failedLoadingPosts();
                 }
             } else {
                 //Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                 failedLoadingPosts();
             }
         } catch(Exception ex) {
             //Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
             failedLoadingPosts();
         }
         return null;
     }

}
