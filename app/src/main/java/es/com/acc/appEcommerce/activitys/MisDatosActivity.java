package es.com.acc.appEcommerce.activitys;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.AddUserResponse;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Modificar_User;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

/**
 * Created by Imanol on 20/05/2015.
 */
public class MisDatosActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{
    private JSONTask_Modificar_User json;

    TextView modifica_datos;
    TextView correoelectronico;
    TextView datos_personales;
    TextView nacimiento;
    TextView genero;

    EditText nombre;
    EditText apellido1;

    EditText apellido2;
    EditText movil;
    Button   dateBut;
    Spinner  gnreSpin;

    TextView cambio_pswd;

    EditText pswantiguo;
    EditText pswnuevo;
    EditText pswrepetido;

    CheckBox promociones;

    Button   enviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Bundle b = getIntent().getExtras();
        //ProductID = b.getInt("keyID");

        setContentView(R.layout.activity_mis_datos);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                CategoriasPrincipales, R.drawable.ic_drawer);

        modifica_datos = (TextView) findViewById(R.id.modifica_datos);
        datos_personales = (TextView) findViewById(R.id.datos_personales);
        correoelectronico = (TextView) findViewById(R.id.email);
        nacimiento = (TextView) findViewById(R.id.nacimiento);
        genero = (TextView) findViewById(R.id.genero);
        cambio_pswd = (TextView) findViewById(R.id.passwordChange);
        enviar = (Button) findViewById(R.id.boton);

        nombre = (EditText) findViewById(R.id.nombre);
        apellido1 = (EditText) findViewById(R.id.apellido1);
        apellido2 = (EditText) findViewById(R.id.apellido2);
        movil = (EditText) findViewById(R.id.telefono);
        dateBut = (Button) findViewById(R.id.datepick);
        promociones = (CheckBox) findViewById(R.id.promociones);
        pswantiguo = (EditText) findViewById(R.id.password0);
        pswnuevo = (EditText) findViewById(R.id.password1);
        pswrepetido = (EditText) findViewById(R.id.password2);

        List<String> list = new ArrayList<String>();

        if(GlobalVars.idioma == 1)
        {
            mTitle = getResources().getString(R.string.cuenta_misdatos_eu);
            modifica_datos.setText(getResources().getString(R.string.midatos_modifica_eu));
            datos_personales.setText(getResources().getString(R.string.midatos_personales_eu));
            correoelectronico.setText(getResources().getString(R.string.midatos_correo_eu));
            nacimiento.setText(getResources().getString(R.string.midatos_nacimiento_eu));
            genero.setText(getResources().getString(R.string.midatos_genero_eu));
            enviar.setText(getResources().getString(R.string.midatos_enviar_eu));
            promociones.setText(getResources().getString(R.string.midatos_promociones_eu));
            cambio_pswd.setText(getResources().getString(R.string.midatos_cambio_contra_eu));

            nombre.setHint(getResources().getString(R.string.midatos_hint_nombre_eu));
            apellido1.setHint(getResources().getString(R.string.midatos_hint_apellido_1_eu));
            apellido2.setHint(getResources().getString(R.string.midatos_hint_apellido_2_eu));
            movil.setHint(getResources().getString(R.string.midatos_hint_movil_eu));

            pswantiguo.setHint(getResources().getString(R.string.midatos_hint_psantiguo_eu));
            pswnuevo.setHint(getResources().getString(R.string.midatos_hint_psnuevo_eu));
            pswrepetido.setHint(getResources().getString(R.string.midatos_hint_psrepetido_eu));

            list.add(getResources().getString(R.string.midatos_gen_spin_1_eu));
            list.add(getResources().getString(R.string.midatos_gen_spin_2_eu));
            list.add(getResources().getString(R.string.midatos_gen_spin_3_eu));
        }
        else {
            mTitle = getResources().getString(R.string.cuenta_misdatos);
            modifica_datos.setText(getResources().getString(R.string.midatos_modifica));
            datos_personales.setText(getResources().getString(R.string.midatos_personales));
            correoelectronico.setText(getResources().getString(R.string.midatos_correo));
            nacimiento.setText(getResources().getString(R.string.midatos_nacimiento));
            genero.setText(getResources().getString(R.string.midatos_genero));
            enviar.setText(getResources().getString(R.string.midatos_enviar));
            promociones.setText(getResources().getString(R.string.midatos_promociones));
            cambio_pswd.setText(getResources().getString(R.string.midatos_cambio_contra));

            nombre.setHint(getResources().getString(R.string.midatos_hint_nombre));
            apellido1.setHint(getResources().getString(R.string.midatos_hint_apellido_1));
            apellido2.setHint(getResources().getString(R.string.midatos_hint_apellido_2));
            movil.setHint(getResources().getString(R.string.midatos_hint_movil));

            pswantiguo.setHint(getResources().getString(R.string.midatos_hint_psantiguo));
            pswnuevo.setHint(getResources().getString(R.string.midatos_hint_psnuevo));
            pswrepetido.setHint(getResources().getString(R.string.midatos_hint_psrepetido));

            list.add(getResources().getString(R.string.midatos_gen_spin_1));
            list.add(getResources().getString(R.string.midatos_gen_spin_2));
            list.add(getResources().getString(R.string.midatos_gen_spin_3));
        }


        gnreSpin = (Spinner) findViewById(R.id.sexResult);



        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this, R.layout.my_spinner_item, list);
        spinnerArrayAdapter1.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        gnreSpin.setAdapter(spinnerArrayAdapter1);
        //if (!dialogInited) {
        //spin1.setOnTouchListener(new AdapterView.OnTouchListener() {
        //spin1.setId(kont1);
        gnreSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                if (position > 0) {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        if (GlobalVars.UserData != null) {
            nombre.setText(GlobalVars.UserData.Name);
            apellido1.setText(GlobalVars.UserData.Surname1);
            apellido2.setText(GlobalVars.UserData.Surname2);
            correoelectronico.setText(GlobalVars.UserData.Email);
            movil.setText(GlobalVars.UserData.Telephone);
            dateBut.setText(GlobalVars.UserData.Birthdate);
            gnreSpin.setSelection(GlobalVars.UserData.Genre);
            if (GlobalVars.UserData.addToNewsletter) {
                promociones.setChecked(true);
            }
        }



        dataCharged = true;

        restoreActionBar();
    }

    @Override
    public void onStart() {
        super.onStart();

        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);

        String lag = "es";
        if(GlobalVars.idioma == 1)lag = "eu";
        String lagUser = "";
        if(GlobalVars.UserData!=null){
            lagUser = String.valueOf(GlobalVars.UserData.ID);
        }

        // Set screen name.
        tracker.setScreenName("VistaMisDatos");

        // Send a screen view.
        tracker.send(new HitBuilders.ScreenViewBuilder()
                .setCustomDimension(1, lag)
                .setCustomDimension(2, lagUser)
                .setCustomDimension(3, "(not set)")
                .setCustomDimension(4, "(not set)")
                .setCustomDimension(5, "(not set)")
                .setCustomDimension(6, "(not set)")
                .setCustomDimension(7, "(not set)")
                .setCustomDimension(8, Locale.getDefault().getLanguage())
                .build());
    }

    public void setdateBut(String str)
    {
        dateBut.setText(str);
    }

    public void xmlCharged(AddUserResponse addUserRes)
    {
        if(addUserRes.Error == 0)
        {
            GlobalVars.UserData.ID = addUserRes.Id;
            GlobalVars.UserData.Birthdate = dateBut.getText().toString();
            GlobalVars.UserData.Email = correoelectronico.getText().toString();
            GlobalVars.UserData.Genre = gnreSpin.getSelectedItemPosition();
            GlobalVars.UserData.Name = nombre.getText().toString();
            GlobalVars.UserData.Surname1 = apellido1.getText().toString();
            GlobalVars.UserData.Surname2 = apellido2.getText().toString();
            GlobalVars.UserData.Telephone = movil.getText().toString();
            GlobalVars.UserData.addToNewsletter = promociones.isChecked();

            SharedPreferences mPrefs = getSharedPreferences("mypref", 0);

            SharedPreferences.Editor mEditor = mPrefs.edit();
            mEditor.putString("birthdate", GlobalVars.UserData.Birthdate).commit();
            mEditor.putString("email", GlobalVars.UserData.Email).commit();
            mEditor.putInt("genre", GlobalVars.UserData.Genre).commit();
            mEditor.putInt("id", GlobalVars.UserData.ID).commit();
            mEditor.putString("name", GlobalVars.UserData.Name).commit();
            mEditor.putString("surname1", GlobalVars.UserData.Surname1).commit();
            mEditor.putString("surname2", GlobalVars.UserData.Surname2).commit();
            mEditor.putString("telephone", GlobalVars.UserData.Telephone).commit();
            mEditor.putBoolean("addnewsletter", GlobalVars.UserData.addToNewsletter).commit();
            mEditor.putString("password", GlobalVars.password).commit();

            if(GlobalVars.idioma == 1)
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.midatos_toast_registro_completo_eu), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.midatos_toast_registro_completo), Toast.LENGTH_SHORT).show();

        }
        else
        {
            if(GlobalVars.idioma == 1)
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.midatos_toast_error_eu), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.midatos_toast_error), Toast.LENGTH_SHORT).show();

        }
    }

    public void ModificarClicked(View v)
    {
        if((nombre.getText().length()<1)||
                (apellido1.getText().length()<1)||
                (apellido2.getText().length()<1)||
                (correoelectronico.getText().length()<1)||
                (movil.getText().length()<1)||
                (gnreSpin.getSelectedItemPosition()==0))
        {
            if(GlobalVars.idioma == 1)
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.midatos_toast_rellena_eu), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.midatos_toast_rellena), Toast.LENGTH_SHORT).show();
        }
        else if((pswnuevo.getText().length()<1)&&(pswrepetido.getText().length()<1))
        {
            json = new JSONTask_Modificar_User(this, MisDatosActivity.this, GlobalVars.UserData.ID, nombre.getText().toString(), apellido1.getText().toString(), apellido2.getText().toString(), correoelectronico.getText().toString(), GlobalVars.password.toString(), movil.getText().toString(), dateBut.getText().toString(), gnreSpin.getSelectedItemPosition(), promociones.isChecked(), GlobalVars.shopID);
            json.execute();
        }
        else if(pswnuevo.getText().toString().equals(pswrepetido.getText().toString()))
        {
            System.out.println(" ::::::::::::::::::::::::::::: Password Antiguo: >> :"+GlobalVars.password.toString());
            if(pswantiguo.getText().toString().equals(GlobalVars.password.toString()))
            {
                json = new JSONTask_Modificar_User(this, MisDatosActivity.this, GlobalVars.UserData.ID, nombre.getText().toString(), apellido1.getText().toString(), apellido2.getText().toString(), correoelectronico.getText().toString(), pswnuevo.getText().toString(), movil.getText().toString(), dateBut.getText().toString(), gnreSpin.getSelectedItemPosition(), promociones.isChecked(), GlobalVars.shopID);
                json.execute();
            }
            else
            {
                if(GlobalVars.idioma == 1)
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.midatos_toast_pswd_incorrecto_eu), Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.midatos_toast_pswd_incorrecto), Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            if(GlobalVars.idioma == 1)
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.midatos_toast_pswd_no_coincide_eu), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.midatos_toast_pswd_no_coincide), Toast.LENGTH_SHORT).show();
        }
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }



    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, day, month, year);
        }

        public void onDateSet(DatePicker view, int day, int month, int year) {
            // Do something with the date chosen by the user
            ((MisDatosActivity)getActivity()).setdateBut(String.valueOf(day)+"-"+String.valueOf(month+1)+"-"+String.valueOf(year));
        }
    }
}
