package es.com.acc.appEcommerce.utils;

import java.util.ArrayList;
import java.util.List;

import es.com.acc.appEcommerce.entities.CategoriaRow;
import es.com.acc.appEcommerce.entities.CestaCompra;
import es.com.acc.appEcommerce.entities.Orden;
import es.com.acc.appEcommerce.entities.UserLogin;

/**
 * Created by Imanol on 28/04/2015.
 */
public class GlobalVars {
    public static List<CategoriaRow> Categorias =  new ArrayList<CategoriaRow>();
    public static List<Orden> Ordenes =  new ArrayList<Orden>();

    public static int momentSection = 1;
    public static int momentSub = 0;
    public static int momentOrd = 0;
    public static int screenWidth = 0;
    public static int screenHeight = 0;
    public static int ProductID = 0;
    public static String CategoriaID = "";

    public static boolean refresh_menu = true;
    public static boolean refresh_db = true;
    public static DataBaseHandler myDB;
    public static int db_version = 1;
    public static String db_name = "Kukimba_DB";
    public static String app = "kukimba";

    public static UserLogin UserData = null;
    public static CestaCompra CestaData = null;

    public static String password = "";
    //public static int CestaID = 311154;
    public static int CestaID = 0;
    public static int shopID = 5;

    public static int idioma = -1;
    public static int cesta_count = 0;

    public static String DireccionEntrega;
    public static int payMethod = 1;

    public static String TPVvalue = "";

    public static boolean returnToDestacado = false;
    public static boolean returnToFavorito = false;
}
