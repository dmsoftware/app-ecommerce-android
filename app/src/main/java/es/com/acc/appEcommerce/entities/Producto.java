package es.com.acc.appEcommerce.entities;

/**
 * Created by Imanol on 30/04/2015.
 */
public class Producto
{
    //@SerializedName("id")
    public int ID;
    public String Name;
    public String Brand;
    public int BrandID;
    public String Description;
    public String[] Image;
    public Boolean Goretex;
    public float OriginalPrice;
    public float Price;
    public float ShippingCost;
    public Tallas[] Size;
    public Producto[] related;

    public Producto() {

    }
}
