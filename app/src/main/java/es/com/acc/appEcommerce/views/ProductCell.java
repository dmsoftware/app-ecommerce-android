package es.com.acc.appEcommerce.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;

import es.com.acc.appEcommerce.R;

/**
 * Created by Imanol on 04/05/2015.
 */

public class ProductCell extends ViewGroup
{
    public ImageView imagen;
    public TextView name;
    public TextView brand;
    public TextView price;
    public String imgURL;
    public String SERVER_URL = "http://images.kukimba.com/item/233x155/";
    public View myView;
    private Context context;

    public ProductCell (Context contxt)
    {
        super(contxt);
        context = contxt;
        myView = ViewGroup.inflate(context,R.layout.product_cell, this);
        imagen = (ImageView)findViewById(R.id.imagen);
        brand = (TextView)findViewById(R.id.brand);
        name = (TextView)findViewById(R.id.name);
        price = (TextView)findViewById(R.id.price);
        //addView(myView);
    }

    public void setValues(String tit, String bra, String prc, String url)
    {
        brand.setText(bra);
        name.setText(tit);
        price.setText(prc+" €");
        imgURL = url;

        String pathURL = SERVER_URL;
        pathURL += imgURL;
        //pathURL += MerchID;

        new LoadImage().execute(pathURL);
    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        protected Bitmap doInBackground(String... args) {
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image)
        {
            if(image != null){
                imagen.setImageBitmap(image);
            }else{
            }
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        // TODO Auto-generated method stub
        //super.onLayout(changed, l, t, r, b);
    }
}
