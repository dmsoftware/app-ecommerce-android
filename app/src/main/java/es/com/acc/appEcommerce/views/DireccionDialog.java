package es.com.acc.appEcommerce.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.Address;
import es.com.acc.appEcommerce.entities.Country;
import es.com.acc.appEcommerce.entities.Province;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Get_Countrys;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Get_Provincies;

/**
 * Created by Imanol on 30/04/2015.
 */
public class DireccionDialog extends DialogFragment
{
    private JSONTask_Get_Countrys jsonCountry;
    private JSONTask_Get_Provincies jsonProvince;

    private Address direccion = null;
    private String myTitle;
    private boolean selected = false;

    public View myView;
    public boolean dialogInited = false;

    private DialogListener listener;

    List<Country> countries;
    List<String> countrieValues;

    List<Province> provinces;
    List<String> provinceValues;

    Spinner  spinPais;
    Spinner  spinProvince;

    CheckBox chk;
    EditText nombre;
    EditText direc;
    EditText cp;
    EditText comentario;
    EditText ciudad;

    int selectedCountry = -1;
    int selectedProvince = -1;

    public interface DialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, Address lag);
    }

    public void setValues(String tit, Address lag)
    {
        myTitle = tit;
        if(lag==null)
        {
            direccion = new Address();
            direccion.IsDefault = false;
            direccion.Observation = "";
            direccion.City = "";
            direccion.AddressName = "";
            direccion.Contact = "";
            direccion.Country = 0;
            direccion.Id = 0;
            direccion.Province = 0;
            direccion.Zip = "";
        }
        else {
            direccion = lag;
        }
    }

    // Override the Fragment.onAttach() method to instantiate the
    // NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the
            // host
            listener = (DialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement DialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());

        jsonCountry = new JSONTask_Get_Countrys(this);
        jsonCountry.execute();

        myView = inflater.inflate(R.layout.dialog_direcciones_view, null);


        TextView cabecera = (TextView)myView.findViewById(R.id.cabecera);
        cabecera.setText(myTitle);

        spinPais = (Spinner)myView.findViewById(R.id.spinPais);
        spinProvince = (Spinner)myView.findViewById(R.id.spinProvincia);

            chk = (CheckBox) myView.findViewById(R.id.chkPredeterminado);
            chk.setChecked(direccion.IsDefault);
            chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    direccion.IsDefault = isChecked;
                }
            });

            nombre = (EditText) myView.findViewById(R.id.editNombre);
            nombre.setText(direccion.Contact);
            nombre.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                    direccion.Contact = s.toString();
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });

            direc = (EditText) myView.findViewById(R.id.editDireccion);
            direc.setText(direccion.AddressName);
            direc.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                    direccion.AddressName = s.toString();
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });

            cp = (EditText) myView.findViewById(R.id.editCp);
            cp.setText(direccion.Zip);
            cp.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                    direccion.Zip = s.toString();
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });

            ciudad = (EditText) myView.findViewById(R.id.editCiudad);
            ciudad.setText(direccion.City);
            ciudad.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                    direccion.City = s.toString();
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });

            comentario = (EditText) myView.findViewById(R.id.editComentario);
            comentario.setText(direccion.Observation);
            comentario.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                    direccion.Observation = s.toString();
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });


        if(GlobalVars.idioma == 1) {

            chk.setText(getResources().getString(R.string.direcciones_predeterminado_eu));
            nombre.setHint(getResources().getString(R.string.midatos_hint_nombre_eu));
            direc.setHint(getResources().getString(R.string.direcciones_direccion_eu));
            cp.setHint(getResources().getString(R.string.direcciones_cp_eu));
            comentario.setHint(getResources().getString(R.string.direcciones_comentario_eu));
            ciudad.setHint(getResources().getString(R.string.direcciones_ciudad_eu));

            builder.setCustomTitle(null)
                    .setView(myView)
                    .setPositiveButton(R.string.dialog_ok_eu,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialogInited = false;
                                    listener.onDialogPositiveClick(DireccionDialog.this, direccion);
                                }
                            })
                    .setNegativeButton(R.string.dialog_ko_eu,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    //listener.onDialogNegativeClick(RadioDialog.this);
                                }
                            });
        }
        else
        {
            chk.setText(getResources().getString(R.string.direcciones_predeterminado));
            nombre.setHint(getResources().getString(R.string.midatos_hint_nombre));
            direc.setHint(getResources().getString(R.string.direcciones_direccion));
            cp.setHint(getResources().getString(R.string.direcciones_cp));
            comentario.setHint(getResources().getString(R.string.direcciones_comentario));
            ciudad.setHint(getResources().getString(R.string.direcciones_ciudad));

            builder.setCustomTitle(null)
                    .setView(myView)
                    .setPositiveButton(R.string.dialog_ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialogInited = false;
                                    listener.onDialogPositiveClick(DireccionDialog.this, direccion);
                                }
                            })
                    .setNegativeButton(R.string.dialog_ko,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    //listener.onDialogNegativeClick(RadioDialog.this);
                                }
                            });
        }

        return builder.create();
    }

    public void xmlCountryCharged(List<Country> _countries)
    {
        countries = _countries;

        countrieValues = new ArrayList<String>();
        if (GlobalVars.idioma == 1)
            countrieValues.add(getResources().getString(R.string.direcciones_pais_eu));
        else
            countrieValues.add(getResources().getString(R.string.direcciones_pais));

        for(Country cou:countries) {
            countrieValues.add(cou.Name);
        }

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(getActivity(), R.layout.my_spinner_item,countrieValues);
        spinnerArrayAdapter1.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinPais.setAdapter(spinnerArrayAdapter1);
        //if (!dialogInited) {
        //spin1.setOnTouchListener(new AdapterView.OnTouchListener() {
        //spinPais.setId(1);
        spinPais.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                if (position > 0) {
                    direccion.Country = countries.get(position-1).ID;
                    loadProvinces(position-1);
                }
                else{   }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });

        if(direccion!=null)
        {
            spinPais.setSelection(direccion.Country);
            loadProvinces(direccion.Country);
        }
        else {
            loadProvinces(0);
        }
    }

    public void loadProvinces(int pos)
    {
        jsonProvince = new JSONTask_Get_Provincies(this,countries.get(pos).ID);
        jsonProvince.execute();
    }

    public void xmlProvinceCharged(List<Province> _provinces)
    {
        provinces = _provinces;

        provinceValues = new ArrayList<String>();
        if (GlobalVars.idioma == 1)
            provinceValues.add(getResources().getString(R.string.direcciones_provincia_eu));
        else
            provinceValues.add(getResources().getString(R.string.direcciones_provincia));

        int lag = 1;
        for(Province pro:provinces) {
            provinceValues.add(pro.Name);
            if(direccion!=null) {
                if (direccion.Province == pro.ID) {
                    selectedProvince = lag;
                }
            }
            lag++;
        }

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(getActivity(), R.layout.my_spinner_item,provinceValues);
        spinnerArrayAdapter1.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinProvince.setAdapter(spinnerArrayAdapter1);
        //if (!dialogInited) {
        //spin1.setOnTouchListener(new AdapterView.OnTouchListener() {
        //spinPais.setId(1);
        spinProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                if (position > 0) {
                    direccion.Province = provinces.get(position-1).ID;
                    selectedProvince = position-1;
                }
                else{  }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });


        if(selectedProvince > -1) {
            spinProvince.setSelection(selectedProvince);
            //System.out.println(" ::::::::::::::::::::::::::::: direccion.Province: >> "+direccion.Province+"::: "+provinceValues.size());
        }

    }
}
