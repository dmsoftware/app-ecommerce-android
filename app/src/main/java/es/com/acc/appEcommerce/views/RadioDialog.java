package es.com.acc.appEcommerce.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.utils.GlobalVars;

/**
 * Created by Imanol on 30/04/2015.
 */
public class RadioDialog extends DialogFragment
{
    private String[] myStringArray;
    private String myTitle;
    private int choice = 0;

    private DialogListener listener;

    public interface DialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, int cho);
    }

    public void setValues(String tit, String[] lag, int selID)
    {
        myTitle = tit;
        myStringArray = lag;
        choice = selID;
    }

    // Override the Fragment.onAttach() method to instantiate the
    // NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the
            // host
            listener = (DialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement DialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if(GlobalVars.idioma == 1) {
            builder.setTitle(myTitle)
                    .setSingleChoiceItems(myStringArray, choice,
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    choice = arg1;
                                }
                            })
                    .setPositiveButton(R.string.dialog_ok_eu,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    listener.onDialogPositiveClick(RadioDialog.this, choice);
                                }
                            })
                    .setNegativeButton(R.string.dialog_ko_eu,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    //listener.onDialogNegativeClick(RadioDialog.this);
                                }
                            });
        }
        else
        {
            builder.setTitle(myTitle)
                    .setSingleChoiceItems(myStringArray, choice,
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    choice = arg1;
                                }
                            })
                    .setPositiveButton(R.string.dialog_ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    listener.onDialogPositiveClick(RadioDialog.this, choice);
                                }
                            })
                    .setNegativeButton(R.string.dialog_ko,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    //listener.onDialogNegativeClick(RadioDialog.this);
                                }
                            });
        }

        return builder.create();
    }
}
