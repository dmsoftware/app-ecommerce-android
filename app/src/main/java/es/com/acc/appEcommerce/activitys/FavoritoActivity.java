package es.com.acc.appEcommerce.activitys;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.entities.CategoriaRow;
import es.com.acc.appEcommerce.entities.ItemCategories;
import es.com.acc.appEcommerce.entities.Producto;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Get_Favoritos;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_ItemCategories;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

public class FavoritoActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{
    private JSONTask_Get_Favoritos json;
    private JSONTask_ItemCategories jsonItemCats;

    public List<Producto> Productos;
    public RelativeLayout container;
    public String SERVER_URL = "http://images.kukimba.com/item/233x155/";
    public ScrollView Scroller;
    public boolean scrollStarted = false;

    public boolean clicked = false;

    @Override
    public void onStart() {
        super.onStart();

        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);

    }

    @Override
    protected void onResume()
    {
        // TODO Auto-generated method stub
        super.onResume();
        if(dataCharged) {
            clicked = false;
            setRefreshing(false);
        }
        freeMemory();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_favoritos);

        container =  (RelativeLayout) findViewById(R.id.cellContainer);

        //mTitle = GlobalVars.Categorias.get(GlobalVars.momentSection-1).principal.Name;
        if(GlobalVars.idioma == 1)
            mTitle = getResources().getString(R.string.menu_favoritos_eu);
        else
            mTitle = getResources().getString(R.string.menu_favoritos);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

            // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                    R.id.navigation_drawer,
                    (DrawerLayout) findViewById(R.id.drawer_layout),
                    CategoriasPrincipales,R.drawable.ic_drawer);

        Scroller = (ScrollView)findViewById(R.id.scroller);

        setRefreshing(true);

        dataCharged = false;

        json = new JSONTask_Get_Favoritos(this,FavoritoActivity.this,String.valueOf(GlobalVars.UserData.ID));
        json.execute();
    }

    public void xmlCharged(List<Producto> Prods)
    {
        dataCharged = true;
        int oldIndex = 0;
        if(Productos == null)
        {
            Productos = new ArrayList<Producto>();
            for(Producto pro: Prods)
            {
                Productos.add(pro);
            }
        }
        else
        {
            oldIndex = Productos.size();
            for(Producto pro: Prods)
            {
                Productos.add(pro);
            }
        }

        int scrollHeight = Scroller.getChildAt(0).getHeight();

        for (int number = oldIndex; number < Productos.size(); number++)
        {
            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            RelativeLayout menuLayout = (RelativeLayout) inflater.inflate(R.layout.product_cell, null, false);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(GlobalVars.screenWidth/2-2, (int) (GlobalVars.screenWidth/1.5)-1);
            RelativeLayout cont = (RelativeLayout)menuLayout.findViewById(R.id.productContainer);
            cont.setLayoutParams(params);
            cont.setId(Productos.get(number).ID);

            ImageView img = (ImageView) menuLayout.findViewById(R.id.imagen);
            //img.getLayoutParams().height = (int) (GlobalVars.screenWidth/2.8/1.5);
            //img.getLayoutParams().width = (int) (GlobalVars.screenWidth/2.8);

            TextView descripcion = (TextView) menuLayout.findViewById(R.id.brand);
            descripcion.setText(Productos.get(number).Brand);

            TextView name = (TextView) menuLayout.findViewById(R.id.name);
            name.setText(Productos.get(number).Name);

            TextView precio = (TextView) menuLayout.findViewById(R.id.price);
            //precio.setText(Float.toString(Productos.get(number).Price)+" €");
            precio.setText( String.format( "%.2f", Productos.get(number).Price)+" €");

            TextView precioOriginal = (TextView) menuLayout.findViewById(R.id.originalprice);
            if(Productos.get(number).Price != Productos.get(number).OriginalPrice)
            {
                //precioOriginal.setText(Float.toString(Productos.get(number).OriginalPrice) + " €");
                precioOriginal.setText( String.format( "%.2f", Productos.get(number).OriginalPrice)+" €");
                precioOriginal.setPaintFlags(precioOriginal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                TextView rebajaText = (TextView) menuLayout.findViewById(R.id.rebajatext);
                int lag = (int) Math.floor((100 - Productos.get(number).Price * 100 / Productos.get(number).OriginalPrice));
                rebajaText.setText(lag+" %");
            }
            else
            {
                precioOriginal.setVisibility(View.INVISIBLE);
                RelativeLayout rebajaPanel = (RelativeLayout) menuLayout.findViewById(R.id.rebajapanel);
                rebajaPanel.setVisibility(View.INVISIBLE);
            }

            LinearLayout botonC = (LinearLayout)menuLayout.findViewById(R.id.botonCelda);
            botonC.setId(Productos.get(number).ID);
            //menuLayout.setId(Productos.get(number).ID);

            RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(GlobalVars.screenWidth/2-2, (int) (GlobalVars.screenWidth/1.8)-1);
            int x = 2 + ((number-oldIndex)%2) * (GlobalVars.screenWidth/2 - 1);
            int y = (int) (scrollHeight + 1 + ((number-oldIndex)/2) * GlobalVars.screenWidth/1.8);
            params2.setMargins(x, y, 1, 0);
            container.addView(menuLayout,params2);

            String pathURL = SERVER_URL;
            pathURL += Productos.get(number).Image[0];
            //pathURL += MerchID;

            //System.out.println(" ::::::::::::::::::::::::::::: load image:  "+number+" >> "+Productos.get(number).Image[0]);

            LoadImage imgJson = new LoadImage(img,FavoritoActivity.this);
            imgJson.execute(pathURL);
        }
        scrollStarted = true;

        setRefreshing(false);

        //sendAnalitycHit("SubCategoria","es",String.valueOf(GlobalVars.UserData.ID),"","",momentCategory,"",mainCategory,"es");
    }

    public void onClick(View view)
    {
        if(clicked==false) {
            setRefreshing(true);
            dataCharged = false;
            clicked=true;
            GlobalVars.ProductID = view.getId();
            jsonItemCats = new JSONTask_ItemCategories(this, FavoritoActivity.this, GlobalVars.ProductID);
            jsonItemCats.execute();
        }
    }

    @Override
    public void xmlItemCatCharged(List<ItemCategories> cats)
    {
        for( ItemCategories cat: cats) {
            if (cat.Id.contains("co")) {
                int pos = 2;
                for (CategoriaRow catId : GlobalVars.Categorias) {
                    if (catId.principal.MerchantID.equals(cat.Id)) {
                        GlobalVars.momentSection = pos;
                        break;
                    }
                    pos++;
                }
            }
        }
        for( ItemCategories cat: cats)
        {
            if (cat.Id.contains("co")) {

            }
            else
            {
                int pos = 0;
                for (CategoriaRow catId: GlobalVars.Categorias.get(GlobalVars.momentSection).secundarios)
                {
                    if(catId.principal.MerchantID.equals(cat.Id))
                    {
                        GlobalVars.momentSub = pos;
                        break;
                    }
                    pos++;
                }
            }
        }

        GlobalVars.returnToFavorito = true;

        Intent intent = new Intent(FavoritoActivity.this, ProductSheetActivity.class);
        startActivity(intent);

        setRefreshing(false);
        dataCharged = true;
    }
}