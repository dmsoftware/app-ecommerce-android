package es.com.acc.appEcommerce.activitys;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.activitys.WebView.FormResponesWebActivity;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

/**
 * Created by Imanol on 08/06/2015.
 */
public class ContactoActivity extends BaseActivity {

    private TextView EdNombre;
    private TextView EdApellidos;
    private TextView EdTelefono;
    private TextView EdEmail;
    private TextView EdAsunto;
    private TextView EdMensaje;

    private boolean textChanged = false;

    public String  nombre;
    public String  apellidos;
    public String telefono;
    public String  email;
    public String  asunto;
    public String  mensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Bundle b = getIntent().getExtras();
        //ProductID = b.getInt("keyID");

        setContentView(R.layout.activity_contacto);
        dataCharged = true;
        mTitle = MainSections[GlobalVars.momentSection];

        String lag = "es";
        if(GlobalVars.idioma == 1)lag = "eu";
        String lagUser = "";
        if(GlobalVars.UserData!=null){
            lagUser = String.valueOf(GlobalVars.UserData.ID);
        }

        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("#ver-DMForm-Contacto")
                .setAction(lag)
                .setLabel(lagUser)
                .build());

        textChanged = false;

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                CategoriasPrincipales,R.drawable.ic_drawer);

        TextView intro = (TextView) findViewById(R.id.iniText);
        intro.setText(Html.fromHtml("\n" +
                "<h1>Contacta con nosotros</h1>\n" +
                "<p><span style=\"font-size: 14px;\"><strong>¿Necesitas ayuda?  </strong></span><br>\n" +
                "Si tienes una duda y quieres que te ayudemos, consulta primero las <a href=\"/kukimba/de/preguntas-frecuentes.asp?cod=2387&amp;nombre=2387&amp;id='Preguntas\">preguntas frecuentes</a>. La mayoría de dudas están solucionadas ahí.</p>\n" +
                "<p>Si no encuentras una respuesta, rellena el formulario y nos pondremos en contacto contigo.</p>\n" +
                "<p>También puedes contactar con nosotros en el teléfono:<br>\n" +
                "<span style=\"color: rgb(233, 90, 51);\"><span style=\"font-size: 20px;\"><strong>943 43 42 18</strong></span></span><br>\n" +
                "<em>Horario de atención<br>\n" +
                "<!--L-J 9:00-13:00 / 16:00-18:00<br />\n" +
                "V&nbsp;&nbsp;&nbsp; 9:00-13:30-->  L-V 9:00-13:00 </em></p>\n"));

        EdNombre = (EditText)findViewById(R.id.EditNombre);
        EdNombre.setHint("Nombre");
        EdNombre.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.equals("") )
                    sendAnaChanged();
            }
            public void beforeTextChanged(CharSequence s, int start, int count,int after) { }
            public void afterTextChanged(Editable s) { }
        });

        EdApellidos = (EditText)findViewById(R.id.EditApellidos);
        EdApellidos.setHint("Apellidos");
        EdApellidos.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.equals("") )
                    sendAnaChanged();
            }
            public void beforeTextChanged(CharSequence s, int start, int count,int after) { }
            public void afterTextChanged(Editable s) { }
        });

        EdTelefono = (EditText)findViewById(R.id.EditTelefono);
        EdTelefono.setHint("Telefono");
        EdTelefono.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.equals("") )
                    sendAnaChanged();
            }
            public void beforeTextChanged(CharSequence s, int start, int count,int after) { }
            public void afterTextChanged(Editable s) { }
        });

        EdEmail = (EditText)findViewById(R.id.EditEmail);
        EdEmail.setHint("Email");
        EdEmail.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.equals("") )
                    sendAnaChanged();
            }
            public void beforeTextChanged(CharSequence s, int start, int count,int after) { }
            public void afterTextChanged(Editable s) { }
        });

        EdAsunto = (EditText)findViewById(R.id.EditAsunto);
        EdAsunto.setHint("Asunto");
        EdAsunto.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.equals("") )
                    sendAnaChanged();
            }
            public void beforeTextChanged(CharSequence s, int start, int count,int after) { }
            public void afterTextChanged(Editable s) { }
        });

        EdMensaje = (EditText)findViewById(R.id.EditMensaje);
        EdMensaje.setHint("Mensaje");
        EdMensaje.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.equals("") )
                    sendAnaChanged();
            }
            public void beforeTextChanged(CharSequence s, int start, int count,int after) { }
            public void afterTextChanged(Editable s) { }
        });
    }

    public void sendAnaChanged()
    {
        if(textChanged==false) {

            textChanged = true;
            String lag = "es";
            if (GlobalVars.idioma == 1) lag = "eu";

            String lagUser = "";
            if(GlobalVars.UserData!=null){
                lagUser = String.valueOf(GlobalVars.UserData.ID);
            }

            tracker.send(new HitBuilders.EventBuilder()
                    .setCategory("#rellenar-DMForm-Contacto")
                    .setAction(lag)
                    .setLabel(lagUser)
                    .build());
        }
    }
    public void llamarClicked(View vi)
    {
        String lag = "es";
        if (GlobalVars.idioma == 1) lag = "eu";

        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("llamada")
                .setAction(lag)
                .setLabel("telefono")
                .build());

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:943 43 42 18"));
        startActivity(callIntent);
    }

    public void enviarClicked(View vi)
    {
        //get message from message box
        nombre = EdNombre.getText().toString();
        apellidos = EdApellidos.getText().toString();
        telefono = EdTelefono.getText().toString();
        email = EdEmail.getText().toString();
        asunto = EdAsunto.getText().toString();
        mensaje = EdMensaje.getText().toString();

        String lag = "es";
        if (GlobalVars.idioma == 1) lag = "eu";

        String lagUser = "";
        if(GlobalVars.UserData!=null){
            lagUser = String.valueOf(GlobalVars.UserData.ID);
        }

        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("#envia-DMForm-Contacto")
                .setAction(lag)
                .setLabel(lagUser)
                .build());


        textChanged = false;

        //check whether the msg empty or not
        if((nombre.length()>0)&&(apellidos.length()>0)&&(email.length()>0)&&(asunto.length()>0)&&(mensaje.length()>0)) {
            new SendFormulatioTask().execute();
        } else {
            //display message if text field is empty
            Toast.makeText(getBaseContext(),"All fields are required",Toast.LENGTH_SHORT).show();
        }
    }

    public void RespondWindow(String str)
    {
        Intent intent = new Intent(ContactoActivity.this, FormResponesWebActivity.class);
        Bundle b = new Bundle();
        b.putString("htmlString", str);
        intent.putExtras(b); //Put your id to your next Intent
        startActivity(intent);
    }

    private class SendFormulatioTask extends AsyncTask<String, String, Void> {

        protected void onPreExecute() {
            // prepare the request

        }

        protected Void doInBackground(String... aurl) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://www.kukimba.com/modulos/formularios/recogerformulario.asp?sesion=1&amp;codform=1");
            try {
                //SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/HH:mm:ss");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                //nameValuePairs.add(new BasicNameValuePair("form_c1", "01"));
                nameValuePairs.add(new BasicNameValuePair("form_c2", currentDateandTime));
                nameValuePairs.add(new BasicNameValuePair("form_c3", nombre));
                nameValuePairs.add(new BasicNameValuePair("form_c4", apellidos));
                nameValuePairs.add(new BasicNameValuePair("form_c5", telefono));
                nameValuePairs.add(new BasicNameValuePair("form_c6", email));
                nameValuePairs.add(new BasicNameValuePair("form_c7", asunto));
                nameValuePairs.add(new BasicNameValuePair("form_c8", mensaje));
                nameValuePairs.add(new BasicNameValuePair("captcha", "appkukimba"));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                System.out.println(" :::::: send POST: >> " + httppost);
                String responseBody = EntityUtils.toString(response.getEntity());
                System.out.println(" ::::::::::::::::::::::::::::: HttpResponse:  "+responseBody+" ::::::::::::::---:::::::::");

                RespondWindow(responseBody);

                /*startActivityForResult(new Intent(getBaseContext(),
                        ContactoActivity.class), 0);*/

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Do what you want after the request has been handled
        }
    }
}
