package es.com.acc.appEcommerce.activitys.WebView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import es.com.acc.appEcommerce.R;
import es.com.acc.appEcommerce.activitys.BaseActivity;
import es.com.acc.appEcommerce.entities.WebNode;
import es.com.acc.appEcommerce.utils.GlobalVars;
import es.com.acc.appEcommerce.utils.JSON.JSONTask_Get_Web_Node;
import es.com.acc.appEcommerce.views.NavigationDrawerFragment;

public class ContactoWebActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{
    public String URLPath = "http://www.kukimba.com/kukimba/de/contacto.asp?cod=2379&nombre=2379";
    public WebView myWebView;
    public boolean loaded = false;
    private JSONTask_Get_Web_Node json;

    public void onStart() {
        super.onStart();
        // The rest of your onStart() code.
        if(mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.selectItem(0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Secondary Activity", String.valueOf(GlobalVars.momentSection));
        setContentView(R.layout.activity_webview);

        mTitle = MainSections[GlobalVars.momentSection];

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                CategoriasPrincipales,R.drawable.ic_drawer);

        myWebView = (WebView) this.findViewById(R.id.webView);
        myWebView.setWebViewClient(new MyWebViewClient());
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.loadUrl(URLPath);

        //json = new JSONTask_Get_Web_Node(this, ContactoWebActivity.this, "2379");
        //json.execute();
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (Uri.parse(url).getHost().equals(URLPath)) {
                // This is my web site, so do not override; let my WebView load the page
                return false;
            }
            // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }

        /*@Override
        public void onPageFinished(WebView view, String url) {
            //view.getContentDescription();
            if(loaded==false) {
                loaded = true;
                String lag = (String) view.getContentDescription();
                System.out.println(" :::::: page content: >> " + lag);
                String htmlData = "<link rel=\"stylesheet\" type=\"text/css\" href=\"skin49.css\" />" + lag;
// lets assume we have /assets/style.css file
                myWebView.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html", "UTF-8", null);
                //myWebView.loadUrl("javascript:window.HtmlViewer.showHTML" + "(document.getElementsByTagName('body')[0].innerHTML);");
            }
        }*/
    }
//
    @Override
    public void xmlWebCharged(WebNode result)
    {
        dataCharged = true;
        setRefreshing(false);

        //myWebView = (WebView) this.findViewById(R.id.webView);
        /*myWebView.loadData("<style>\n" +
                "body { margin:20px }\n" +
                "</style>\n"+result.Description, "text/html", "UTF-8");*/

        String htmlData = "<link rel=\"stylesheet\" type=\"text/css\" href=\"skin49.css\" />" + result.Description;
// lets assume we have /assets/style.css file
        myWebView.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html", "UTF-8", null);
    }
}